# Content Moderation Links

This module provides links for content moderation state transitions.

* For a full description of the module visit:
  https://www.drupal.org/project/content_moderation_links
* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/content_moderation_links

## Requirements

This module requires no modules outside of Drupal core.

## Installation

* Install the Content Moderation Links module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.
