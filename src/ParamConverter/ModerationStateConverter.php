<?php

namespace Drupal\content_moderation_links\ParamConverter;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\workflows\WorkflowInterface;
use Drupal\workflows\WorkflowTypeInterface;
use Symfony\Component\Routing\Route;

/**
 * Parameter converter for upcasting content moderation state IDs to objects.
 */
class ModerationStateConverter implements ParamConverterInterface {

  /**
   * The moderation information.
   */
  protected ModerationInformationInterface $moderationInformation;

  /**
   * Constructs a new ModerationStateConverter object.
   *
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information.
   *
   * @codeCoverageIgnore
   */
  public function __construct(ModerationInformationInterface $moderation_information) {
    $this->moderationInformation = $moderation_information;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $route = $this->getRoute($defaults);

    if (!($route instanceof Route)) {
      return NULL;
    }

    $entity_type_id = $this->getEntityTypeId($route, $defaults);

    if (!isset($entity_type_id)) {
      return NULL;
    }

    $entity = $this->getEntity($entity_type_id, $defaults);

    if (!($entity instanceof ContentEntityInterface)) {
      return NULL;
    }

    $workflow_type = $this->getWorkflowType($entity);

    return is_string($value) && $workflow_type instanceof WorkflowTypeInterface ? $workflow_type->getState($value) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return is_array($definition) && !empty($definition['type']) && $definition['type'] == 'content_moderation_links:moderation_state';
  }

  /**
   * Returns the content entity object from given route defaults.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $defaults
   *   The route defaults array.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The content entity on success, otherwise NULL.
   */
  protected function getEntity(string $entity_type_id, array $defaults): ?ContentEntityInterface {
    $entity = $defaults[$entity_type_id] ?? NULL;

    if (!($entity instanceof ContentEntityInterface) || !$this->moderationInformation->isModeratedEntity($entity)) {
      return NULL;
    }

    return $entity;
  }

  /**
   * Returns the entity type ID from given route defaults.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   * @param array $defaults
   *   The route defaults array.
   *
   * @return string|null
   *   The entity type ID on success, otherwise NULL.
   */
  protected function getEntityTypeId(Route $route, array $defaults): ?string {
    $entity_type_id = $route->getOption('_content_moderation_links_entity_type');

    if (!$entity_type_id || !isset($defaults[$entity_type_id])) {
      return NULL;
    }

    return is_string($entity_type_id) ? $entity_type_id : NULL;
  }

  /**
   * Returns the route object from given route defaults.
   *
   * @param array $defaults
   *   The route defaults array.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The route object on success, otherwise NULL.
   */
  protected function getRoute(array $defaults): ?Route {
    if (!isset($defaults[RouteObjectInterface::ROUTE_OBJECT])) {
      return NULL;
    }

    $route = $defaults[RouteObjectInterface::ROUTE_OBJECT];

    if (!($route instanceof Route)) {
      return NULL;
    }

    return $route;
  }

  /**
   * Returns the workflow type for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\workflows\WorkflowTypeInterface|null
   *   The workflow type on success, otherwise NULL.
   */
  protected function getWorkflowType(ContentEntityInterface $entity): ?WorkflowTypeInterface {
    $workflow = $this->moderationInformation->getWorkflowForEntity($entity);

    if (!($workflow instanceof WorkflowInterface)) {
      return NULL;
    }

    return $workflow->getTypePlugin();
  }

}
