<?php

namespace Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction;

use Drupal\content_moderation_links\Entity\LinkTemplate;

/**
 * Provides local action definitions to discard latest entity version.
 */
class DiscardLatestVersionDeriver extends ModerationLinkDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    foreach ($this->getModeratedEntityTypes() as $entity_type_id => $entity_type) {
      // Entity type has link template for discard latest entity version?
      if ($entity_type->hasLinkTemplate(LinkTemplate::DISCARD_LATEST_VERSION)) {
        $this->derivatives[$entity_type_id] = [
          'title' => $this->t('Discard latest version'),
          'route_name' => 'entity.' . $entity_type_id . '.content_moderation_links_discard_latest_version',
          'appears_on' => [
            'entity.' . $entity_type_id . '.canonical',
            'entity.' . $entity_type_id . '.latest_version',
          ],
          'weight' => -50,
        ] + $base_plugin_definition;
      }
    }

    return $this->derivatives;
  }

}
