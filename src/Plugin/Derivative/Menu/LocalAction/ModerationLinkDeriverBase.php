<?php

namespace Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\WorkflowInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for menu link definitions of content moderation links.
 */
class ModerationLinkDeriverBase extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The moderation information.
   */
  protected ModerationInformationInterface $moderationInformation;

  /**
   * Constructs a ModerationLinksDeriverBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information.
   *
   * @codeCoverageIgnore
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModerationInformationInterface $moderation_information) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moderationInformation = $moderation_information;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('entity_type.manager'),
      $container->get('content_moderation.moderation_information'),
    );
  }

  /**
   * Gets the entity types that support content moderation.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   An associative array of moderated entity type objects keyed by entity
   *   type ID.
   */
  protected function getModeratedEntityTypes(): array {
    $cache = &drupal_static(__METHOD__);

    if (!isset($cache)) {
      $cache = [];

      $entity_types = $this->entityTypeManager->getDefinitions();

      foreach ($entity_types as $entity_type_id => $entity_type) {
        if (!($entity_type instanceof ContentEntityTypeInterface) || !$this->moderationInformation->isModeratedEntityType($entity_type)) {
          continue;
        }

        $cache[$entity_type_id] = $entity_type;
      }
    }

    return is_array($cache) ? $cache : [];
  }

  /**
   * Gets the content moderation workflow type plugin.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow object.
   *
   * @return \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface|null
   *   The content moderation workflow type plugin on success, otherwise NULL.
   */
  protected function getWorkflowType(WorkflowInterface $workflow): ?ContentModerationInterface {
    $workflow_type = $workflow->getTypePlugin();

    return $workflow_type instanceof ContentModerationInterface ? $workflow_type : NULL;
  }

}
