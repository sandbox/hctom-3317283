<?php

namespace Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction;

use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\ModeratedEntityHelperInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\workflows\Entity\Workflow;

/**
 * Provides local action definitions for moderation state transition links.
 */
class ModerationStateTransitionDeriver extends ModerationLinkDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    foreach (Workflow::loadMultipleByType(ModeratedEntityHelperInterface::WORKFLOW_TYPE) as $workflow) {
      if (($workflow_type = $this->getWorkflowType($workflow))) {
        $entity_type_ids = $workflow_type->getEntityTypes();

        foreach ($entity_type_ids as $entity_type_id) {
          $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

          if (!($entity_type instanceof ContentEntityTypeInterface)) {
            continue;
          }

          // Entity type has link template for discard latest entity version?
          if ($entity_type->hasLinkTemplate(LinkTemplate::MODERATION_STATE_TRANSITION)) {
            foreach ($workflow_type->getTransitions() as $transition) {
              foreach ($transition->from() as $from) {
                if ($from instanceof ContentModerationState) {
                  $this->derivatives[$entity_type_id . '.' . $workflow->id() . '.' . $from->id() . '.' . $transition->to()->id()] = [
                    'title' => $transition->label(),
                    'route_name' => 'entity.' . $entity_type_id . '.content_moderation_links_moderation_state_transition',
                    'route_parameters' => [
                      'from' => $from->id(),
                      'to' => $transition->to()->id(),
                    ],
                    'appears_on' => [
                      'entity.' . $entity_type_id . '.canonical',
                      'entity.' . $entity_type_id . '.latest_version',
                    ],
                    'weight' => $transition->weight(),
                    'cache_contexts' => $workflow->getCacheContexts(),
                    'cache_max_age' => $workflow->getCacheMaxAge(),
                    'cache_tags' => $workflow->getCacheTags(),
                  ] + $base_plugin_definition;
                }
              }
            }
          }
        }
      }
    }

    return $this->derivatives;
  }

}
