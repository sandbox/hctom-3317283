<?php

namespace Drupal\content_moderation_links;

use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Provides an interface for moderated entity helper service classes.
 */
interface ModeratedEntityHelperInterface {

  /**
   * Workflow type: Content moderation.
   */
  const WORKFLOW_TYPE = 'content_moderation';

  /**
   * Discards pending revision(s) of the given content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param string|null $revision_log_message
   *   Optional custom revision log message.
   *
   * @return bool
   *   Whether the latest version has successfully been discarded.
   */
  public function discardLatestVersion(ContentEntityInterface $entity, ?string $revision_log_message = NULL): bool;

  /**
   * Gets the current content moderation state of the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity object.
   *
   * @return \Drupal\content_moderation\ContentModerationState|null
   *   The content moderation state object on success, otherwise NULL.
   */
  public function getCurrentModerationState(ContentEntityInterface $entity): ?ContentModerationState;

  /**
   * Gets the workflow for the given content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity to get the workflow for.
   *
   * @return \Drupal\workflows\WorkflowInterface|null
   *   The workflow entity on success, otherwise NULL.
   */
  public function getWorkflow(ContentEntityInterface $entity): ?WorkflowInterface;

  /**
   * Gets the content moderation workflow type plugin for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity to get the workflow type for.
   *
   * @return \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface|null
   *   The content moderation workflow type plugin on success, otherwise NULL.
   */
  public function getWorkflowType(ContentEntityInterface $entity): ?ContentModerationInterface;

  /**
   * Given content entity has revision in default revision state?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return bool
   *   Whether the given content entity has a revision in default revision
   *   state.
   */
  public function hasRevisionInDefaultRevisionState(ContentEntityInterface $entity): bool;

  /**
   * Is the given content entity a discardable latest version?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return bool
   *   Whether the given content entity is a discardable latest version.
   */
  public function isDiscardableLatestVersion(ContentEntityInterface $entity): bool;

  /**
   * Is the given content entity restored as latest version?
   *
   * This happens e.g. when you transition from a content moderation state with
   * unpublished default revision state to a content moderation state that does
   * not have the default revision state.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return bool
   *   Whether the given content entity is restored as latest version.
   */
  public function isRestoredAsLatestVersion(ContentEntityInterface $entity): bool;

  /**
   * Performs a content moderation transition for the given content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param string $state_to_id
   *   The ID of the content moderation state to transition to.
   * @param string|null $revision_log_message
   *   Optional custom revision log message.
   *
   * @return bool
   *   Whether the transition has successfully been performed.
   */
  public function performTransition(ContentEntityInterface $entity, string $state_to_id, ?string $revision_log_message = NULL): bool;

}
