<?php

namespace Drupal\content_moderation_links;

use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\Entity\Workflow;

/**
 * Provides dynamic content moderation link permissions.
 */
class ModerationLinkPermissions {

  use StringTranslationTrait;

  /**
   * Returns discard latest entity version content moderation link permissions.
   *
   * @return array
   *   The discard latest entity version content moderation link permissions
   *   (per workflow).
   */
  public function discardLatestVersionPermissions(): array {
    $permissions = [];

    foreach (Workflow::loadMultipleByType(ModeratedEntityHelperInterface::WORKFLOW_TYPE) as $workflow) {
      $workflow_type = $workflow->getTypePlugin();
      assert($workflow_type instanceof ContentModerationInterface);

      $permissions['discard latest version in ' . $workflow->id() . ' workflow'] = [
        'title' => $this->t('%workflow workflow: Discard latest version.', [
          '%workflow' => $workflow->label(),
        ]),
        'description' => $this->t('To back up and discard pending revision(s) in %workflow workflow, you also need permission to edit/update the corresponding content and to view its latest version.', [
          '%workflow' => $workflow->label(),
        ]),
        'dependencies' => [
          $workflow->getConfigDependencyKey() => [
            $workflow->getConfigDependencyName(),
          ],
        ],
      ];
    }

    return $permissions;
  }

}
