<?php

namespace Drupal\content_moderation_links\Hook;

use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\workflows\WorkflowInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements hook_entity_operation().
 */
class EntityOperationHook implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Weight offset to apply to all content moderation link operations.
   */
  const WEIGHT_OFFSET = 200;

  /**
   * The moderation information.
   */
  protected ModerationInformationInterface $moderationInformation;

  /**
   * Constructs a new EntityOperationHook object.
   *
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information.
   *
   * @codeCoverageIgnore
   */
  public function __construct(ModerationInformationInterface $moderation_information) {
    $this->moderationInformation = $moderation_information;
  }

  /**
   * Add 'Discard latest version' operation.
   *
   * @param array $operations
   *   An operations array as returned by
   *   EntityListBuilderInterface::getOperations().
   * @param \Drupal\Core\Url $url
   *   The 'Discard latest version' URL object.
   */
  protected function addDiscardLatestVersionOperation(array &$operations, Url $url): void {
    if ($url->access()) {
      $operations['content_moderation_links.discard_latest_version'] = [
        'title' => $this->t('Discard latest version'),
        'url' => $url,
        'weight' => static::WEIGHT_OFFSET,
      ];
    }
  }

  /**
   * Add 'Moderation state transition' operation(s).
   *
   * @param array $operations
   *   An operations array as returned by
   *   EntityListBuilderInterface::getOperations().
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   */
  protected function addModerationStateTransitionOperations(array &$operations, ContentEntityInterface $entity): void {
    foreach ($this->getTransitions($entity) as $transition) {
      foreach ($transition->from() as $from) {
        if ($from instanceof ContentModerationState) {
          $url = $entity->toUrl(LinkTemplate::MODERATION_STATE_TRANSITION);
          $state_from_id = $from->id();
          $state_to_id = $transition->to()->id();
          $transition_id = $transition->id();
          $transition_label = $transition->label();
          $transition_weight = $transition->weight();

          $url->setRouteParameter('from', $state_from_id);
          $url->setRouteParameter('to', $state_to_id);

          if ($url->access()) {
            $operations['content_moderation_links.moderation_state_transition.' . $transition_id] = [
              'title' => $transition_label,
              'url' => $url,
              'weight' => static::WEIGHT_OFFSET + (int) $transition_weight,
            ];
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container): EntityOperationHook {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('content_moderation.moderation_information'),
    );
  }

  /**
   * Declares entity operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which the linked operations will be performed.
   *
   * @return array
   *   An operations array as returned by
   *   EntityListBuilderInterface::getOperations().
   *
   * @see \hook_entity_operation()
   * @see \content_moderation_links_entity_operation()
   */
  public function entityOperation(EntityInterface $entity): array {
    $operations = [];

    if ($entity instanceof ContentEntityInterface && $this->moderationInformation->isModeratedEntity($entity)) {
      // Discard latest version.
      $url_discard_latest_version = $entity->toUrl(LinkTemplate::DISCARD_LATEST_VERSION);
      $this->addDiscardLatestVersionOperation($operations, $url_discard_latest_version);

      // Moderation state transition(s).
      $this->addModerationStateTransitionOperations($operations, $entity);
    }

    return $operations;
  }

  /**
   * Returns available workflow transitions.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\workflows\TransitionInterface[]
   *   An array of workflow transitions on success, otherwise an empty array.
   */
  protected function getTransitions(ContentEntityInterface $entity): array {
    $transitions = [];
    $workflow = $this->moderationInformation->getWorkflowForEntity($entity);

    if ($workflow instanceof WorkflowInterface) {
      $workflow_type = $workflow->getTypePlugin();
      if ($workflow_type instanceof ContentModerationInterface) {
        $transitions = $workflow_type->getTransitions();
      }
    }

    return $transitions;
  }

}
