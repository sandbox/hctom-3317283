<?php

namespace Drupal\content_moderation_links\Hook;

/**
 * Implements hook_module_implements_alter().
 */
class ModuleImplementsAlterHook {

  /**
   * Alter the registry of modules implementing a hook.
   *
   * Moves content_moderation_links_entity_type_alter() to the end of the list
   * of modules implementing the hook_entity_type_alter() hook.
   *
   * @param array $implementations
   *   An array keyed by the module's name. The value of each item corresponds
   *   to a $group, which is usually FALSE, unless the implementation is in a
   *   file named $module.$group.inc.
   * @param string $hook
   *   The name of the module hook being implemented.
   *
   * @see \hook_module_implements_alter()
   * @see \content_moderation_links_module_implements_alter()
   */
  public function moduleImplementsAlter(array &$implementations, string $hook): void {
    if ($hook === 'entity_type_alter' && isset($implementations['content_moderation_links'])) {
      // Move content_moderation_links_entity_type_alter() to the end of the
      // list. \Drupal::moduleHandler()->getImplementationInfo() iterates
      // through $implementations with a foreach loop which PHP iterates in the
      // order that the items were added, so to move an item to the end of the
      // array, we remove it and then add it.
      $group = $implementations['content_moderation_links'];
      unset($implementations['content_moderation_links']);
      $implementations['content_moderation_links'] = $group;
    }
  }

}
