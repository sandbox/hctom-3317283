<?php

namespace Drupal\content_moderation_links\Hook;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_usage\EntityUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements hook_form_alter().
 */
class FormAlterHook implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity usage statistics.
   */
  protected EntityUsageInterface $entityUsage;

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected UrlGeneratorInterface $urlGenerator;

  /**
   * Constructs a new FormAlterHook object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\entity_usage\EntityUsageInterface $entity_usage
   *   The entity usage statistics.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   THe URL generator.
   *
   * @codeCoverageIgnore
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityUsageInterface $entity_usage, UrlGeneratorInterface $url_generator) {
    $this->configFactory = $config_factory;
    $this->entityUsage = $entity_usage;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container): FormAlterHook {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('config.factory'),
      $container->get('entity_usage.usage'),
      $container->get('url_generator'),
    );
  }

  /**
   * Entity usage data exists?
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Whether entity usage data exists for given entity.
   */
  protected function entityUsageDataExists(EntityInterface $entity): bool {
    $usage_data = $this->entityUsage->listSources($entity);

    if (count($usage_data) === 0) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Entity usage notice is enabled?
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Whether entity usage notice is enabled for given entity.
   */
  protected function entityUsageNoticeIsEnabledForEntity(EntityInterface $entity): bool {
    $config = $this->configFactory->get('entity_usage.settings');
    $edit_entity_types = $config->get('edit_warning_message_entity_types') ?: [];
    $edit_entity_types = is_array($edit_entity_types) ? $edit_entity_types : [];

    // Entity is configured to show message?
    if (in_array($entity->getEntityTypeId(), $edit_entity_types, TRUE)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Perform alterations before a form is rendered.
   *
   * Applies the entity usage warning message to content moderation link confirm
   * forms.
   *
   * @todo This should be refactored, once entity_usage module provides an API
   *   for displaying these message. For now it is a modified copy of the
   *   original source code.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   A string that is the unique ID of the form.
   *
   * @see \hook_form_alter()
   * @see \content_moderation_links_form_alter()
   */
  public function formAlter(array &$form, FormStateInterface $form_state, string $form_id): void {
    $entity = $this->getEntity($form_state);

    if (!($entity instanceof EntityInterface)) {
      return;
    }

    if ($this->shouldHaveEntityUsageNotice($form_state)) {
      $entity_usage_url = $this->urlGenerator->generateFromRoute('entity_usage.usage_list', [
        'entity_type' => $entity->getEntityTypeId(),
        'entity_id' => $entity->id(),
      ]);

      $form['entity_usage_edit_warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            $this->t('Modifications on this form will affect all <a href="@usage_url" target="_blank">existing usages</a> of this entity.', [
              '@usage_url' => $entity_usage_url,
            ]),
          ],
        ],
        '#status_headings' => [
          'warning' => $this->t('Warning message'),
        ],
        '#weight' => -201,
      ];
    }
  }

  /**
   * Returns the entity the form is for (if any).
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity on success, otherwise NULL.
   */
  protected function getEntity(FormStateInterface $form_state): ?EntityInterface {
    $form_object = $form_state->getFormObject();

    if (!($form_object instanceof EntityFormInterface)) {
      return NULL;
    }

    return $form_object->getEntity();
  }

  /**
   * Should this form have the entity usage notice?
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Whether this form should have the entity usage notice.
   */
  protected function shouldHaveEntityUsageNotice(FormStateInterface $form_state): bool {
    $form_object = $form_state->getFormObject();

    if ($form_object instanceof EntityFormInterface) {
      $entity = $this->getEntity($form_state);

      if (!($entity instanceof EntityInterface)) {
        return FALSE;
      }

      // Abort early if this entity is not configured to show any message.
      if (!$this->entityUsageNoticeIsEnabledForEntity($entity)) {
        return FALSE;
      }

      // No usage data recorded so far?
      if (!$this->entityUsageDataExists($entity)) {
        return FALSE;
      }

      $operations = [
        LinkTemplate::DISCARD_LATEST_VERSION,
        LinkTemplate::MODERATION_STATE_TRANSITION,
      ];

      // Check for the edit warning.
      if (in_array($form_object->getOperation(), $operations, TRUE)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
