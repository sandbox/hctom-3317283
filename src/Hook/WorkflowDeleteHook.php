<?php

namespace Drupal\content_moderation_links\Hook;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
class WorkflowDeleteHook {

  /**
   * Respond to entity deletion of a particular type.
   *
   * This hook runs once the entity has been deleted from the storage.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object for the entity that has been deleted.
   *
   * @see \hook_ENTITY_TYPE_delete()
   * @see \content_moderation_links_workflow_delete()
   */
  public function workflowDelete(EntityInterface $entity): void {
    // Invalidate local actions cache when workflow is deleted.
    Cache::invalidateTags([
      'local_action',
    ]);
  }

}
