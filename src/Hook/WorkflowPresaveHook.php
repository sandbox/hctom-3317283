<?php

namespace Drupal\content_moderation_links\Hook;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
class WorkflowPresaveHook {

  /**
   * Act on a specific type of entity before it is created or updated.
   *
   * You can get the original entity object from $entity->original when it is an
   * update of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @see \hook_ENTITY_TYPE_presave()
   * @see \content_moderation_links_workflow_presave()
   */
  public function workflowPresave(EntityInterface $entity): void {
    // Invalidate local actions cache on workflow changes.
    Cache::invalidateTags([
      'local_action',
    ]);
  }

}
