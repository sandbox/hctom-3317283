<?php

namespace Drupal\content_moderation_links\Hook;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\Entity\Routing\ModerationLinksRouteSubscriber;
use Drupal\content_moderation_links\Form\ModerationStateTransitionConfirmForm;
use Drupal\content_moderation_links\Form\DiscardLatestVersionConfirmForm;
use Drupal\Core\Entity\ContentEntityTypeInterface;

/**
 * Implements hook_entity_type_alter().
 */
class EntityTypeAlterHook {

  /**
   * Alter the entity type definitions.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   An associative array of all entity type definitions, keyed by the entity
   *   type name. Passed by reference.
   *
   * @see \hook_entity_type_alter()
   * @see \content_moderation_links_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types): void {
    // Internal entity types should never be moderated, and the 'path_alias'
    // entity type needs to be excluded for now.
    // @todo Enable moderation for path aliases after they become publishable
    //   in https://www.drupal.org/project/drupal/issues/3007669.
    // Workspace entities can not be moderated because they use string IDs.
    // @see \Drupal\content_moderation\Entity\ContentModerationState::baseFieldDefinitions()
    // where the target entity ID is defined as an integer.
    $entity_type_to_exclude = [
      'path_alias',
      'workspace',
    ];

    foreach ($entity_types as $entity_type) {
      $entity_type_id = $entity_type->id();

      if ($entity_type instanceof ContentEntityTypeInterface && $entity_type->isRevisionable() && !$entity_type->isInternal() && !in_array($entity_type_id, $entity_type_to_exclude)) {
        $entity_types[$entity_type_id] = $this->addModerationLinksToEntityType($entity_type);
      }
    }
  }

  /**
   * Modifies an entity definition to include moderation links support.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type
   *   The content entity definition to modify.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface
   *   The modified content entity definition.
   */
  protected function addModerationLinksToEntityType(ContentEntityTypeInterface $entity_type): ContentEntityTypeInterface {
    // Link template: Discard latest version.
    if ($entity_type->hasLinkTemplate('latest-version') && !$entity_type->hasLinkTemplate(LinkTemplate::DISCARD_LATEST_VERSION)) {
      $entity_type->setLinkTemplate(LinkTemplate::DISCARD_LATEST_VERSION, $entity_type->getLinkTemplate('latest-version') . '/discard');
    }

    // Link template: Content moderation state transition.
    $entity_type->setLinkTemplate(LinkTemplate::MODERATION_STATE_TRANSITION, $entity_type->getLinkTemplate('canonical') . '/transition/from/{from}/to/{to}');

    // Form class: Discard latest version.
    $entity_type->setFormClass(LinkTemplate::DISCARD_LATEST_VERSION, DiscardLatestVersionConfirmForm::class);

    // Form class: Content moderation state transition.
    $entity_type->setFormClass(LinkTemplate::MODERATION_STATE_TRANSITION, ModerationStateTransitionConfirmForm::class);

    $providers = $entity_type->getRouteProviderClasses() ?: [];
    $providers['content_moderation_links'] = ModerationLinksRouteSubscriber::class;
    $entity_type->setHandlerClass('route_provider', $providers);

    return $entity_type;
  }

}
