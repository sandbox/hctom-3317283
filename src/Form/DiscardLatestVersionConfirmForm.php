<?php

namespace Drupal\content_moderation_links\Form;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the content moderation link form to discard latest entity version.
 */
class DiscardLatestVersionConfirmForm extends ModerationLinkConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entity = $this->getEntity();

    // Special handling if restored as latest version. In that case the
    // canonical route is the actual 'latest_version' route.
    if ($entity instanceof ContentEntityInterface && $this->moderatedEntityHelper->isRestoredAsLatestVersion($entity)) {
      return $entity->toUrl('canonical');
    }

    return $entity->toUrl('latest-version');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Discard latest version');
  }

  /**
   * {@inheritdoc}
   */
  protected function getRevisionLogMessageDefault(): string {
    return $this->t('Discard latest version');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will back up and discard the current pending revision(s).');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to discard the latest version of %label?', [
      '%label' => $this->getEntity()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->getEntity();
    assert($entity instanceof ContentEntityInterface);

    // Determine revision log message to use?
    $revision_log_message = $this->getRevisionLogMessage($form_state);

    // Back up and discard latest version.
    $success = $this->moderatedEntityHelper->discardLatestVersion($entity, $revision_log_message);

    // Display message to user.
    $this->submitFormMessage($success);

    // Redirect to entity's canonical URL.
    $form_state->setRedirectUrl($entity->toUrl());
  }

  /**
   * Displays a message to the user after submission.
   *
   * In addition, the submission status is also logged.
   *
   * @param bool $success
   *   Whether the process was successful.
   */
  protected function submitFormMessage(bool $success): void {
    $entity = $this->getEntity();
    assert($entity instanceof ContentEntityInterface);
    $entity_type_id = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $bundle = $entity->bundle();
    $label = $entity->label();

    $messenger = $this->messenger();
    $logger = $this->logger($entity_type_id);
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

    $context = [
      '@type' => $bundle,
      '%label' => $label,
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    $t_args = [
      '@type' => $bundle_info[$bundle]['label'] ?? 'undefined',
      '%label' => $entity->toLink($label)->toString(),
      '@revisions' => $this->urlGenerator->generateFromRoute('entity.' . $entity_type_id . '.version_history', [
        $entity_type_id => $entity_id,
      ]),
    ];

    if ($success) {
      $logger->notice('@type: backed up and discarded latest version of %label.', $context);
      $messenger->addStatus($this->t('Latest version of @type %label has been <a href="@revisions">backed up</a> and discarded.', $t_args));
    }
    else {
      $logger->error('@type: unable to back up and discard latest version of %label.', $context);
      $messenger->addError($this->t('Unable to back up and discard latest version of @type %label. Please try again later.', $t_args));
    }
  }

}
