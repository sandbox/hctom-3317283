<?php

namespace Drupal\content_moderation_links\Form;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_links\ModeratedEntityHelperInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for content moderation link confirm forms.
 */
abstract class ModerationLinkConfirmFormBase extends ContentEntityConfirmFormBase {

  /**
   * The moderated entity helper.
   */
  protected ModeratedEntityHelperInterface $moderatedEntityHelper;

  /**
   * The moderation information.
   */
  protected ModerationInformationInterface $moderationInformation;

  /**
   * The URL generator.
   */
  protected UrlGeneratorInterface $urlGenerator;

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\content_moderation_links\Form\ModerationLinkConfirmFormBase::buildFormBase()
   * @see \Drupal\content_moderation_links\Form\ModerationLinkConfirmFormBase::buildFormParent()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return $this->buildFormBase($form, $form_state);
  }

  /**
   * Actual form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   *
   * @see \Drupal\content_moderation_links\Form\ModerationLinkConfirmFormBase::buildForm()
   */
  protected function buildFormBase(array $form, FormStateInterface $form_state): array {
    $form = $this->buildFormParent($form, $form_state);

    $default_message = $this->getRevisionLogMessageDefault();

    // Default revision log message toggle.
    $form['default_revision_log_message_toggle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use default revision log message'),
      '#description' => $this->t('Uncheck this to provide a custom revision log message instead of the default %message message.', [
        '%message' => $default_message,
      ]),
      '#default_value' => TRUE,
    ];

    // Revision log message.
    $form['revision_log_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Revision log message'),
      '#description' => $this->t('Briefly describe the changes you have made. Leave empty to use default %message message.', [
        '%message' => $default_message,
      ]),
      '#states' => [
        'visible' => [
          ':input[name="default_revision_log_message_toggle"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Form constructor in parent class.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   *
   * @codeCoverageIgnore
   */
  public function buildFormParent(array $form, FormStateInterface $form_state): array {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container): ModerationLinkConfirmFormBase {
    $instance = parent::create($container);

    // Inject dependencies.
    $instance->setModeratedEntityHelper($container->get('content_moderation_links.moderated_entity_helper'));
    $instance->setModerationInformation($container->get('content_moderation.moderation_information'));
    $instance->setUrlGenerator($container->get('url_generator'));

    return $instance;
  }

  /**
   * Returns the revision log message.
   *
   * @return string
   *   The revision log message.
   */
  protected function getRevisionLogMessage(FormStateInterface $form_state): string {
    $revision_log_message = $this->getRevisionLogMessageCustom($form_state);

    if (isset($revision_log_message)) {
      return $revision_log_message;
    }

    return $this->getRevisionLogMessageDefault();
  }

  /**
   * Returns the custom revision log message (if any).
   *
   * @return string|null
   *   The custom revision log message on success, otherwise NULL.
   */
  protected function getRevisionLogMessageCustom(FormStateInterface $form_state): ?string {
    $revision_log_message = NULL;

    if (!$form_state->getValue('default_revision_log_message_toggle')) {
      $revision_log_message = $form_state->getValue('revision_log_message');

      if (is_string($revision_log_message)) {
        $revision_log_message = trim($revision_log_message);
        $revision_log_message = strlen($revision_log_message) > 0 ? $revision_log_message : NULL;
      }
      else {
        $revision_log_message = NULL;
      }
    }

    return $revision_log_message;
  }

  /**
   * Returns the default revision log message.
   *
   * @return string
   *   The default revision log message.
   */
  abstract protected function getRevisionLogMessageDefault(): string;

  /**
   * Sets the moderated entity helper.
   *
   * @param \Drupal\content_moderation_links\ModeratedEntityHelperInterface $moderated_entity_helper
   *   The moderated entity helper.
   */
  public function setModeratedEntityHelper(ModeratedEntityHelperInterface $moderated_entity_helper): void {
    $this->moderatedEntityHelper = $moderated_entity_helper;
  }

  /**
   * Sets the moderation information service.
   *
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information service.
   */
  public function setModerationInformation(ModerationInformationInterface $moderation_information): void {
    $this->moderationInformation = $moderation_information;
  }

  /**
   * Sets the URL generator.
   *
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator.
   */
  public function setUrlGenerator(UrlGeneratorInterface $url_generator): void {
    $this->urlGenerator = $url_generator;
  }

}
