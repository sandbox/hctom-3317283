<?php

namespace Drupal\content_moderation_links\Form;

use Drupal\content_moderation\ContentModerationState;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\TransitionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides the content moderation link form for state transitions.
 */
class ModerationStateTransitionConfirmForm extends ModerationLinkConfirmFormBase {

  /**
   * The content moderation state to transition from.
   */
  protected ContentModerationState $fromState;

  /**
   * The workflow transition.
   */
  protected TransitionInterface $transition;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?ContentModerationState $from = NULL, ?ContentModerationState $to = NULL): array {
    if (!isset($from) || !isset($to)) {
      throw new NotFoundHttpException();
    }

    $entity = $this->getEntity();

    if (!($entity instanceof ContentEntityInterface)) {
      throw new NotFoundHttpException();
    }

    $state_to_id = $to->id();

    if (!$from->canTransitionTo($state_to_id)) {
      throw new NotFoundHttpException();
    }

    $this->fromState = $from;
    $this->transition = $from->getTransitionTo($state_to_id);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entity = $this->getEntity();

    if ($this->getFromState()->isDefaultRevisionState()) {
      return $entity->toUrl('canonical');
    }

    elseif ($entity instanceof ContentEntityInterface) {
      $is_restored_as_latest_version = $this->moderatedEntityHelper->isRestoredAsLatestVersion($entity);
      $is_not_discardable_latest_version = !$this->moderatedEntityHelper->isDiscardableLatestVersion($entity);

      if ($is_restored_as_latest_version || $is_not_discardable_latest_version) {
        return $entity->toUrl('canonical');
      }
    }

    return $entity->toUrl('latest-version');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $transition_label */
    $transition_label = $this->getTransition()->label();

    return $transition_label;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This changes the current state from %from to %to.', [
      '%from' => $this->getFromState()->label(),
      '%to' => $this->getTransition()->to()->label(),
    ]);
  }

  /**
   * Returns the content moderation state to transition from.
   *
   * @return \Drupal\content_moderation\ContentModerationState
   *   The content moderation state to transition from.
   */
  protected function getFromState(): ContentModerationState {
    return $this->fromState;
  }

  /**
   * {@inheritdoc}
   */
  protected function getRevisionLogMessageDefault(): string {
    return $this->getTransition()->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to perform the %transition transition for %label?', [
      '%label' => $this->getEntity()->label(),
      '%transition' => $this->getTransition()->label(),
    ]);
  }

  /**
   * Returns the related workflow transition.
   *
   * @return \Drupal\workflows\TransitionInterface
   *   The workflow transition.
   */
  protected function getTransition(): TransitionInterface {
    return $this->transition;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->getEntity();
    assert($entity instanceof ContentEntityInterface);
    $transition = $this->getTransition();
    $state_to = $transition->to();
    $state_to_id = $state_to->id();

    // Determine revision log message to use?
    $revision_log_message = $this->getRevisionLogMessage($form_state);

    // Perform transition.
    $success = $this->moderatedEntityHelper->performTransition($entity, $state_to_id, $revision_log_message);

    // Display message to user.
    $this->submitFormMessage($success);

    // Entity has no pending revision -> redirect to canonical.
    if (!$this->moderationInformation->hasPendingRevision($entity)) {
      $form_state->setRedirectUrl($entity->toUrl());
    }

    // Entity has pending revision -> redirect to latest version.
    else {
      $form_state->setRedirectUrl($entity->toUrl('latest-version'));
    }
  }

  /**
   * Displays a message to the user after submission.
   *
   * In addition, the submission status is also logged.
   *
   * @param bool $success
   *   Whether the process was successful.
   */
  protected function submitFormMessage(bool $success): void {
    $entity = $this->getEntity();
    assert($entity instanceof ContentEntityInterface);
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $label = $entity->label();
    $transition = $this->getTransition();
    $transition_label = $transition->label();
    $state_from = $this->getFromState();
    $state_from_label = $state_from->label();
    $state_to_label = $transition->to()->label();

    $messenger = $this->messenger();
    $logger = $this->logger($entity_type_id);
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

    $context = [
      '@type' => $bundle,
      '%label' => $label,
      '%transition' => $transition_label,
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    $t_args = [
      '@type' => $bundle_info[$bundle]['label'] ?? 'undefined',
      '%label' => $entity->toLink($label)->toString(),
      '%from' => $state_from_label,
      '%to' => $state_to_label,
    ];

    // Perform transition.
    if ($success) {
      $logger->notice('@type: performed %transition transition for %label.', $context);
      $messenger->addStatus($this->t('State of @type %label has been changed from %from to %to.', $t_args));
    }
    else {
      $logger->error('@type: unable to perform %transition transition for %label.', $context);
      $messenger->addError($this->t('Unable to change state of @type %label from %from to %to. Please try again later.', $t_args));
    }
  }

}
