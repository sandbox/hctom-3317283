<?php

namespace Drupal\content_moderation_links\Access;

use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\workflows\StateInterface;
use Drupal\workflows\WorkflowInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access for content moderation state transitions.
 */
class ModerationStateTransitionAccessCheck extends ModerationLinkAccessCheckBase {

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    $access_result = parent::access($route, $route_match, $account);

    if ($access_result->isAllowed()) {
      $entity = $this->getEntity($route, $route_match);
      assert($entity instanceof ContentEntityInterface);

      $access_result = $access_result
        // Required content moderation state data is available.
        ->andIf($this->allowedIfStateDataIsValid($entity, $route_match));

      $access_result = $access_result->isNeutral() ? $access_result : $access_result
        // Current content moderation state can transition to requested content
        // moderation state?
        ->andIf($this->allowedIfCanTransition($entity, $route_match));

      $access_result = $access_result->isNeutral() ? $access_result : $access_result
        // User is allowed to use workflow transition?
        ->andIf($this->allowedIfTransitionAllowed($entity, $route_match, $account));
    }

    return $access_result;
  }

  /**
   * Is allowed, if given moderation states are allowed to transition?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfCanTransition(ContentEntityInterface $entity, RouteMatchInterface $route_match): AccessResultInterface {
    $state_current = $this->getStateCurrent($entity);
    $state_from = $this->getStateFrom($route_match);
    $state_to = $this->getStateTo($route_match);
    assert($state_current instanceof StateInterface);
    assert($state_from instanceof StateInterface);
    assert($state_to instanceof StateInterface);

    // Current content moderation state ID is the same as requested 'from'
    // content moderation state ID?
    if ($state_current->id() !== $state_from->id()) {
      return AccessResult::neutral('Current moderation state and moderation state to transition from do not match.');
    }

    // Current content moderation state ID is not the same as requested
    // content moderation state ID?
    if ($state_current->id() === $state_to->id()) {
      return AccessResult::neutral('Current moderation state and moderation state to transition to are the same.');
    }

    // Current content moderation state can transition to requested content
    // moderation state?
    if (!$state_current->canTransitionTo($state_to->id())) {
      return AccessResult::neutral('Moderation state transition is not allowed');
    }

    return AccessResult::allowed();
  }

  /**
   * Is allowed, if content moderation state data is available and valid?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfStateDataIsValid(ContentEntityInterface $entity, RouteMatchInterface $route_match): AccessResultInterface {
    $workflow_type = $this->moderatedEntityHelper->getWorkflowType($entity);
    if (!($workflow_type instanceof ContentModerationInterface)) {
      return AccessResult::neutral('No associated workflow type.');
    }

    // Determine entity's current content moderation state.
    $state_current = $this->getStateCurrent($entity);

    // Determine content moderation states to transition from/to.
    $state_from = $this->getStateFrom($route_match);
    $state_to = $this->getStateTo($route_match);

    // Entity's current content moderation state exists?
    if (!($state_current instanceof StateInterface)) {
      return AccessResult::neutral('Unable to determine current moderation state.');
    }

    // Moderation state to transition from exists?
    if (!($state_from instanceof StateInterface)) {
      return AccessResult::neutral('Unable to determine moderation state to transition from.');
    }

    // Moderation state to transition to exists?
    if (!($state_to instanceof StateInterface)) {
      return AccessResult::neutral('Unable to determine moderation state to transition to.');
    }

    // Moderation state to transition from exists in workflow?
    if (!$workflow_type->hasState($state_from->id())) {
      return AccessResult::neutral('Moderation state to transition from does not exist in workflow.');
    }

    // Moderation state to transition to exists in workflow?
    if (!$workflow_type->hasState($state_to->id())) {
      return AccessResult::neutral('Moderation state to transition to does not exist in workflow.');
    }

    return AccessResult::allowed();
  }

  /**
   * Is allowed, if user is allowed to use workflow transition?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function allowedIfTransitionAllowed(ContentEntityInterface $entity, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    // Determine workflow.
    $workflow = $this->moderatedEntityHelper->getWorkFlow($entity);
    if (!($workflow instanceof WorkflowInterface)) {
      return AccessResult::neutral('No associated workflow.');
    }

    $state_current = $this->getStateCurrent($entity);
    $state_to = $this->getStateTo($route_match);
    assert($state_current instanceof StateInterface);
    assert($state_to instanceof StateInterface);

    return AccessResult::allowedIfHasPermission($account, sprintf('use %s transition %s', $workflow->id(), $state_current->getTransitionTo($state_to->id())->id()));
  }

  /**
   * Returns the current content moderation state.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\workflows\StateInterface|null
   *   The current content moderation state of the given entity on success,
   *   otherwise NULL.
   */
  protected function getStateCurrent(ContentEntityInterface $entity): ?StateInterface {
    return $this->moderatedEntityHelper->getCurrentModerationState($entity);
  }

  /**
   * Returns the content moderation state to transition from.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   *
   * @return \Drupal\workflows\StateInterface|null
   *   The content moderation state to transition from, otherwise NULL.
   */
  protected function getStateFrom(RouteMatchInterface $route_match): ?StateInterface {
    $state = $route_match->getParameter('from');

    return $state instanceof StateInterface ? $state : NULL;
  }

  /**
   * Returns the content moderation state to transition to.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   *
   * @return \Drupal\workflows\StateInterface|null
   *   The content moderation state to transition to, otherwise NULL.
   */
  protected function getStateTo(RouteMatchInterface $route_match): ?StateInterface {
    $state = $route_match->getParameter('to');

    return $state instanceof StateInterface ? $state : NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function isAllowedOnCanonicalRoute(ContentEntityInterface $entity): bool {
    if ($this->moderationInformation->hasPendingRevision($entity)) {
      return FALSE;
    }

    // When media entities do not have a standalone URL, the entity form is the
    // canonical route -> remove local actions.
    elseif ($entity->getEntityTypeId() === 'media') {
      $route = $this->currentRouteMatch->getRouteObject();

      return !isset($route) || !$route->hasDefault('_entity_form');
    }

    return TRUE;
  }

}
