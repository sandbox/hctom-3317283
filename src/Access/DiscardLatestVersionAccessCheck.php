<?php

namespace Drupal\content_moderation_links\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\workflows\WorkflowInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to discard latest entity version.
 */
class DiscardLatestVersionAccessCheck extends ModerationLinkAccessCheckBase {

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    $access_result = parent::access($route, $route_match, $account);

    if ($access_result->isAllowed()) {
      $entity = $this->getEntity($route, $route_match);
      assert($entity instanceof ContentEntityInterface);

      // Determine workflow.
      $workflow = $this->moderationInformation->getWorkflowForEntity($entity);
      if (!($workflow instanceof WorkflowInterface)) {
        return AccessResult::neutral('No associated workflow.')->addCacheableDependency($access_result);
      }

      $access_result = $access_result
        // Has access to latest entity version?
        ->andIf($this->allowedIfLatestVersionAccess($entity, $account))
        // Is allowed to discard latest version in entity's workflow?
        ->andIf($this->allowedIfHasDiscardPermission($workflow, $account))
        // Entity has discardable latest version?
        ->andIf($this->allowedIfDiscardableLatestVersion($entity));
    }

    return $access_result;
  }

  /**
   * Is allowed, if entity is a discardable latest version?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfDiscardableLatestVersion(ContentEntityInterface $entity): AccessResultInterface {
    return AccessResult::allowedIf($this->moderatedEntityHelper->isDiscardableLatestVersion($entity));
  }

  /**
   * Is allowed, if user is allowed to discard latest version?
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfHasDiscardPermission(WorkflowInterface $workflow, AccountInterface $account): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, sprintf('discard latest version in %s workflow', $workflow->id()));
  }

  /**
   * Is allowed, if user is allowed to access latest version?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfLatestVersionAccess(ContentEntityInterface $entity, AccountInterface $account): AccessResultInterface {
    // Check latest version access permissions.
    $access_result = AccessResult::allowedIfHasPermissions($account, [
      'view latest version',
      'view any unpublished content',
    ]);

    if (!$access_result->isAllowed() && $entity instanceof EntityOwnerInterface) {
      // Check entity owner access.
      $access_result_owner = AccessResult::allowedIf($entity->getOwnerId() === $account->id())
        ->andIf(AccessResult::allowedIfHasPermissions($account, [
          'view latest version',
          'view own unpublished content',
        ]));

      $access_result = $access_result->orIf($access_result_owner);
    }

    return $access_result;
  }

  /**
   * {@inheritdoc}
   */
  protected function isAllowedOnCanonicalRoute(ContentEntityInterface $entity): bool {
    // Entity is restored as latest version?
    if ($this->moderatedEntityHelper->isRestoredAsLatestVersion($entity)) {
      return TRUE;
    }

    return FALSE;
  }

}
