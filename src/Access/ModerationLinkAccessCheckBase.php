<?php

namespace Drupal\content_moderation_links\Access;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_links\ModeratedEntityHelperInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\workflows\WorkflowInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides a base class for content moderation link access checks.
 */
abstract class ModerationLinkAccessCheckBase implements AccessInterface {

  /**
   * The current route match.
   */
  protected RouteMatchInterface $currentRouteMatch;

  /**
   * The moderated entity helper.
   */
  protected ModeratedEntityHelperInterface $moderatedEntityHelper;

  /**
   * The moderation information.
   */
  protected ModerationInformationInterface $moderationInformation;

  /**
   * Constructs a ModerationLinkAccessCheckBase object.
   *
   * @param \Drupal\content_moderation_links\ModeratedEntityHelperInterface $moderated_entity_helper
   *   The moderated entity helper.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match.
   *
   * @codeCoverageIgnore
   */
  public function __construct(ModeratedEntityHelperInterface $moderated_entity_helper, ModerationInformationInterface $moderation_information, RouteMatchInterface $current_route_match) {
    $this->currentRouteMatch = $current_route_match;
    $this->moderatedEntityHelper = $moderated_entity_helper;
    $this->moderationInformation = $moderation_information;
  }

  /**
   * Checks access for content moderation links.
   *
   * This method should be overridden and extended in inheriting classes.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see \Drupal\content_moderation_links\Access\ModerationLinkAccessCheckBase::checkAccess()
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    return $this->checkAccess($route, $route_match, $account);
  }

  /**
   * Is allowed, if on entity's canonical route?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfEntityCanonicalRoute(ContentEntityInterface $entity): AccessResultInterface {
    $access_result = AccessResult::allowed();

    if ($this->currentRouteMatch->getRouteName() === $entity->toUrl('canonical')->getRouteName()) {
      // Special handling for entity's canonical route.
      $access_result = $access_result->andIf(AccessResult::allowedIf($this->isAllowedOnCanonicalRoute($entity)));

      // Cache per route on entity's canonical route.
      if ($access_result instanceof RefinableCacheableDependencyInterface) {
        $access_result = $access_result->addCacheContexts([
          'route.name',
        ]);
      }
    }

    return $access_result;
  }

  /**
   * Is allowed, if entity update is allowed?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfEntityUpdateAllowed(ContentEntityInterface $entity, AccountInterface $account): AccessResultInterface {
    return $entity->access('update', $account, TRUE);
  }

  /**
   * Is allowed, if entity is moderated?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function allowedIfModeratedEntity(ContentEntityInterface $entity): AccessResultInterface {
    return AccessResult::allowedIf($this->moderationInformation->isModeratedEntity($entity));
  }

  /**
   * Checks access for content moderation links.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see \Drupal\content_moderation_links\Access\ModerationLinkAccessCheckBase::access()
   */
  protected function checkAccess(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    // Get entity from route parameters.
    $entity = $this->getEntity($route, $route_match);

    // Abort early if not a content entity.
    if (!($entity instanceof ContentEntityInterface)) {
      return AccessResult::neutral('Not a content entity.');
    }

    // Is moderated entity?
    $access_result = $this->allowedIfModeratedEntity($entity)
      // Require 'update' access for entity.
      ->andIf($this->allowedIfEntityUpdateAllowed($entity, $account))
      // Special handling for entity's canonical route.
      ->andIf($this->allowedIfEntityCanonicalRoute($entity));

    // Merge workflow information (if any).
    $access_result = $this->mergeWorkflow($access_result, $entity);

    // Adjust cache metadata.
    if ($access_result instanceof RefinableCacheableDependencyInterface) {
      // Ensure that access is evaluated again when the entity changes.
      $access_result = $access_result
        ->addCacheableDependency($entity);
    }

    return $access_result;
  }

  /**
   * Gets the content entity object from route parameters.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to get the content entity object from.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The content entity object on success, otherwise NULL.
   */
  protected function getEntity(Route $route, RouteMatchInterface $route_match): ?ContentEntityInterface {
    $entity_type_id = $route->getOption('_content_moderation_links_entity_type');

    if (isset($entity_type_id) && is_string($entity_type_id)) {
      if (($entity = $route_match->getParameter($entity_type_id)) && $entity instanceof ContentEntityInterface) {
        return $entity;
      }
    }

    return NULL;
  }

  /**
   * Content moderation link is allowed on entity's canonical route?
   *
   * Content moderation links are allowed everywhere by default. Only canonical
   * routes of entities may need some special logic, because they are also the
   * latest version display under certain circumstances.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return bool
   *   Whether content moderation link is allowed on entity's canonical route.
   */
  abstract protected function isAllowedOnCanonicalRoute(ContentEntityInterface $entity): bool;

  /**
   * Merge associated workflow metadata (if any).
   *
   * @param \Drupal\Core\Access\AccessResultInterface $access_result
   *   The access result to merge the data into.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The processed access result.
   */
  protected function mergeWorkflow(AccessResultInterface $access_result, ContentEntityInterface $entity): AccessResultInterface {
    // Determine entity's workflow for cache metadata.
    $workflow = $this->moderationInformation->getWorkflowForEntity($entity);
    if (!($workflow instanceof WorkflowInterface)) {
      return $access_result;
    }

    if ($access_result instanceof RefinableCacheableDependencyInterface) {
      // Ensure that access is evaluated again when the workflow changes.
      $access_result = $access_result
        ->addCacheableDependency($workflow);
    }

    return $access_result;
  }

}
