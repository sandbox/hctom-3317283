<?php

namespace Drupal\content_moderation_links;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\StateInterface;
use Drupal\workflows\TransitionInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Provides the moderated entity helper service.
 */
class ModeratedEntityHelper implements ModeratedEntityHelperInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   */
  protected AccountInterface $currentUser;

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * The entity repository.
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The moderation information.
   */
  protected ModerationInformationInterface $moderationInformation;

  /**
   * The time service.
   */
  protected TimeInterface $time;

  /**
   * Constructs a new ModerationHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, ModerationInformationInterface $moderation_information, Connection $database, AccountInterface $current_user, TimeInterface $time) {
    $this->currentUser = $current_user;
    $this->database = $database;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->moderationInformation = $moderation_information;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function discardLatestVersion(ContentEntityInterface $entity, ?string $revision_log_message = NULL): bool {
    // Revision with content moderation state with default revision state
    // exists?
    if (!$this->hasRevisionInDefaultRevisionState($entity)) {
      return FALSE;
    }

    $default_revision = NULL;

    // Determine most recent revision with content moderation state with
    // default revision state. This has to be done manually instead of using
    // the getDefaultRevisionId() method of the moderation information service
    // that returns latest version revisions as default revision (e.g. when
    // restored as latest version).
    foreach ($this->getRevisions($entity) as $revision) {
      $state = $this->getCurrentModerationState($revision);

      if (isset($state) && $state->isDefaultRevisionState()) {
        $default_revision = $revision;
        break;
      }
    }

    // Discard latest version by creating a copy of the most recent revision
    // with content moderation state with default revision state at the top of
    // the revision list.
    if (isset($default_revision)) {
      $revision = $this->prepareNewRevision($default_revision, $revision_log_message);
      $revision->save();

      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentModerationState(ContentEntityInterface $entity): ?ContentModerationState {
    $workflow_type = $this->getWorkflowType($entity);
    $moderation_state_value = $this->getModerationStateValueFromEntity($entity);

    if (isset($workflow_type) && isset($moderation_state_value)) {
      $state = $workflow_type->getState($moderation_state_value);

      return $state instanceof ContentModerationState ? $state : NULL;
    }

    return NULL;
  }

  /**
   * Returns the moderation state value from the given entity (if any).
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity interface.
   *
   * @return string|null
   *   The moderation state value on success, otherwise NULL.
   */
  protected function getModerationStateValueFromEntity(ContentEntityInterface $entity): ?string {
    return isset($entity->moderation_state) && isset($entity->moderation_state->value) && is_string($entity->moderation_state->value) ? $entity->moderation_state->value : NULL;
  }

  /**
   * Returns all content moderation states with default revision state.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity interface.
   *
   * @return \Drupal\workflows\StateInterface[]
   *   An array of content moderation states keyed by state ID.
   */
  protected function getModerationStatesWithDefaultRevisionState(ContentEntityInterface $entity): array {
    $workflow_type = $this->getWorkflowType($entity);

    if (!isset($workflow_type)) {
      return [];
    }

    return array_filter($workflow_type->getStates(), function (StateInterface $state): bool {
      if ($state instanceof ContentModerationState) {
        return $state->isDefaultRevisionState();
      }

      return FALSE;
    });
  }

  /**
   * Returns all language-specific revision IDs of the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return int[]
   *   An array of revision IDs.
   */
  protected function getRevisionIds(ContentEntityInterface $entity): array {
    $entity_type_id = $entity->getEntityTypeId();
    $entity_id = $entity->id();

    $cid = implode(':', [
      $entity_type_id,
      $entity_id,
    ]);

    $cache = &drupal_static(__METHOD__, []);
    assert(is_array($cache));

    if (!isset($cache[$cid])) {
      $cache[$cid] = [];

      $query = $this->queryGetRevisionIds($entity);

      if ($query instanceof SelectInterface) {
        $result = $query->execute();

        if ($result instanceof StatementInterface) {
          $revision_ids = $result->fetchCol();

          if (is_array($revision_ids)) {
            $cache[$cid] = $revision_ids;
          }
        }
      }
    }

    return is_array($cache[$cid]) ? $cache[$cid] : [];
  }

  /**
   * Returns all language-specific revisions of the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   An array of content entity revisions.
   */
  protected function getRevisions(ContentEntityInterface $entity): array {
    $revisions = [];
    $entity_type_id = $entity->getEntityTypeId();
    $storage = $this->entityTypeManager->getStorage($entity_type_id);

    if ($storage instanceof RevisionableStorageInterface) {
      $langcode = $entity->language()->getId();
      $revision_ids = $this->getRevisionIds($entity);
      $revisions = count($revision_ids) > 0 ? $storage->loadMultipleRevisions($revision_ids) : [];

      $revisions = array_map(function (EntityInterface $revision) use ($langcode): ContentEntityInterface {
        $revision = $this->entityRepository->getTranslationFromContext($revision, $langcode);
        assert($revision instanceof ContentEntityInterface);

        return $revision;
      }, $revisions);
    }

    return $revisions;
  }

  /**
   * Returns the transition from current moderation state to given state ID.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity interface.
   * @param string $state_to_id
   *   The ID of the content moderation state to transition to.
   *
   * @return \Drupal\workflows\TransitionInterface|null
   *   The workflow transition on success, otherwise NULL.
   */
  public function getTransitionFromCurrentStateTo(ContentEntityInterface $entity, string $state_to_id): ?TransitionInterface {
    $workflow = $this->moderationInformation->getWorkflowForEntity($entity);
    if (!($workflow instanceof WorkflowInterface)) {
      return NULL;
    }

    $workflow_type = $workflow->getTypePlugin();
    if (!($workflow_type instanceof ContentModerationInterface)) {
      return NULL;
    }

    // Validate if state change is allowed by a transition.
    $moderation_state_value = $this->getModerationStateValueFromEntity($entity);
    if (isset($moderation_state_value)) {
      if (!$workflow_type->hasTransitionFromStateToState($moderation_state_value, $state_to_id)) {
        return NULL;
      }
    }
    else {
      return NULL;
    }

    return $workflow_type->getTransitionFromStateToState($moderation_state_value, $state_to_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkflow(ContentEntityInterface $entity): ?WorkflowInterface {
    $workflow = $this->moderationInformation->getWorkflowForEntity($entity);

    return $workflow instanceof WorkflowInterface ? $workflow : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkflowType(ContentEntityInterface $entity): ?ContentModerationInterface {
    $workflow = $this->getWorkflow($entity);

    if (!isset($workflow)) {
      return NULL;
    }

    $workflow_type = $workflow->getTypePlugin();

    return $workflow_type instanceof ContentModerationInterface ? $workflow_type : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRevisionInDefaultRevisionState(ContentEntityInterface $entity): bool {
    $entity_type_id = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $entity_revision_id = $entity->getRevisionId();

    $cid = implode(':', [
      $entity_type_id,
      $entity_id,
      // TODO Really needed?
      $entity_revision_id,
    ]);

    $cache = &drupal_static(__METHOD__, []);
    assert(is_array($cache));

    if (!isset($cache[$cid])) {
      $cache[$cid] = FALSE;
      $workflow = $this->getWorkflow($entity);

      if (isset($workflow)) {
        $query = $this->queryHasRevisionInDefaultRevisionState($entity, $workflow);

        if ($query instanceof SelectInterface) {
          $result = $query->countQuery()->execute();

          if (isset($result)) {
            $cache[$cid] = $result->fetchField() > 0;
          }
        }
      }
    }

    return !!$cache[$cid];
  }

  /**
   * {@inheritdoc}
   */
  public function isDiscardableLatestVersion(ContentEntityInterface $entity): bool {
    assert($entity instanceof EntityPublishedInterface);

    // Entity is published -> no need to discard a latest version.
    if ($entity->isPublished()) {
      return FALSE;
    }

    // Determine current moderation state.
    $current_state = $this->getCurrentModerationState($entity);

    if (isset($current_state)) {
      // Is default revision state?
      if ($current_state->isDefaultRevisionState()) {
        return FALSE;
      }

      // Has pending revision?
      if ($this->moderationInformation->hasPendingRevision($entity)) {
        return TRUE;
      }

      // Has revision in default revision state?
      return $this->hasRevisionInDefaultRevisionState($entity);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isRestoredAsLatestVersion(ContentEntityInterface $entity): bool {
    $current_state = $this->getCurrentModerationState($entity);

    if (isset($current_state)) {
      if (!$this->moderationInformation->hasPendingRevision($entity) && !$current_state->isDefaultRevisionState() && $this->hasRevisionInDefaultRevisionState($entity)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function performTransition(ContentEntityInterface $entity, string $state_to_id, ?string $revision_log_message = NULL): bool {
    $transition = $this->getTransitionFromCurrentStateTo($entity, $state_to_id);
    if (!($transition instanceof TransitionInterface)) {
      return FALSE;
    }

    $state = $transition->to();
    if (!($state instanceof ContentModerationState)) {
      return FALSE;
    }

    $revision = $this->prepareNewRevision($entity, $revision_log_message);
    $revision->isDefaultRevision($state->isDefaultRevisionState());
    $revision->set('moderation_state', $state->id());
    $revision->save();

    return TRUE;
  }

  /**
   * Prepares a new revision of a given content entity, if applicable.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   A content entity.
   * @param string|null $revision_log_message
   *   Optional custom revision log message.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The new entity revision.
   */
  protected function prepareNewRevision(ContentEntityInterface $entity, ?string $revision_log_message = NULL): ContentEntityInterface {
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());

    if ($storage instanceof ContentEntityStorageInterface) {
      $revision = $storage->createRevision($entity);
      assert($revision instanceof ContentEntityInterface);

      if ($revision instanceof RevisionLogInterface) {
        if (isset($revision_log_message)) {
          $revision->setRevisionLogMessage($revision_log_message);
        }

        $revision->setRevisionCreationTime($this->time->getRequestTime());
        $revision->setRevisionUserId($this->currentUser->id());
      }

      return $revision;
    }

    return $entity;
  }

  /**
   * Prepares the query for all language-specific revision IDs of given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface|null
   *   A database select query on success, otherwise NULL.
   */
  protected function queryGetRevisionIds(ContentEntityInterface $entity): ?SelectInterface {
    $query = NULL;
    $entity_type = $entity->getEntityType();

    // Entity type is not revisionable?
    if (!$entity_type->isRevisionable()) {
      return $query;
    }

    $entity_id = $entity->id();
    $entity_revision_id = $entity->getRevisionId();
    $entity_langcode = $entity->language()->getId();
    $entity_key_id = $entity_type->getKey('id') ?: NULL;
    $entity_key_revision_id = $entity_type->getKey('revision') ?: NULL;
    $entity_key_langcode = $entity_type->getKey('langcode') ?: NULL;
    $entity_revision_table = $entity_type->getRevisionTable();

    if (is_string($entity_key_id) && is_string($entity_key_revision_id) && is_string($entity_key_langcode) && is_string($entity_revision_table)) {
      $query = $this->database->select($entity_revision_table, 'r')
        ->fields('r', [
          $entity_key_revision_id,
        ])
        ->condition($entity_key_id, (string) $entity_id)
        ->condition($entity_key_revision_id, (string) $entity_revision_id, '<>')
        ->condition($entity_key_langcode, $entity_langcode)
        ->orderBy($entity_key_revision_id, 'DESC');
    }

    return $query;
  }

  /**
   * Prepares the query for if entity has revision in default revision state?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param \Drupal\workflows\WorkflowInterface $workflow
   *   The workflow object.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface|null
   *   A database select query on success, otherwise NULL.
   */
  protected function queryHasRevisionInDefaultRevisionState(ContentEntityInterface $entity, WorkflowInterface $workflow): ?SelectInterface {
    $query = NULL;

    $entity_type_content_moderation_state = $this->entityTypeManager->getDefinition('content_moderation_state');
    assert($entity_type_content_moderation_state instanceof EntityTypeInterface);

    $content_moderation_state_revision_data_table = $entity_type_content_moderation_state->getRevisionDataTable();

    if (!is_string($content_moderation_state_revision_data_table)) {
      return $query;
    }

    $entity_type_id = $entity->getEntityTypeId();
    $entity_id = (string) $entity->id();
    $entity_is_new_revision = $entity->isNewRevision();
    $entity_revision_id = $entity->getRevisionId();
    $entity_revision_id_loaded = $entity->getLoadedRevisionId();
    $workflow_id = (string) $workflow->id();
    $moderation_states_with_default_revision_state = $this->getModerationStatesWithDefaultRevisionState($entity);
    $langcode = $entity->language()->getId();

    $query = $this->database->select($content_moderation_state_revision_data_table)
      ->condition('workflow', $workflow_id)
      ->condition('moderation_state', count($moderation_states_with_default_revision_state) ? array_keys($moderation_states_with_default_revision_state) : [''], 'IN')
      ->condition('content_entity_type_id', $entity_type_id)
      ->condition('content_entity_id', $entity_id)
      ->condition('content_entity_revision_id', (string) ($entity_is_new_revision ? $entity_revision_id_loaded : $entity_revision_id), '<>');

    if ($entity_type_content_moderation_state->isTranslatable()) {
      $query = $query->condition('langcode', $langcode);
    }

    return $query;
  }

}
