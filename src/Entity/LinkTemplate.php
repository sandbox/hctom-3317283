<?php

namespace Drupal\content_moderation_links\Entity;

/**
 * Provides a class with entity link template names.
 *
 * @interal
 */
final class LinkTemplate {

  /**
   * Entity link template: Discard latest version.
   */
  public const DISCARD_LATEST_VERSION = 'content-moderation-links-discard-latest-version';

  /**
   * Entity link template: Moderation state transition.
   */
  public const MODERATION_STATE_TRANSITION = 'content-moderation-links-moderation-state-transition';

}
