<?php

namespace Drupal\content_moderation_links\Entity\Routing;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber for moderation links of revisionable entities.
 */
class ModerationLinksRouteSubscriber implements EntityRouteProviderInterface, EntityHandlerInterface {

  /**
   * The entity field manager.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new ModerationLinksRouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   *
   * @codeCoverageIgnore
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * Creates and returns a new route collection.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   The route collection.
   */
  protected function createRouteCollection(): RouteCollection {
    return new RouteCollection();
  }

  /**
   * Gets the type of the ID key for a given entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   An entity type.
   *
   * @return string|null
   *   The type of the ID key for a given entity type, or NULL if the entity
   *   type does not support fields.
   */
  protected function getEntityTypeIdKeyType(EntityTypeInterface $entity_type): ?string {
    if (!$entity_type->entityClassImplements(FieldableEntityInterface::class)) {
      return NULL;
    }

    $key_id = $entity_type->getKey('id');
    if ($key_id === FALSE) {
      return NULL;
    }

    $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type->id());

    if (!isset($field_storage_definitions[$key_id])) {
      return NULL;
    }

    return $field_storage_definitions[$key_id]->getType();
  }

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = $this->createRouteCollection();
    $entity_type_id = $entity_type->id();

    // Discard latest version.
    if ($route_discard_latest_version = $this->getRouteDiscardLatestVersion($entity_type)) {
      $collection->add('entity.' . $entity_type_id . '.content_moderation_links_discard_latest_version', $route_discard_latest_version);
    }

    // Content moderation state transition.
    if ($route_workflow_transition = $this->getRouteModerationStateTransition($entity_type)) {
      $collection->add('entity.' . $entity_type_id . '.content_moderation_links_moderation_state_transition', $route_workflow_transition);
    }

    return $collection;
  }

  /**
   * Gets the discard latest version route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type object.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRouteDiscardLatestVersion(EntityTypeInterface $entity_type): ?Route {
    if (!$entity_type->hasLinkTemplate(LinkTemplate::DISCARD_LATEST_VERSION)) {
      return NULL;
    }

    $link_template_discard_latest_version = $entity_type->getLinkTemplate(LinkTemplate::DISCARD_LATEST_VERSION);
    assert(is_string($link_template_discard_latest_version));

    $entity_type_id = $entity_type->id();

    $route = new Route($link_template_discard_latest_version);
    $route
      ->addDefaults([
        '_entity_form' => $entity_type_id . '.' . LinkTemplate::DISCARD_LATEST_VERSION,
        '_title' => 'Discard latest version',
      ])
      ->setRequirement('_content_moderation_links_discard_latest_version', 'TRUE')
      ->setOption('_content_moderation_links_entity_type', $entity_type_id)
      ->setOption('parameters', [
        $entity_type_id => [
          'type' => 'entity:' . $entity_type_id,
          'load_latest_revision' => TRUE,
        ],
      ]);

    // Entity types with serial IDs can specify this in their route
    // requirements, improving the matching process.
    if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
      $route->setRequirement($entity_type_id, '\d+');
    }

    return $route;
  }

  /**
   * Gets the content moderation state transition route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type object.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRouteModerationStateTransition(EntityTypeInterface $entity_type): ?Route {
    if (!$entity_type->hasLinkTemplate(LinkTemplate::MODERATION_STATE_TRANSITION)) {
      return NULL;
    }

    $link_template_moderation_state_transition = $entity_type->getLinkTemplate(LinkTemplate::MODERATION_STATE_TRANSITION);
    assert(is_string($link_template_moderation_state_transition));

    $entity_type_id = $entity_type->id();

    $route = new Route($link_template_moderation_state_transition);
    $route
      ->addDefaults([
        '_entity_form' => $entity_type_id . '.' . LinkTemplate::MODERATION_STATE_TRANSITION,
      ])
      ->setRequirement('_content_moderation_links_moderation_state_transition', 'TRUE')
      ->setOption('_content_moderation_links_entity_type', $entity_type_id)
      ->setOption('parameters', [
        $entity_type_id => [
          'type' => 'entity:' . $entity_type_id,
          'load_latest_revision' => TRUE,
        ],
        'from' => [
          'type' => 'content_moderation_links:moderation_state',
        ],
        'to' => [
          'type' => 'content_moderation_links:moderation_state',
        ],
      ]);

    // Entity types with serial IDs can specify this in their route
    // requirements, improving the matching process.
    if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
      $route->setRequirement($entity_type_id, '\d+');
    }

    return $route;
  }

}
