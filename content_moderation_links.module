<?php

/**
 * @file
 * Provides links for content moderation state transitions.
 */

use Drupal\content_moderation_links\Hook\EntityOperationHook;
use Drupal\content_moderation_links\Hook\EntityTypeAlterHook;
use Drupal\content_moderation_links\Hook\FormAlterHook;
use Drupal\content_moderation_links\Hook\ModuleImplementsAlterHook;
use Drupal\content_moderation_links\Hook\WorkflowDeleteHook;
use Drupal\content_moderation_links\Hook\WorkflowPresaveHook;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_entity_operation().
 */
function content_moderation_links_entity_operation(EntityInterface $entity): array {
  $hook = \Drupal::classResolver(EntityOperationHook::class);
  assert($hook instanceof EntityOperationHook);

  return $hook->entityOperation($entity);
}

/**
 * Implements hook_entity_type_alter().
 */
function content_moderation_links_entity_type_alter(array &$entity_types): void {
  $hook = \Drupal::classResolver(EntityTypeAlterHook::class);
  assert($hook instanceof EntityTypeAlterHook);

  $hook->entityTypeAlter($entity_types);
}

/**
 * Implements hook_form_alter().
 */
function content_moderation_links_form_alter(array &$form, FormStateInterface $form_state, string $form_id): void {
  // Check if 'entity_usage' module exists?
  if (\Drupal::moduleHandler()->moduleExists('entity_usage')) {
    $hook = \Drupal::classResolver(FormAlterHook::class);
    assert($hook instanceof FormAlterHook);

    $hook->formAlter($form, $form_state, $form_id);
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function content_moderation_links_module_implements_alter(array &$implementations, string $hook): void {
  $hook_object = \Drupal::classResolver(ModuleImplementsAlterHook::class);
  assert($hook_object instanceof ModuleImplementsAlterHook);

  $hook_object->moduleImplementsAlter($implementations, $hook);
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function content_moderation_links_workflow_presave(EntityInterface $entity): void {
  $hook = \Drupal::classResolver(WorkflowPresaveHook::class);
  assert($hook instanceof WorkflowPresaveHook);

  $hook->workflowPresave($entity);
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function content_moderation_links_workflow_delete(EntityInterface $entity): void {
  $hook = \Drupal::classResolver(WorkflowDeleteHook::class);
  assert($hook instanceof WorkflowDeleteHook);

  $hook->workflowDelete($entity);
}
