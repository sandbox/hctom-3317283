<?php

namespace Drupal\Tests\content_moderation_links\Kernel;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\token\Kernel\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\workflows\WorkflowInterface;

/**
 * Base class for content moderation link unit tests.
 */
abstract class ModerationLinkTestBase extends KernelTestBase {

  use ContentModerationTestTrait;
  use ContentTypeCreationTrait;
  use NodeCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'content_moderation_links',
    'field',
    'filter',
    'node',
    'system',
    'text',
    'user',
    'workflows',
  ];

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The moderation information service.
   */
  protected ModerationInformationInterface $moderationInformation;

  /**
   * The test node type.
   */
  protected NodeTypeInterface $nodeType;

  /**
   * The test workflow.
   */
  protected WorkflowInterface $workflow;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('user');
    $this->installEntitySchema('content_moderation_state');
    $this->installConfig([
      'content_moderation',
      'filter',
      'node',
    ]);

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->moderationInformation = $this->container->get('content_moderation.moderation_information');

    $this->nodeType = $this->createNodeType();
    $this->assertIsString($this->nodeType->id());
    $workflow = $this->moderationInformation->getWorkflowForEntityTypeAndBundle('node', $this->nodeType->id());
    $this->assertInstanceOf(WorkflowInterface::class, $workflow);
    $this->workflow = $workflow;
  }

  /**
   * Returns the IDs of all available workflow transitions.
   *
   * @return array
   *   The IDs of all available workflow transitions.
   */
  protected function getAllTransitionIds(): array {
    return array_keys($this->workflow->getTypePlugin()->getTransitions());
  }

  /**
   * Creates a page node type to test with, ensuring that it's moderated.
   */
  protected function createNodeType(): NodeTypeInterface {
    $id = $this->randomMachineName();
    $node_type = $this->createContentType([
      'type' => $id,
      'label' => 'Test',
    ]);
    $node_type->save();

    $workflow = $this->createEditorialWorkflow();
    $this->addEntityTypeAndBundleToWorkflow($workflow, 'node', $id);

    return $node_type;
  }

}
