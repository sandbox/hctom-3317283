<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Hook;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Tests\content_moderation_links\Kernel\ModerationLinkTestBase;
use Drupal\workflows\Entity\Workflow;

/**
 * Tests the 'hook_ENTITY_TYPE_delete' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\WorkflowDeleteHook
 * @group content_moderation_links
 */
class WorkflowDeleteHookTest extends ModerationLinkTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'workflow_type_test',
  ];

  /**
   * Tests response to workflow entity deletion.
   *
   * @covers ::workflowDelete
   */
  public function testWorkflowDelete(): void {
    // Prepare test cache tags invalidator.
    $test_invalidator = new WorkflowDeleteHook_Test_CacheTagsInvalidator();

    // Initialize actual cache tags invalidator with test cache tags
    // invalidator.
    $cache_tags_invalidator = $this->container->get('cache_tags.invalidator');
    $this->assertInstanceOf(CacheTagsInvalidatorInterface::class, $cache_tags_invalidator);
    $cache_tags_invalidator->addInvalidator($test_invalidator);

    $workflow = Workflow::create([
      'id' => 'test_workflow',
      'type' => 'workflow_type_test',
    ]);
    $workflow->save();

    // Reset recorded names of invalidated cache tags.
    $test_invalidator->resetInvalidatedTags();

    // Delete entity.
    $workflow->save();

    $this->assertNotFalse(array_search('local_action', $test_invalidator->getInvalidatedTags()));
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked cache tags invalidator class for tests.
 */
class WorkflowDeleteHook_Test_CacheTagsInvalidator implements CacheTagsInvalidatorInterface {

  /**
   * Names of invalidated cache tags.
   */
  protected array $invalidatedTags;

  /**
   * Constructs a new WorkflowDeleteHook_Test_CacheTagsInvalidator.
   */
  public function __construct() {
    $this->resetInvalidatedTags();
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags): void {
    $this->invalidatedTags = array_merge($this->invalidatedTags, $tags);
  }

  /**
   * Returns the names of invalidated cache tags.
   *
   * @return array
   *   The names of all invalidated cache tags since the last reset.
   */
  public function getInvalidatedTags(): array {
    return $this->invalidatedTags;
  }

  /**
   * Resets the list of invalidated cache tags.
   */
  public function resetInvalidatedTags(): void {
    $this->invalidatedTags = [];
  }

}
// @codingStandardsIgnoreEnd
