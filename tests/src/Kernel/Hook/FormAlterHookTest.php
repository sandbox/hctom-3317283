<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Hook;

use Drupal\Tests\content_moderation_links\Kernel\ModerationLinkTestBase;

/**
 * Tests the 'hook_form_alter' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\FormAlterHook
 * @group content_moderation_links
 */
class FormAlterHookTest extends ModerationLinkTestBase {

  /**
   * Data provider for performing alterations before a form is rendered.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderFormAlter(): array {
    return [
      // TODO
    ];
  }

  /**
   * Tests performing alterations before a form is rendered.
   *
   * TODO Param docs
   *
   * @covers ::entityUsageDataExists
   * @covers ::entityUsageNoticeIsEnabledForEntity
   * @covers ::formAlter
   * @covers ::getEntity
   * @covers ::shouldHaveEntityUsageNotice
   *
   * @dataProvider dataProviderFormAlter
   */
  public function testFormAlter(): void {
    // TODO
    $this->assertTrue(TRUE);
  }

}
