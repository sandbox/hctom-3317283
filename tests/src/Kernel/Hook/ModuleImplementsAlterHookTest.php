<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Hook;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\content_moderation_links\Kernel\ModerationLinkTestBase;

/**
 * Tests the 'hook_module_implements_alter' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\ModuleImplementsAlterHook
 * @group content_moderation_links
 */
class ModuleImplementsAlterHookTest extends ModerationLinkTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'image',
    'media',
  ];

  /**
   * Tests altering the registry of modules implementing a hook.
   *
   * Ensures that content_moderation_links_entity_type_alter() is executed after
   * media module's implementation.
   *
   * @covers ::moduleImplementsAlter
   */
  public function testModuleImplementsAlter(): void {
    $module_handler = $this->container->get('module_handler');
    $this->assertInstanceOf(ModuleHandlerInterface::class, $module_handler);

    $module_handler->resetImplementations();

    $this->assertTrue($module_handler->hasImplementations('entity_type_alter', [
      'content_moderation_links',
      'media',
    ]));

    $invoked = [];
    $module_handler->invokeAllWith('entity_type_alter', function (callable $hook, string $module) use (&$invoked): void {
      $invoked[] = $module;
    });

    $this->assertGreaterThan(array_search('media', $invoked, TRUE), array_search('content_moderation_links', $invoked, TRUE));
  }

}
