<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Hook;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Tests\content_moderation_links\Kernel\ModerationLinkTestBase;

/**
 * Tests the 'hook_entity_type_alter' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\EntityTypeAlterHook
 * @group content_moderation_links
 */
class EntityTypeAlterHookTest extends ModerationLinkTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'image',
    'media',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('media');
    $this->installConfig(['media']);
  }

  /**
   * Data provider for altering the entity type definitions.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderEntityTypeAlter(): array {
    return [
      'media' => [
        'media',
        [
          LinkTemplate::DISCARD_LATEST_VERSION => '/media/{media}/edit/latest/discard',
          LinkTemplate::MODERATION_STATE_TRANSITION => '/media/{media}/edit/transition/from/{from}/to/{to}',
        ],
      ],
      'media-standalone-pages' => [
        'media_STANDALONE',
        [
          LinkTemplate::DISCARD_LATEST_VERSION => '/media/{media}/edit/latest/discard',
          LinkTemplate::MODERATION_STATE_TRANSITION => '/media/{media}/transition/from/{from}/to/{to}',
        ],
      ],
      'node' => [
        'node',
        [
          LinkTemplate::DISCARD_LATEST_VERSION => '/node/{node}/latest/discard',
          LinkTemplate::MODERATION_STATE_TRANSITION => '/node/{node}/transition/from/{from}/to/{to}',
        ],
      ],
      'taxonomy_term' => [
        'taxonomy_term',
        [
          LinkTemplate::DISCARD_LATEST_VERSION => '/taxonomy/term/{taxonomy_term}/latest/discard',
          LinkTemplate::MODERATION_STATE_TRANSITION => '/taxonomy/term/{taxonomy_term}/transition/from/{from}/to/{to}',
        ],
      ],
    ];
  }

  /**
   * Tests altering the entity type definitions.
   *
   * @covers ::addModerationLinksToEntityType
   * @covers ::entityTypeAlter
   *
   * @dataProvider dataProviderEntityTypeAlter
   */
  public function testEntityTypeAlter(string $entity_type_id, array $expected): void {
    if ($entity_type_id === 'media_STANDALONE') {
      // Rewrite media entity type ID to correct value again.
      $entity_type_id = 'media';

      // Enable standalone media URLs.
      $this->config('media.settings')->set('standalone_url', TRUE)->save();
    }
    elseif ($entity_type_id === 'media') {
      // Ensure media standalone URLs are disabled.
      $this->config('media.settings')->set('standalone_url', FALSE)->save();
    }

    // Ensure up-to-date entity type plugin definitions.
    $this->entityTypeManager->clearCachedDefinitions();

    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

    $this->assertInstanceOf(EntityTypeInterface::class, $entity_type);
    $this->assertEquals(!!$expected, $entity_type->hasLinkTemplate(LinkTemplate::DISCARD_LATEST_VERSION));
    $this->assertEquals(!!$expected, $entity_type->hasLinkTemplate(LinkTemplate::MODERATION_STATE_TRANSITION));

    if (!!$expected) {
      $this->assertArrayHasKey(LinkTemplate::DISCARD_LATEST_VERSION, $expected);
      $this->assertEquals($expected[LinkTemplate::DISCARD_LATEST_VERSION], $entity_type->getLinkTemplate(LinkTemplate::DISCARD_LATEST_VERSION));
      $this->assertArrayHasKey(LinkTemplate::MODERATION_STATE_TRANSITION, $expected);
      $this->assertEquals($expected[LinkTemplate::MODERATION_STATE_TRANSITION], $entity_type->getLinkTemplate(LinkTemplate::MODERATION_STATE_TRANSITION));
    }
  }

}
