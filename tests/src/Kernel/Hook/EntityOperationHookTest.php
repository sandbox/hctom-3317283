<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Hook;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\content_moderation_links\Kernel\ModerationLinkTestBase;

/**
 * Tests the 'hook_entity_operation' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\EntityOperationHook
 * @group content_moderation_links
 */
class EntityOperationHookTest extends ModerationLinkTestBase {

  /**
   * Data provider for declaring entity operations.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderEntityOperation(): array {
    return [
      'archived' => [
        'archived',
        [
          'content_moderation_links.moderation_state_transition.archived_draft',
          'content_moderation_links.moderation_state_transition.archived_published',
        ],
      ],
      'draft' => [
        'draft',
        [
          'content_moderation_links.discard_latest_version',
          'content_moderation_links.moderation_state_transition.publish',
        ],
      ],
      'published' => [
        'published',
        [
          'content_moderation_links.moderation_state_transition.create_new_draft',
          'content_moderation_links.moderation_state_transition.archive',
        ],
      ],
    ];
  }

  /**
   * Tests declaring entity operations.
   *
   * @covers ::entityOperation
   *
   * @dataProvider dataProviderEntityOperation
   */
  public function testEntityOperation(string $moderation_state, array $expected): void {
    // Create test node.
    $node = $this->createNode([
      'type' => $this->nodeType->id(),
      'title' => $this->randomString(),
      'moderation_state' => 'published',
    ]);
    $node->save();

    $node->set('moderation_state', $moderation_state);
    $node->setNewRevision();
    $node->save();

    // Prepare list of all available content moderation link entity operations.
    $all_transition_operations = array_merge(array_map(
      fn(string $id) => sprintf('content_moderation_links.moderation_state_transition.%s', $id),
      $this->getAllTransitionIds(),
    ), ['content_moderation_links_discard_latest_version']);

    // Prepare test admin user.
    $user = $this->createUser([], NULL, TRUE);
    $this->assertInstanceOf(AccountInterface::class, $user);
    $this->setCurrentUser($user);

    $operations = $this->entityTypeManager->getListBuilder($node->getEntityTypeId())
      ->getOperations($node);

    foreach ($expected as $expected_operation) {
      $this->assertArrayHasKey($expected_operation, $operations);
    }

    $unexpected = array_diff($all_transition_operations, $expected);
    foreach ($unexpected as $unexpected_operation) {
      $this->assertArrayNotHasKey($unexpected_operation, $operations);
    }
  }

}
