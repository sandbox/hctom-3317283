<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Hook;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Tests\content_moderation_links\Kernel\ModerationLinkTestBase;
use Drupal\workflows\Entity\Workflow;

/**
 * Tests the 'hook_ENTITY_TYPE_presave' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\WorkflowPresaveHook
 * @group content_moderation_links
 */
class WorkflowPresaveHookTest extends ModerationLinkTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'workflow_type_test',
  ];

  /**
   * Data provider for acting on an entity before it is created or updated.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderWorkflowPresave(): array {
    return [
      'saved-new' => [
        FALSE,
      ],
      'saved-updated' => [
        TRUE,
      ],
    ];
  }

  /**
   * Tests acting on a specific type of entity before it is created or updated.
   *
   * @param bool $test_update
   *   Whether to test an entity update or its initial creation.
   *
   * @covers ::workflowPresave
   *
   * @dataProvider dataProviderWorkflowPresave
   */
  public function testWorkflowPresave(bool $test_update): void {
    // Prepare test cache tags invalidator.
    $test_invalidator = new WorkflowPresaveHook_Test_CacheTagsInvalidator();

    // Initialize actual cache tags invalidator with test cache tags
    // invalidator.
    $cache_tags_invalidator = $this->container->get('cache_tags.invalidator');
    $this->assertInstanceOf(CacheTagsInvalidatorInterface::class, $cache_tags_invalidator);
    $cache_tags_invalidator->addInvalidator($test_invalidator);

    $workflow = Workflow::create([
      'id' => 'test_workflow',
      'type' => 'workflow_type_test',
    ]);
    $workflow->save();

    if ($test_update) {
      // Reset recorded names of invalidated cache tags.
      $test_invalidator->resetInvalidatedTags();

      // Save entity again for testing an update.
      $workflow->save();
    }

    $this->assertNotFalse(array_search('local_action', $test_invalidator->getInvalidatedTags()));
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked cache tags invalidator class for tests.
 */
class WorkflowPresaveHook_Test_CacheTagsInvalidator implements CacheTagsInvalidatorInterface {

  /**
   * Names of invalidated cache tags.
   */
  protected array $invalidatedTags;

  /**
   * Constructs a new WorkflowPresave_Test_CacheTagsInvalidator.
   */
  public function __construct() {
    $this->resetInvalidatedTags();
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags): void {
    $this->invalidatedTags = array_merge($this->invalidatedTags, $tags);
  }

  /**
   * Returns the names of invalidated cache tags.
   *
   * @return array
   *   The names of all invalidated cache tags since the last reset.
   */
  public function getInvalidatedTags(): array {
    return $this->invalidatedTags;
  }

  /**
   * Resets the list of invalidated cache tags.
   */
  public function resetInvalidatedTags(): void {
    $this->invalidatedTags = [];
  }

}
// @codingStandardsIgnoreEnd
