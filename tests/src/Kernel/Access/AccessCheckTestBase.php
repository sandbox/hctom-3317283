<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\content_moderation_links\Kernel\ModerationLinkTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Provides a base class for content moderation link access unit test classes.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Access\DiscardLatestVersionAccessCheck
 * @group content_moderation_links
 */
abstract class AccessCheckTestBase extends ModerationLinkTestBase {

  /**
   * The test user.
   */
  protected AccountInterface $user;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create test user.
    $user = $this->createUser([
      'access content',
      sprintf('edit any %s content', $this->nodeType->id()),
    ], NULL, FALSE, [
      // Ensure we do not create a user with ID 1.
      'uid' => rand(2, 1234),
    ]);
    $this->assertInstanceOf(AccountInterface::class, $user);
    $this->user = $user;

    // Set test user as current user.
    $this->container->get('current_user')->setAccount($this->user);
  }

  // TODO DOcs
  protected function addPermissionToCurrentUser(string $permission): void {
    $rids = $this->user->getRoles(TRUE);
    $rid = reset($rids);

    $role = $this->container->get('entity_type.manager')->getStorage('user_role')->load($rid);
    $this->assertInstanceOf(RoleInterface::class, $role);
    $role->grantPermission($permission)->save();
  }

  // TODO DOcs
  protected function allowCurrentUserToDiscardLatestVersion(WorkflowInterface $workflow): void {
    $permission = sprintf('discard latest version in %s workflow', $workflow->id());

    $this->addPermissionToCurrentUser($permission);
  }

  // TODO DOcs
  protected function allowCurrentUserToViewLatestVersion(): void {
    $permission = 'view latest version';

    $this->addPermissionToCurrentUser($permission);
  }

  // TODO DOcs
  protected function allowCurrentUserToViewAnyUnpublishedContent(): void {
    $permission = 'view any unpublished content';

    $this->addPermissionToCurrentUser($permission);
  }

  // TODO DOcs
  protected function allowCurrentUserToViewOwnUnpublishedContent(): void {
    $permission = 'view own unpublished content';

    $this->addPermissionToCurrentUser($permission);
  }

}
