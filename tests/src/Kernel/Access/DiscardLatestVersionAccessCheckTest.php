<?php

namespace Drupal\Tests\content_moderation_links\Kernel\Access;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;

/**
 * Tests the discard latest entity version access check.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Access\DiscardLatestVersionAccessCheck
 * @group content_moderation_links
 */
class DiscardLatestVersionAccessCheckTest extends AccessCheckTestBase {

  public function dataProviderAccess(): array {
    return [
      // TODO DOes not work
//      'not-owner' => [
//        TRUE,
//        TRUE,
//        TRUE,
//        FALSE,
//        FALSE,
//        FALSE,
//        TRUE,
//      ],









      'not-owner-view-own-unpublished-only' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
        FALSE,
        FALSE,
      ],
      'owner' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
        TRUE,
      ],
      'owner-view-own-unpublished-only' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
        TRUE,
      ],
//
//
//
//
//
//      'not-owner-not-allowed-to-discard-latest-version' => [
//        FALSE,
//        TRUE,
//        TRUE,
//        TRUE,
//        FALSE,
//        FALSE,
//        FALSE,
//      ],
//      'owner-not-allowed-to-discard-latest-version' => [
//        FALSE,
//        TRUE,
//        TRUE,
//        TRUE,
//        TRUE,
//        FALSE,
//        FALSE,
//      ],
//      'not-owner-not-allowed-to-view-latest-version' => [
//        TRUE,
//        FALSE,
//        TRUE,
//        TRUE,
//        FALSE,
//        FALSE,
//        FALSE,
//      ],
//      'owner-not-allowed-to-view-latest-version' => [
//        TRUE,
//        FALSE,
//        TRUE,
//        TRUE,
//        TRUE,
//        FALSE,
//        FALSE,
//      ],
//      'not-owner-not-allowed-to-view-unpublished' => [
//        TRUE,
//        TRUE,
//        FALSE,
//        TRUE,
//        FALSE,
//        FALSE,
//        FALSE,
//      ],
//      'owner-not-allowed-to-view-unpublished' => [
//        TRUE,
//        TRUE,
//        FALSE,
//        FALSE,
//        TRUE,
//        FALSE,
//        FALSE,
//      ],
    ];
  }

  /**
   * TODO Docs
   *
   * @param bool $allowed_to_discard_latest_version
   * @param bool $allowed_to_view_latest_version
   * @param bool $allowed_to_view_any_unpublished_content
   * @param bool $allowed_to_view_own_unpublished_content
   * @param bool $is_owner
   * @param bool $is_canonical_route
   * @param bool $expected
   *
   * @covers ::access
   * @covers ::allowedIfDiscardableLatestVersion
   * @covers ::allowedIfHasDiscardPermission
   * @covers ::allowedIfLatestVersionAccess
   * @covers ::isAllowedOnCanonicalRoute
   *
   * @dataProvider dataProviderAccess
   */
  public function testAccess(bool $allowed_to_discard_latest_version, bool $allowed_to_view_latest_version, bool $allowed_to_view_any_unpublished_content, bool $allowed_to_view_own_unpublished_content, bool $is_owner, bool $is_canonical_route, bool $expected): void {
    if ($allowed_to_discard_latest_version) {
      $this->allowCurrentUserToDiscardLatestVersion($this->workflow);
    }

    if ($allowed_to_view_latest_version) {
      $this->allowCurrentUserToViewLatestVersion();
    }

    if ($allowed_to_view_any_unpublished_content) {
      $this->allowCurrentUserToViewAnyUnpublishedContent();
    }

    if ($allowed_to_view_own_unpublished_content) {
      $this->allowCurrentUserToViewOwnUnpublishedContent();
    }

    $node = $this->createNode([
      'type' => $this->nodeType->id(),
      'title' => $this->randomString(),
      'uid' => $is_owner ? $this->user->id() : RoleInterface::ANONYMOUS_ID,
      'moderation_state' => 'draft',
    ]);

    // Never allowed, because there is no other revision available.
    $this->assertFalse($this->urlDiscardLatestVersion($node)->access($this->user));

    // Publish node.
    $node->set('moderation_state', 'published');
    $node->setNewRevision();
    $node->save();

    // Never allowed, because default revisions are not discardable.
    $this->assertFalse($this->urlDiscardLatestVersion($node)->access($this->user));

    // Create new draft.
    $node->set('moderation_state', 'draft');
    $node->setNewRevision();
    $node->save();

    // Check expected access.
    $this->assertEquals($expected, $this->urlDiscardLatestVersion($node)->access($this->user));

    // Move to archive.
    $node->set('moderation_state', 'archived');
    $node->setNewRevision();
    $node->save();

    // Never allowed, because default revisions are not discardable.
    $this->assertFalse($this->urlDiscardLatestVersion($node)->access($this->user));

    // Restore as draft.
    $node->set('moderation_state', 'draft');
    $node->setNewRevision();
    $node->save();

    // Check expected access.
    $this->assertEquals($expected, $this->urlDiscardLatestVersion($node)->access($this->user));
  }

  protected function urlDiscardLatestVersion(ContentEntityInterface $entity): Url {
    return $entity->toUrl(LinkTemplate::DISCARD_LATEST_VERSION);
  }

}
