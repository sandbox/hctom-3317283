<?php

namespace Drupal\Tests\content_moderation_links\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * TODO Docs
 */
class ModerationLinkTest extends BrowserTestBase {

  use ContentTypeCreationTrait;
  use ContentModerationTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'content_moderation',
    'content_moderation_links',
    'node',
  ];

  /**
   * The ID of the local actions block for testing.
   *
   * @var string
   */
  protected string $blockId;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $block = $this->drupalPlaceBlock('local_actions_block');
    $this->assertIsString($block->id());
    $this->blockId = $block->id();

    $moderated_bundle = $this->createContentType(['type' => 'moderated_bundle']);
    $moderated_bundle->save();
    $standard_bundle = $this->createContentType(['type' => 'standard_bundle']);
    $standard_bundle->save();

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'moderated_bundle');
    $workflow->save();

    $admin = $this->drupalCreateUser([
      'access content overview',
      'administer nodes',
      'bypass node access',
      'view latest version',
      'view any unpublished content',
      'use ' . $workflow->id() . ' transition archive',
      'use ' . $workflow->id() . ' transition archived_draft',
      'use ' . $workflow->id() . ' transition archived_published',
      'use ' . $workflow->id() . ' transition create_new_draft',
      'use ' . $workflow->id() . ' transition publish',
      'discard latest version in ' . $workflow->id() . ' workflow',
    ]);
    $this->assertInstanceOf(AccountInterface::class, $admin);
    $this->drupalLogin($admin);
  }

  /**
   * Test cases for ::testNodeStatusActions.
   *
   * @return array
   *   An array of test cases.
   */
  public function dataProviderDraft(): array {
    return [
      'moderated-bundle' => [
        'moderated_bundle',
        TRUE,
      ],
      'normal-bundle' => [
        'standard_bundle',
        FALSE,
      ],
    ];
  }


  /**
   * @param string $bundle
   * @param bool $expected
   *
   * @dataProvider dataProviderDraft
   */
  public function testArchived(string $bundle, bool $expected): void {
    // Create and run an action on a node.
    $node = Node::create([
      'type' => $bundle,
      'title' => $this->randomString(),
      'status' => FALSE,
      'moderation_state' => 'published',
    ]);
    $node->save();
    $node->set('moderation_state', 'archived');
    $node->save();

    $this->drupalGet('node/' . $node->id());

    if ($expected) {
      $this->assertLocalActionsCount($this->blockId, 2);
      $this->assertLocalActionRestore($this->blockId);
      $this->assertLocalActionRestoreToDraft($this->blockId);
    }
    else {
      $this->assertLocalActionsCount($this->blockId, 0);
      $this->assertNoLocalActionsAtAll($this->blockId);
    }
  }

  /**
   * @param string $bundle
   * @param bool $expected
   *
   * @dataProvider dataProviderDraft
   */
  public function testDraft(string $bundle, bool $expected): void {
    // Create and run an action on a node.
    $node = Node::create([
      'type' => $bundle,
      'title' => $this->randomString(),
      'status' => FALSE,
      'moderation_state' => 'draft',
    ]);
    $node->save();

    $this->drupalGet('node/' . $node->id());

    if ($expected) {
      $this->assertLocalActionsCount($this->blockId, 1);
      $this->assertLocalActionPublish($this->blockId);

      $node->set('moderation_state', 'published');
      $node->save();
      $node->set('moderation_state', 'draft');
      $node->save();

      $this->drupalGet('node/' . $node->id() . '/latest');

      $this->assertLocalActionsCount($this->blockId, 2);
      $this->assertLocalActionPublish($this->blockId);
      $this->assertLocalActionDiscard($this->blockId);
    }
    else {
      $this->assertLocalActionsCount($this->blockId, 0);
      $this->assertNoLocalActionsAtAll($this->blockId);
    }
  }

  /**
   * @param string $bundle
   * @param bool $expected
   *
   * @dataProvider dataProviderDraft
   */
  public function testPublished(string $bundle, bool $expected): void {
    // Create and run an action on a node.
    $node = Node::create([
      'type' => $bundle,
      'title' => $this->randomString(),
      'status' => FALSE,
      'moderation_state' => 'published',
    ]);
    $node->save();

    $this->drupalGet('node/' . $node->id());

    if ($expected) {
      $this->assertLocalActionsCount($this->blockId, 2);
      $this->assertLocalActionArchive($this->blockId);
      $this->assertLocalActionCreateNewDraft($this->blockId);
    }
    else {
      $this->assertLocalActionsCount($this->blockId, 0);
      $this->assertNoLocalActionsAtAll($this->blockId);
    }
  }

  /**
   * TODO Docs
   *
   * @param string $block_id
   *
   * @return \Behat\Mink\Element\NodeElement
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertLocalActionsBlock(string $block_id): NodeElement {
    return $this->assertSession()->elementExists('xpath', "//div[@id = 'block-{$block_id}']");
  }

  protected function assertLocalAction(string $block_id, string $href_prefix, bool $expected): void {
    $block = $this->assertLocalActionsBlock($block_id);
    $this->assertInstanceOf(NodeElement::class, $block);
    $action = $block->find('css', 'a[href$="' . $href_prefix . '"]');

    if ($expected) {
      $this->assertInstanceOf(NodeElement::class, $action);
    }
    else {
      $this->assertEmpty($action);
    }
  }

  protected function assertLocalActionArchive(string $block_id, bool $expected = TRUE): void {
    $this->assertLocalAction($block_id, '/transition/from/published/to/archived', $expected);
  }

  protected function assertLocalActionCreateNewDraft(string $block_id, bool $expected = TRUE): void {
    $this->assertLocalAction($block_id, '/transition/from/published/to/draft', $expected);
  }

  protected function assertLocalActionDiscard(string $block_id, bool $expected = TRUE): void {
    $this->assertLocalAction($block_id, '/latest/discard', $expected);
  }

  protected function assertLocalActionPublish(string $block_id, bool $expected = TRUE): void {
    $this->assertLocalAction($block_id, '/transition/from/draft/to/published', $expected);
  }

  protected function assertLocalActionRestore(string $block_id, bool $expected = TRUE): void {
    $this->assertLocalAction($block_id, '/transition/from/archived/to/published', $expected);
  }

  protected function assertLocalActionRestoreToDraft(string $block_id, bool $expected = TRUE): void {
    $this->assertLocalAction($block_id, '/transition/from/archived/to/draft', $expected);
  }

  protected function assertLocalActionsCount(string $block_id, int $expected_count): void {
    $block_id = $this->assertLocalActionsBlock($block_id);
    $actions = $block_id->findAll('css', 'a.button');
    $this->assertEquals($expected_count, count(array_keys($actions)));
  }

  protected function assertNoLocalActionsAtAll(string $block_id): void {
    $this->assertLocalActionArchive($block_id, FALSE);
    $this->assertLocalActionCreateNewDraft($block_id, FALSE);
    $this->assertLocalActionPublish($block_id, FALSE);
    $this->assertLocalActionRestore($block_id, FALSE);
    $this->assertLocalActionRestoreToDraft($block_id, FALSE);
  }

}
