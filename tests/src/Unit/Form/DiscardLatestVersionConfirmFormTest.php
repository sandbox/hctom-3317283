<?php

namespace Drupal\Tests\content_moderation_links\Unit\Form;

use Drupal\content_moderation_links\Form\DiscardLatestVersionConfirmForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

/**
 * Tests the content moderation link form to discard latest entity version.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Form\DiscardLatestVersionConfirmForm
 * @group content_moderation_links
 */
class DiscardLatestVersionConfirmFormTest extends FormTestBase {

  /**
   * Data provider for returning the route to go to if user cancels the action.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetCancelUrl(): array {
    return [
      'no-content-entity' => [
        FALSE,
        TRUE,
        'latest-version',
      ],
      'not-restored-as-latest-version' => [
        TRUE,
        FALSE,
        'latest-version',
      ],
      'restored-as-latest-version' => [
        TRUE,
        TRUE,
        'canonical',
      ],
    ];
  }

  /**
   * Tests returning the route to go to if the user cancels the action.
   *
   * @param bool $is_content_entity
   *   Whether it is a content entity.
   * @param bool $is_restored_as_latest_version
   *   Whether the entity is restored as latest version.
   * @param string $expected
   *   The expected value.
   *
   * @covers ::getCancelUrl
   *
   * @dataProvider dataProviderGetCancelUrl
   */
  public function testGetCancelUrl(bool $is_content_entity, bool $is_restored_as_latest_version, string $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock($is_content_entity ? ContentEntityInterface::class : EntityInterface::class);

    $entity->expects($this->once())
      ->method('toUrl')
      ->with($expected)
      ->willReturnCallback(function ($rel): string {
        return $rel;
      });

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($is_content_entity ? $this->once() : $this->never())
      ->method('isRestoredAsLatestVersion')
      ->with($entity)
      ->willReturn($is_restored_as_latest_version);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $this->assertEquals($expected, $class->getCancelUrl());
  }

  /**
   * Tests returning a caption for the button that confirms the action.
   *
   * @covers ::getConfirmText
   */
  public function testGetConfirmText(): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals('Discard latest version', $class->getConfirmText());
  }

  /**
   * Tests returning additional text to display as a description.
   *
   * @covers ::getDescription
   */
  public function testGetDescription(): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals('This will back up and discard the current pending revision(s).', $class->getDescription());
  }

  /**
   * Tests returning the question to ask the user.
   *
   * @covers ::getQuestion
   */
  public function testGetQuestion(): void {
    $label = 'test label';

    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    $entity->expects($this->once())
      ->method('label')
      ->willReturn($label);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $this->assertEquals(sprintf('Are you sure you want to discard the latest version of <em class="placeholder">%s</em>?', $label), $class->getQuestion());
  }

  /**
   * Tests returning the default revision log message.
   *
   * @covers ::getRevisionLogMessageDefault
   */
  public function testGetRevisionLogMessageDefault(): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals('Discard latest version', $class->getRevisionLogMessageDefault());
  }

  /**
   * Data provider for the form submission handler.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderSubmitForm(): array {
    return [
      'success' => [
        TRUE,
      ],
      'no-success' => [
        FALSE,
      ],
    ];
  }

  /**
   * Tests the form submission handler.
   *
   * @param bool $success
   *   Whether the form submission is successful.
   *
   * @covers ::submitForm
   *
   * @dataProvider dataProviderSubmitForm
   */
  public function testSubmitForm(bool $success): void {
    $revision_log_message = 'test message';
    $entity_url = Url::fromRoute('<front>');
    $form = [];

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('toUrl')
      ->willReturn($entity_url);

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    $form_state->expects($this->once())
      ->method('setRedirectUrl')
      ->with($entity_url);

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($this->once())
      ->method('discardLatestVersion')
      ->with($entity, $revision_log_message)
      ->willReturn($success);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'getRevisionLogMessage',
      'submitFormMessage',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $class->expects($this->once())
      ->method('getRevisionLogMessage')
      ->with($form_state)
      ->willReturn($revision_log_message);

    $class->expects($this->once())
      ->method('submitFormMessage')
      ->with($success);

    $class->submitForm($form, $form_state);
  }

  /**
   * Data provider for displaying a message to the user after submission.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderSubmitFormMessage(): array {
    return [
      'success' => [
        TRUE,
        TRUE,
      ],
      'success-no-bundle-label' => [
        TRUE,
        FALSE,
      ],
      'no-success' => [
        FALSE,
        TRUE,
      ],
      'no-success-no-bundle-label' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests displaying a message to the user after submission.
   *
   * @param bool $success
   *   Whether the form submission is successful.
   * @param bool $has_bundle_label
   *   Whether a bundle label is available.
   *
   * @covers ::submitFormMessage
   *
   * @dataProvider dataProviderSubmitFormMessage
   */
  public function testSubmitFormMessage(bool $success, bool $has_bundle_label): void {
    $entity_type_id = 'test_entity_type';
    $entity_id = 123;
    $label = 'test label';
    $bundle = 'test_bundle';
    $bundle_label = 'test bundle label';
    $bundle_info = [
      $bundle => [
        'label' => $has_bundle_label ? $bundle_label : NULL,
      ],
    ];
    $link_view_string = 'link view';
    $link_label_string = 'link label';
    $url_revisions = '/test/revisions';

    // Prepare link mocks.
    $link_view = $this->createMock(Link::class);

    $link_view->expects($this->once())
      ->method('toString')
      ->willReturn($link_view_string);

    $link_label = $this->createMock(Link::class);

    $link_label->expects($this->once())
      ->method('toString')
      ->willReturn($link_label_string);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $entity->expects($this->once())
      ->method('bundle')
      ->willReturn($bundle);

    $entity->expects($this->once())
      ->method('label')
      ->willReturn($label);

    $entity->expects($this->once())
      ->method('id')
      ->willReturn($entity_id);

    $entity->expects($this->exactly(2))
      ->method('toLink')
      // @todo Implement better solution for consecutive calls.
      ->with($this->logicalOr(
        'View',
        $label,
      ))
      ->willReturnCallback(fn($text) => match((string) $text) {
        'View' => $link_view,
        $label => $link_label,
        default => throw new \LogicException('Parameter does not match expected value.'),
      });

    // Prepare entity bundle info mock.
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with($entity_type_id)
      ->willReturn($bundle_info);

    // Prepare URL generator mock.
    $this->urlGenerator->expects($this->once())
      ->method('generateFromRoute')
      ->with(
        'entity.' . $entity_type_id . '.version_history',
        [$entity_type_id => $entity_id],
      )
      ->willReturn($url_revisions);

    // Prepare logger mock.
    $logger = $this->createMock(LoggerInterface::class);

    $logger_context = [
      '@type' => $bundle,
      '%label' => $label,
      'link' => $link_view_string,
    ];

    if ($success) {
      $logger->expects($this->once())
        ->method('notice')
        ->with('@type: backed up and discarded latest version of %label.', $logger_context);
    }
    else {
      $logger->expects($this->once())
        ->method('error')
        ->with('@type: unable to back up and discard latest version of %label.', $logger_context);
    }

    // Prepare messenger mock.
    $messenger = $this->createMock(MessengerInterface::class);

    if ($success) {
      $messenger->expects($this->once())
        ->method('addStatus')
        ->with(sprintf(
          'Latest version of %s <em class="placeholder">%s</em> has been <a href="%s">backed up</a> and discarded.',
          $has_bundle_label ? $bundle_label : 'undefined',
          $link_label_string,
          $url_revisions,
        ));
    }
    else {
      $messenger->expects($this->once())
        ->method('addError')
        ->with(sprintf(
          'Unable to back up and discard latest version of %s <em class="placeholder">%s</em>. Please try again later.',
          $has_bundle_label ? $bundle_label : 'undefined',
          $link_label_string,
        ));
    }

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'logger',
      'messenger',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $class->expects($this->once())
      ->method('messenger')
      ->willReturn($messenger);

    $class->expects($this->once())
      ->method('logger')
      ->with($entity_type_id)
      ->willReturn($logger);

    $class->submitFormMessage($success);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Form\DiscardLatestVersionConfirmForm_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): DiscardLatestVersionConfirmForm_Test|MockObject {
    $mock = $this->getMockBuilder(DiscardLatestVersionConfirmForm_Test::class)
      ->setConstructorArgs([
        $this->entityRepository,
        $this->entityTypeBundleInfo,
        $this->time,
      ])
      ->onlyMethods($only_methods)
      ->getMock();

    $mock->setModeratedEntityHelper($this->moderatedEntityHelper);
    $mock->setModerationInformation($this->moderationInformation);
    $mock->setUrlGenerator($this->urlGenerator);

    return $mock;
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked 'Discard latest version' content moderation link form class for tests.
 */
class DiscardLatestVersionConfirmForm_Test extends DiscardLatestVersionConfirmForm {

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessage(FormStateInterface $form_state): string {
    return parent::getRevisionLogMessage($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessageCustom(FormStateInterface $form_state): ?string {
    return parent::getRevisionLogMessageCustom($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessageDefault(): string {
    return parent::getRevisionLogMessageDefault();
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormMessage(bool $success): void {
    parent::submitFormMessage($success);
  }

}
// @codingStandardsIgnoreEnd
