<?php

namespace Drupal\Tests\content_moderation_links\Unit\Form;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_links\Form\ModerationLinkConfirmFormBase;
use Drupal\content_moderation_links\ModeratedEntityHelperInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the base class for content moderation link confirm forms.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Form\ModerationLinkConfirmFormBase
 * @group content_moderation_links
 */
class ModerationLinkConfirmFormBaseTest extends FormTestBase {

  /**
   * Tests the form constructor.
   *
   * @covers ::buildForm
   */
  public function testBuildForm(): void {
    // Prepare form mock.
    $form = [];

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    // Prepare class mock.
    $class = $this->createClassMock([
      'buildFormBase',
    ]);

    $class->expects($this->once())
      ->method('buildFormBase')
      ->with($form, $form_state)
      ->willReturn($form);

    $this->assertEquals($form, $class->buildForm($form, $form_state));
  }

  /**
   * Tests the actual form constructor.
   *
   * @covers ::buildFormBase
   * @covers ::buildFormParent
   */
  public function testBuildFormBase(): void {
    $default_revision_message = 'default test message';

    // Prepare form mock.
    $form = [];

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    // Prepare class mock.
    $class = $this->createClassMock([
      'buildFormParent',
      'getRevisionLogMessageDefault',
    ]);

    $class->expects($this->once())
      ->method('buildFormParent')
      ->with($form, $form_state)
      ->willReturn($form);

    $class->expects($this->once())
      ->method('getRevisionLogMessageDefault')
      ->willReturn($default_revision_message);

    $this->assertEquals([
      'default_revision_log_message_toggle' => [
        '#type' => 'checkbox',
        '#title' => 'Use default revision log message',
        '#description' => sprintf(
          'Uncheck this to provide a custom revision log message instead of the default <em class="placeholder">%s</em> message.',
          $default_revision_message,
        ),
        '#default_value' => TRUE,
      ],
      'revision_log_message' => [
        '#type' => 'textarea',
        '#title' => 'Revision log message',
        '#description' => sprintf(
          'Briefly describe the changes you have made. Leave empty to use default <em class="placeholder">%s</em> message.',
          $default_revision_message,
        ),
        '#states' => [
          'visible' => [
            ':input[name="default_revision_log_message_toggle"]' => [
              'checked' => FALSE,
            ],
          ],
        ],
      ],
    ], $class->buildFormBase($form, $form_state));
  }

  /**
   * Data provider for returning the revision log message.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetRevisionLogMessage(): array {
    return [
      'custom-message' => [
        'custom message',
      ],
      'no-custom-message' => [
        NULL,
      ],
    ];
  }

  /**
   * Tests returning the revision log message.
   *
   * @param string|null $custom_revision_log_message
   *   Whether a custom revision log message is given.
   *
   * @covers ::getRevisionLogMessage
   *
   * @dataProvider dataProviderGetRevisionLogMessage
   */
  public function testGetRevisionLogMessage(?string $custom_revision_log_message): void {
    $default_revision_log_message = 'default message';

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getRevisionLogMessageCustom',
      'getRevisionLogMessageDefault',
    ]);

    $class->expects($this->once())
      ->method('getRevisionLogMessageCustom')
      ->willReturn($custom_revision_log_message);

    $class->expects(!isset($custom_revision_log_message) ? $this->once() : $this->never())
      ->method('getRevisionLogMessageDefault')
      ->willReturn($default_revision_log_message);

    $this->assertEquals($custom_revision_log_message ?? $default_revision_log_message, $class->getRevisionLogMessage($form_state));
  }

  /**
   * Data provider for returning the custom revision log message (if any).
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetRevisionLogMessageCustom(): array {
    return [
      'custom-message' => [
        FALSE,
        'custom message',
        'custom message',
      ],
      'custom-message-with-whitespace' => [
        FALSE,
        ' custom message ',
        'custom message',
      ],
      'no-custom-message' => [
        TRUE,
        NULL,
        NULL,
      ],
      'null-custom-message' => [
        FALSE,
        NULL,
        NULL,
      ],
      'empty-custom-message' => [
        FALSE,
        '',
        NULL,
      ],
      'empty-custom-message-whitespace' => [
        FALSE,
        ' ',
        NULL,
      ],
    ];
  }

  /**
   * Tests returning the custom revision log message (if any).
   *
   * @param bool $uses_default_revision_log_message
   *   Whether the default revision log message should used.
   * @param string|null $custom_revision_log_message
   *   A custom revision log message.
   * @param string|null $expected
   *   The expected value.
   *
   * @covers ::getRevisionLogMessageCustom
   *
   * @dataProvider dataProviderGetRevisionLogMessageCustom
   */
  public function testGetRevisionLogMessageCustom(bool $uses_default_revision_log_message, ?string $custom_revision_log_message, ?string $expected): void {
    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    $consecutive_get_value = [
      ['default_revision_log_message_toggle'],
    ];

    $consecutive_get_value_return = [
      $uses_default_revision_log_message,
    ];

    if (!$uses_default_revision_log_message) {
      $consecutive_get_value[] = ['revision_log_message'];
      $consecutive_get_value_return[] = $custom_revision_log_message;
    }

    $form_state->expects($this->exactly(count($consecutive_get_value)))
      ->method('getValue')
      // @todo Implement better solution for consecutive calls.
      ->withConsecutive(...$consecutive_get_value)
      ->willReturnOnConsecutiveCalls(...$consecutive_get_value_return);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected, $class->getRevisionLogMessageCustom($form_state));
  }

  /**
   * Tests setting the moderated entity helper.
   *
   * @covers ::setModeratedEntityHelper
   */
  public function testSetModeratedEntityHelper(): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $class->setModeratedEntityHelper($this->moderatedEntityHelper);

    $this->assertInstanceOf(ModeratedEntityHelperInterface::class, $class->moderatedEntityHelper);
  }

  /**
   * Tests setting the moderation information service.
   *
   * @covers ::setModerationInformation
   */
  public function testSetModerationInformation(): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $class->setModerationInformation($this->moderationInformation);

    $this->assertInstanceOf(ModerationInformationInterface::class, $class->moderationInformation);
  }

  /**
   * Tests setting the URL generator.
   *
   * @covers ::setUrlGenerator
   */
  public function testSetUrlGenerator(): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $class->setUrlGenerator($this->urlGenerator);

    $this->assertInstanceOf(UrlGeneratorInterface::class, $class->urlGenerator);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Form\ModerationLinkConfirmFormBase_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModerationLinkConfirmFormBase_Test|MockObject {
    $mock = $this->getMockBuilder(ModerationLinkConfirmFormBase_Test::class)
      ->setConstructorArgs([
        $this->entityRepository,
        $this->entityTypeBundleInfo,
        $this->time,
      ])
      ->onlyMethods($only_methods)
      ->getMock();

    $mock->setModeratedEntityHelper($this->moderatedEntityHelper);
    $mock->setModerationInformation($this->moderationInformation);
    $mock->setUrlGenerator($this->urlGenerator);

    return $mock;
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked content moderation link confirm form base class for tests.
 */
class ModerationLinkConfirmFormBase_Test extends ModerationLinkConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public ModeratedEntityHelperInterface $moderatedEntityHelper;

  /**
   * {@inheritdoc}
   */
  public ModerationInformationInterface $moderationInformation;

  /**
   * {@inheritdoc}
   */
  public UrlGeneratorInterface $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public function buildFormBase(array $form, FormStateInterface $form_state): array {
    return parent::buildFormBase($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessage(FormStateInterface $form_state): string {
    return parent::getRevisionLogMessage($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessageCustom(FormStateInterface $form_state): ?string {
    return parent::getRevisionLogMessageCustom($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessageDefault(): string {
    return 'test default revision message';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return new TranslatableMarkup('test question');
  }

}
// @codingStandardsIgnoreEnd
