<?php

namespace Drupal\Tests\content_moderation_links\Unit\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_links\ModeratedEntityHelperInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Provides a base class for content moderation link form unit test classes.
 */
abstract class FormTestBase extends UnitTestCase {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityRepository;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeBundleInfo;

  /**
   * The moderated entity helper.
   *
   * @var \Drupal\content_moderation_links\ModeratedEntityHelperInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderatedEntityHelper;

  /**
   * The moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderationInformation;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $time;

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create/register string translation mock.
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    // Initialize entity repository mock.
    $this->entityRepository = $this->createMock(EntityRepositoryInterface::class);

    // Prepare entity type bundle info mock.
    $this->entityTypeBundleInfo = $this->createMock(EntityTypeBundleInfoInterface::class);

    // Initialize moderated entity helper mock.
    $this->moderatedEntityHelper = $this->createMock(ModeratedEntityHelperInterface::class);

    // Initialize moderation information mock.
    $this->moderationInformation = $this->createMock(ModerationInformationInterface::class);

    // Initialize time mock.
    $this->time = $this->createMock(TimeInterface::class);

    // Initialize URL generator mock.
    $this->urlGenerator = $this->createMock(UrlGeneratorInterface::class);
  }

}
