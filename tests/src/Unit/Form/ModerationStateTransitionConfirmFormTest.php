<?php

namespace Drupal\Tests\content_moderation_links\Unit\Form;

use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation_links\Form\ModerationStateTransitionConfirmForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\workflows\TransitionInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Tests the content moderation link form for state transitions.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Form\ModerationStateTransitionConfirmForm
 * @group content_moderation_links
 */
class ModerationStateTransitionConfirmFormTest extends FormTestBase {

  /**
   * Data provider for the form constructor.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderBuildForm(): array {
    return [
      'no-state-from' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
      ],
      'no-state-to' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
      ],
      'no-content-entity' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
      ],
      'can-not-transition' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'all-requirements' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests the form constructor.
   *
   * @param bool $has_state_from
   *   Whether the content moderation state to transition from exists.
   * @param bool $has_state_to
   *   Whether the content moderation state to transition to exists.
   * @param bool $is_content_entity
   *   Whether it is a content entity.
   * @param bool $can_transition
   *   Whether the transition can be performed.
   *
   * @covers ::buildForm
   *
   * @dataProvider dataProviderBuildForm
   */
  public function testBuildForm(bool $has_state_from, bool $has_state_to, bool $is_content_entity, bool $can_transition): void {
    $has_all_requirements = $has_state_from && $has_state_to && $is_content_entity && $can_transition;
    $state_to_id = 'test_state_to';

    // Prepare form mock.
    $form = [];

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    // Prepare from state mock.
    $state_from = $this->createMock(ContentModerationState::class);

    $state_from->expects($has_state_from && $has_state_to && $is_content_entity ? $this->once() : $this->never())
      ->method('canTransitionTo')
      ->with($state_to_id)
      ->willReturn($can_transition);

    $state_from->expects($has_all_requirements ? $this->once() : $this->never())
      ->method('getTransitionTo')
      ->with($state_to_id)
      ->willReturn($transition);

    // Prepare to state mock.
    $state_to = $this->createMock(ContentModerationState::class);

    $state_to->expects($has_state_from && $has_state_to && $is_content_entity ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($state_to_id);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare class mock.
    $class = $this->createClassMock([
      'buildFormBase',
      'getEntity',
    ]);

    $class->expects($has_state_from && $has_state_to ? $this->once() : $this->never())
      ->method('getEntity')
      ->willReturn($is_content_entity ? $entity : NULL);

    $class->expects($has_all_requirements ? $this->once() : $this->never())
      ->method('buildFormBase')
      ->with($form, $form_state)
      ->willReturn($form);

    if (!$has_all_requirements) {
      $this->expectException(NotFoundHttpException::class);
    }

    $class->buildForm($form, $form_state, $has_state_from ? $state_from : NULL, $has_state_to ? $state_to : NULL);

    if ($has_all_requirements) {
      $this->assertEquals($state_from, $class->fromState);
      $this->assertEquals($transition, $class->transition);
    }
  }

  /**
   * Data provider for returning the route to go to if user cancels the action.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetCancelUrl(): array {
    return [
      'default-revision-state' => [
        TRUE,
        FALSE,
        FALSE,
        FALSE,
        'canonical',
      ],
      'no-content-entity' => [
        FALSE,
        FALSE,
        TRUE,
        FALSE,
        'latest-version',
      ],
      'restored-as-latest-version' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        'canonical',
      ],
      'not-restored-as-latest-version' => [
        FALSE,
        TRUE,
        FALSE,
        TRUE,
        'latest-version',
      ],
      'discardable-latest-version' => [
        FALSE,
        TRUE,
        FALSE,
        TRUE,
        'latest-version',
      ],
      'not-discardable-latest-version' => [
        FALSE,
        TRUE,
        FALSE,
        FALSE,
        'canonical',
      ],
    ];
  }

  /**
   * Tests returning the route to go to if the user cancels the action.
   *
   * @param bool $is_default_revision_state
   *   Whether it is in default revision state.
   * @param bool $is_content_entity
   *   Whether it is a content entity.
   * @param bool $is_restored_as_latest_version
   *   Whether it is restored as latest version.
   * @param bool $is_discardable_latest_version
   *   Whether it is a discardable latest version.
   * @param string $expected
   *   The expected value.
   *
   * @covers ::getCancelUrl
   *
   * @dataProvider dataProviderGetCancelUrl
   */
  public function testGetCancelUrl(bool $is_default_revision_state, bool $is_content_entity, bool $is_restored_as_latest_version, bool $is_discardable_latest_version, string $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock($is_content_entity ? ContentEntityInterface::class : EntityInterface::class);

    $entity->expects($this->once())
      ->method('toUrl')
      ->with($expected)
      ->willReturnCallback(function ($rel): string {
        return $rel;
      });

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($is_content_entity && !$is_default_revision_state ? $this->once() : $this->never())
      ->method('isRestoredAsLatestVersion')
      ->with($entity)
      ->willReturn($is_restored_as_latest_version);

    // Prepare from state mock.
    $state_from = $this->createMock(ContentModerationState::class);

    $state_from->expects($this->once())
      ->method('isDefaultRevisionState')
      ->willReturn($is_default_revision_state);

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($is_content_entity && !$is_default_revision_state ? $this->once() : $this->never())
      ->method('isRestoredAsLatestVersion')
      ->with($entity)
      ->willReturn($is_restored_as_latest_version);

    $this->moderatedEntityHelper->expects($is_content_entity && !$is_default_revision_state ? $this->once() : $this->never())
      ->method('isDiscardableLatestVersion')
      ->with($entity)
      ->willReturn($is_discardable_latest_version);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'getFromState',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $class->expects($this->once())
      ->method('getFromState')
      ->willReturn($state_from);

    $this->assertEquals($expected, $class->getCancelUrl());
  }

  /**
   * Tests returning a caption for the button that confirms the action.
   *
   * @covers ::getConfirmText
   */
  public function testGetConfirmText(): void {
    $label = 'test label';

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->once())
      ->method('label')
      ->willReturn($label);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getTransition',
    ]);

    $class->expects($this->once())
      ->method('getTransition')
      ->willReturn($transition);

    $this->assertEquals($label, $class->getConfirmText());
  }

  /**
   * Tests returning additional text to display as a description.
   *
   * @covers ::getDescription
   */
  public function testGetDescription(): void {
    $state_from_label = 'test from label';
    $state_to_label = 'test to label';

    // Prepare from state mock.
    $state_from = $this->createMock(ContentModerationState::class);

    $state_from->expects($this->once())
      ->method('label')
      ->willReturn($state_from_label);

    // Prepare to state mock.
    $state_to = $this->createMock(ContentModerationState::class);

    $state_to->expects($this->once())
      ->method('label')
      ->willReturn($state_to_label);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->once())
      ->method('to')
      ->willReturn($state_to);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getFromState',
      'getTransition',
    ]);

    $class->expects($this->once())
      ->method('getFromState')
      ->willReturn($state_from);

    $class->expects($this->once())
      ->method('getTransition')
      ->willReturn($transition);

    $expected = sprintf(
      'This changes the current state from <em class="placeholder">%s</em> to <em class="placeholder">%s</em>.',
      $state_from_label,
      $state_to_label,
    );

    $this->assertEquals($expected, (string) $class->getDescription());
  }

  /**
   * Tests returning the content moderation state to transition from.
   *
   * @covers ::getFromState
   */
  public function testGetFromState(): void {
    // Prepare state mock.
    $state = $this->createMock(ContentModerationState::class);

    // Prepare class mock.
    $class = $this->createClassMock();
    $class->fromState = $state;

    $this->assertEquals($state, $class->getFromState());
  }

  /**
   * Tests returning the question to ask the user.
   *
   * @covers ::getQuestion
   */
  public function testGetQuestion(): void {
    $entity_label = 'test entity label';
    $transition_label = 'test transition label';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('label')
      ->willReturn($entity_label);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->once())
      ->method('label')
      ->willReturn($transition_label);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'getTransition',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $class->expects($this->once())
      ->method('getTransition')
      ->willReturn($transition);

    $expected = sprintf(
      'Are you sure you want to perform the <em class="placeholder">%s</em> transition for <em class="placeholder">%s</em>?',
      $transition_label,
      $entity_label,
    );

    $this->assertEquals($expected, (string) $class->getQuestion());
  }

  /**
   * Tests returning the default revision log message.
   *
   * @covers ::getRevisionLogMessageDefault
   */
  public function testGetRevisionLogMessageDefault(): void {
    $label = 'test transition label';

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->once())
      ->method('label')
      ->willReturn($label);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getTransition',
    ]);

    $class->expects($this->once())
      ->method('getTransition')
      ->willReturn($transition);

    $this->assertEquals($label, $class->getRevisionLogMessageDefault());
  }

  /**
   * Tests returning the related workflow transition.
   *
   * @covers ::getTransition
   */
  public function testGetTransition(): void {
    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    // Prepare class mock.
    $class = $this->createClassMock();
    $class->transition = $transition;

    $this->assertEquals($transition, $class->getTransition());
  }

  /**
   * Data provider for the form submission handler.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderSubmitForm(): array {
    return [
      'success-not-pending-revision' => [
        TRUE,
        FALSE,
      ],
      'success-pending-revision' => [
        TRUE,
        TRUE,
      ],
      'no-success-not-pending-revision' => [
        FALSE,
        FALSE,
      ],
      'no-success-pending-revision' => [
        FALSE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests the form submission handler.
   *
   * @param bool $success
   *   Whether the form submission is successful.
   * @param bool $has_pending_revision
   *   Whether a pending revision is available.
   *
   * @covers ::submitForm
   *
   * @dataProvider dataProviderSubmitForm
   */
  public function testSubmitForm(bool $success, bool $has_pending_revision): void {
    $revision_log_message = 'test message';
    $entity_url = Url::fromRoute('<front>');
    $entity_url_latest_version = Url::fromRoute('<none>');
    $state_to_id = 'test_state_to';
    $form = [];

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('toUrl')
      ->with(!$has_pending_revision ? 'canonical' : 'latest-version')
      ->willReturn(!$has_pending_revision ? $entity_url : $entity_url_latest_version);

    // Prepare to state mock.
    $state_to = $this->createMock(ContentModerationState::class);

    $state_to->expects($this->once())
      ->method('id')
      ->willReturn($state_to_id);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->once())
      ->method('to')
      ->willReturn($state_to);

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    $form_state->expects($this->once())
      ->method('setRedirectUrl')
      ->with(!$has_pending_revision ? $entity_url : $entity_url_latest_version);

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($this->once())
      ->method('performTransition')
      ->with($entity, $state_to_id, $revision_log_message)
      ->willReturn($success);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('hasPendingRevision')
      ->with($entity)
      ->willReturn($has_pending_revision);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'getRevisionLogMessage',
      'getTransition',
      'submitFormMessage',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $class->expects($this->once())
      ->method('getTransition')
      ->willReturn($transition);

    $class->expects($this->once())
      ->method('getRevisionLogMessage')
      ->with($form_state)
      ->willReturn($revision_log_message);

    $class->expects($this->once())
      ->method('submitFormMessage')
      ->with($success);

    $class->submitForm($form, $form_state);
  }

  /**
   * Data provider for displaying a message to the user after submission.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderSubmitFormMessage(): array {
    return [
      'success' => [
        TRUE,
        TRUE,
      ],
      'success-no-bundle-label' => [
        TRUE,
        FALSE,
      ],
      'no-success' => [
        FALSE,
        TRUE,
      ],
      'no-success-no-bundle-label' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests displaying a message to the user after submission.
   *
   * @param bool $success
   *   Whether the form submission is successful.
   * @param bool $has_bundle_label
   *   Whether a bundle label is available.
   *
   * @covers ::submitFormMessage
   *
   * @dataProvider dataProviderSubmitFormMessage
   */
  public function testSubmitFormMessage(bool $success, bool $has_bundle_label): void {
    $entity_type_id = 'test_entity_type';
    $label = 'test label';
    $bundle = 'test_bundle';
    $bundle_label = 'test bundle label';
    $bundle_info = [
      $bundle => [
        'label' => $has_bundle_label ? $bundle_label : NULL,
      ],
    ];
    $link_view_string = 'link view';
    $link_label_string = 'link label';
    $transition_label = 'test transition label';
    $state_from_label = 'test from label';
    $state_to_label = 'test to label';

    // Prepare from state mock.
    $state_from = $this->createMock(ContentModerationState::class);

    $state_from->expects($this->once())
      ->method('label')
      ->willReturn($state_from_label);

    // Prepare to state mock.
    $state_to = $this->createMock(ContentModerationState::class);

    $state_to->expects($this->once())
      ->method('label')
      ->willReturn($state_to_label);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->once())
      ->method('label')
      ->willReturn($transition_label);

    $transition->expects($this->once())
      ->method('to')
      ->willReturn($state_to);

    // Prepare link mocks.
    $link_view = $this->createMock(Link::class);

    $link_view->expects($this->once())
      ->method('toString')
      ->willReturn($link_view_string);

    $link_label = $this->createMock(Link::class);

    $link_label->expects($this->once())
      ->method('toString')
      ->willReturn($link_label_string);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $entity->expects($this->once())
      ->method('bundle')
      ->willReturn($bundle);

    $entity->expects($this->once())
      ->method('label')
      ->willReturn($label);

    $entity->expects($this->exactly(2))
      ->method('toLink')
      // @todo Implement better solution for consecutive calls.
      ->with(
        $this->logicalOr(
          'View',
          $label,
        ),
      )
      ->willReturnCallback(fn($text) => match ((string) $text) {
        'View' => $link_view,
        $label => $link_label,
        default => throw new \LogicException('Parameter does not match expected value.'),
      });

    // Prepare entity bundle info mock.
    $this->entityTypeBundleInfo->expects($this->once())
      ->method('getBundleInfo')
      ->with($entity_type_id)
      ->willReturn($bundle_info);

    // Prepare logger mock.
    $logger = $this->createMock(LoggerInterface::class);

    $logger_context = [
      '@type' => $bundle,
      '%label' => $label,
      '%transition' => $transition_label,
      'link' => $link_view_string,
    ];

    if ($success) {
      $logger->expects($this->once())
        ->method('notice')
        ->with('@type: performed %transition transition for %label.', $logger_context);
    }
    else {
      $logger->expects($this->once())
        ->method('error')
        ->with('@type: unable to perform %transition transition for %label.', $logger_context);
    }

    // Prepare messenger mock.
    $messenger = $this->createMock(MessengerInterface::class);

    if ($success) {
      $messenger->expects($this->once())
        ->method('addStatus')
        ->with(sprintf(
          'State of %s <em class="placeholder">%s</em> has been changed from <em class="placeholder">%s</em> to <em class="placeholder">%s</em>.',
          $has_bundle_label ? $bundle_label : 'undefined',
          $link_label_string,
          $state_from_label,
          $state_to_label,
        ));
    }
    else {
      $messenger->expects($this->once())
        ->method('addError')
        ->with(sprintf(
          'Unable to change state of %s <em class="placeholder">%s</em> from <em class="placeholder">%s</em> to <em class="placeholder">%s</em>. Please try again later.',
          $has_bundle_label ? $bundle_label : 'undefined',
          $link_label_string,
          $state_from_label,
          $state_to_label,
        ));
    }

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'getFromState',
      'getTransition',
      'logger',
      'messenger',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($entity);

    $class->expects($this->once())
      ->method('getTransition')
      ->willReturn($transition);

    $class->expects($this->once())
      ->method('getFromState')
      ->willReturn($state_from);

    $class->expects($this->once())
      ->method('messenger')
      ->willReturn($messenger);

    $class->expects($this->once())
      ->method('logger')
      ->with($entity_type_id)
      ->willReturn($logger);

    $class->expects($this->once())
      ->method('messenger')
      ->willReturn($messenger);

    $class->submitFormMessage($success);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Form\ModerationStateTransitionConfirmForm_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModerationStateTransitionConfirmForm_Test|MockObject {
    $mock = $this->getMockBuilder(ModerationStateTransitionConfirmForm_Test::class)
      ->setConstructorArgs([
        $this->entityRepository,
        $this->entityTypeBundleInfo,
        $this->time,
      ])
      ->onlyMethods($only_methods)
      ->getMock();

    $mock->setModeratedEntityHelper($this->moderatedEntityHelper);
    $mock->setModerationInformation($this->moderationInformation);
    $mock->setUrlGenerator($this->urlGenerator);

    return $mock;
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked content moderation link form for state transitions class for tests.
 */
class ModerationStateTransitionConfirmForm_Test extends ModerationStateTransitionConfirmForm {

  /**
   * {@inheritdoc}
   */
  public ContentModerationState $fromState;

  /**
   * {@inheritdoc}
   */
  public TransitionInterface $transition;

  /**
   * {@inheritdoc}
   */
  public function buildFormBase(array $form, FormStateInterface $form_state): array {
    return parent::buildFormBase($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFromState(): ContentModerationState {
    return parent::getFromState();
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessage(FormStateInterface $form_state): string {
    return parent::getRevisionLogMessage($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessageCustom(FormStateInterface $form_state): ?string {
    return parent::getRevisionLogMessageCustom($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionLogMessageDefault(): string {
    return parent::getRevisionLogMessageDefault();
  }

  /**
   * {@inheritdoc}
   */
  public function getTransition(): TransitionInterface {
    return parent::getTransition();
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormMessage(bool $success): void {
    parent::submitFormMessage($success);
  }

}
// @codingStandardsIgnoreEnd
