<?php

namespace Drupal\Tests\content_moderation_links\Unit;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\ModeratedEntityHelper;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\workflows\StateInterface;
use Drupal\workflows\TransitionInterface;
use Drupal\workflows\WorkflowInterface;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the moderated entity helper.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\ModeratedEntityHelper
 * @group content_moderation_links
 */
class ModeratedEntityHelperTest extends ModerationLinkTestBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $currentUser;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $database;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityRepository;

  /**
   * The moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderationInformation;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Initialize current user mock.
    $this->currentUser = $this->createMock(AccountInterface::class);

    // Initialize database mock.
    $this->database = $this->createMock(Connection::class);

    // Initialize entity repository.
    $this->entityRepository = $this->createMock(EntityRepositoryInterface::class);

    // Initialize moderation information mock.
    $this->moderationInformation = $this->createMock(ModerationInformationInterface::class);

    // Initialize time mock.
    $this->time = $this->createMock(TimeInterface::class);
  }

  /**
   * Data provider for discarding pending revision(s).
   *
   * @return array
   *   The test data.
   */
  public function dataProviderDiscardLatestVersion(): array {
    return [
      'not-has-revision-in-default-revision-state' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-is-default-revision-state' => [
        TRUE,
        FALSE,
        FALSE,
      ],
      'is-default-revision-state' => [
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests discarding pending revision(s).
   *
   * @covers ::discardLatestVersion
   *
   * @dataProvider dataProviderDiscardLatestVersion
   */
  public function testDiscardLatestVersion(bool $has_revision_in_default_revision_state, bool $is_default_revision_state, bool $expected): void {
    $revision_log_message = 'test message';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare revisions.
    $revision = $this->createMock(ContentEntityInterface::class);

    $revisions = [$revision];

    $new_revision = $this->createMock(ContentEntityInterface::class);

    $new_revision->expects($has_revision_in_default_revision_state && $is_default_revision_state ? $this->once() : $this->never())
      ->method('save');

    // Prepare current moderation state mock.
    $state_current = $this->createMock(ContentModerationState::class);

    $state_current->expects($has_revision_in_default_revision_state ? $this->once() : $this->never())
      ->method('isDefaultRevisionState')
      ->willReturn($is_default_revision_state);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getCurrentModerationState',
      'getRevisions',
      'hasRevisionInDefaultRevisionState',
      'prepareNewRevision',
    ]);

    $class->expects($this->once())
      ->method('hasRevisionInDefaultRevisionState')
      ->willReturn($has_revision_in_default_revision_state);

    $class->expects($has_revision_in_default_revision_state ? $this->once() : $this->never())
      ->method('getRevisions')
      ->with($entity)
      ->willReturn($revisions);

    $class->expects($has_revision_in_default_revision_state ? $this->once() : $this->never())
      ->method('getCurrentModerationState')
      ->with($revision)
      ->willReturn($state_current);

    $class->expects($has_revision_in_default_revision_state && $is_default_revision_state ? $this->once() : $this->never())
      ->method('prepareNewRevision')
      ->with($revision, $revision_log_message)
      ->willReturn($new_revision);

    $this->assertEquals($expected, $class->discardLatestVersion($entity, $revision_log_message));
  }

  /**
   * Data provider for getting the current content moderation state.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetCurrentModerationState(): array {
    return [
      'current-state' => [
        TRUE,
        TRUE,
        TRUE,
      ],
      'not-has-workflow-type' => [
        FALSE,
        TRUE,
        TRUE,
      ],
      'not-has-moderation-state-value' => [
        TRUE,
        FALSE,
        TRUE,
      ],
      'not-has-state' => [
        TRUE,
        TRUE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests getting the current content moderation state of a given entity.
   *
   * @param bool $has_workflow_type
   *   Whether the entity uses a content moderation workflow type.
   * @param bool $has_moderation_state_value
   *   Whether the entity has a content moderation state value.
   * @param bool $has_state
   *   Whetehr the entity is associated to a content moderation state.
   *
   * @covers ::getCurrentModerationState
   *
   * @dataProvider dataProviderGetCurrentModerationState
   */
  public function testGetCurrentModerationState(bool $has_workflow_type, bool $has_moderation_state_value, bool $has_state): void {
    $moderation_state_value = 'test_content_moderation_state';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare content moderation workflow type mock.
    $worfklow_type = $this->createMock(ContentModerationInterface::class);

    // Prepare content moderation state mock.
    $moderation_state = $this->createMock(ContentModerationState::class);

    $worfklow_type->expects($this->atMost(1))
      ->method('getState')
      ->with($moderation_state_value)
      ->willReturn($has_state ? $moderation_state : NULL);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getModerationStateValueFromEntity',
      'getWorkflowType',
    ]);

    $class->expects($this->once())
      ->method('getWorkflowType')
      ->with($entity)
      ->willReturn($has_workflow_type ? $worfklow_type : NULL);

    $class->expects($this->once())
      ->method('getModerationStateValueFromEntity')
      ->with($entity)
      ->willReturn($has_moderation_state_value ? $moderation_state_value : NULL);

    $this->assertEquals($has_workflow_type && $has_moderation_state_value && $has_state ? $moderation_state : NULL, $class->getCurrentModerationState($entity));
  }

  /**
   * Data provider for returning the moderation state value from given entity.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderModerationStateValueFromEntity(): array {
    return [
      'moderation-state-value' => [
        'test_value',
        'test_value',
      ],
      'no-moderation-state-value' => [
        NULL,
        NULL,
      ],
      'no-moderation-state-string-value' => [
        TRUE,
        NULL,
      ],
    ];
  }

  /**
   * Tests returning the moderation state value from the given entity.
   *
   * @param string|bool|null $value
   *   The moderation state value.
   * @param string|null $expected
   *   The expected value.
   *
   * @covers ::getModerationStateValueFromEntity
   *
   * @dataProvider dataProviderModerationStateValueFromEntity
   */
  public function testModerationStateValueFromEntity($value, ?string $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    if (isset($value)) {
      // @phpstan-ignore-next-line
      $entity->moderation_state = (object) [
        'value' => $value,
      ];
    }

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected, $class->getModerationStateValueFromEntity($entity));
  }

  /**
   * Data provider for returning moderation states with default revision state.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetModerationStatesWithDefaultRevisionState(): array {
    return [
      'has-workflow-type' => [
        TRUE,
      ],
      'not-has-workflow-type' => [
        FALSE,
      ],
    ];
  }

  /**
   * Tests returning all content moderation states with default revision state.
   *
   * @param bool $has_workflow_type
   *   Whether the entity uses a content moderation workflow type.
   *
   * @covers ::getModerationStatesWithDefaultRevisionState
   *
   * @dataProvider dataProviderGetModerationStatesWithDefaultRevisionState
   */
  public function testGetModerationStatesWithDefaultRevisionState(bool $has_workflow_type): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare moderation state mocks.
    $moderation_state_not_default_revision_state = $this->createMock(ContentModerationState::class);
    $moderation_state_not_default_revision_state->expects($this->atMost(1))
      ->method('isDefaultRevisionState')
      ->willReturn(FALSE);

    $moderation_state_default_revision_state = $this->createMock(ContentModerationState::class);
    $moderation_state_default_revision_state->expects($this->atMost(1))
      ->method('isDefaultRevisionState')
      ->willReturn(TRUE);

    $moderation_state_default_revision_state_other = $this->createMock(ContentModerationState::class);
    $moderation_state_default_revision_state_other->expects($this->atMost(1))
      ->method('isDefaultRevisionState')
      ->willReturn(TRUE);

    $moderation_state_not_content_moderation_state = $this->createMock(StateInterface::class);

    $moderation_states = [
      'not_default_revision_state' => $moderation_state_not_default_revision_state,
      'default_revision_state' => $moderation_state_default_revision_state,
      'not_content_moderation_state' => $moderation_state_not_content_moderation_state,
      'other_default_revision_state' => $moderation_state_default_revision_state_other,
    ];

    // Prepare content moderation workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    $workflow_type->expects($this->atMost(1))
      ->method('getStates')
      ->willReturn($moderation_states);

    $class = $this->createClassMock([
      'getWorkflowType',
    ]);

    $class->expects($this->once())
      ->method('getWorkflowType')
      ->with($entity)
      ->willReturn($has_workflow_type ? $workflow_type : NULL);

    $this->assertEquals($has_workflow_type ? [
      'default_revision_state' => $moderation_state_default_revision_state,
      'other_default_revision_state' => $moderation_state_default_revision_state_other,
    ] : [], $class->getModerationStatesWithDefaultRevisionState($entity));
  }

  /**
   * Tests returning all language-specific revision IDs of the given entity.
   *
   * @covers ::getRevisionIds
   */
  public function testGetRevisionIds(): void {
    $entity_type_id = 'test_entity_type';
    $entity_id = 123;
    $revision_ids = [
      456,
      789,
      1011,
    ];

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $entity->expects($this->exactly(2))
      ->method('id')
      ->willReturn($entity_id);

    // Prepare select result mock.
    $result = $this->createMock(StatementInterface::class);

    $result->expects($this->once())
      ->method('fetchCol')
      ->willReturn($revision_ids);

    // Prepare database query mock.
    $query = $this->createMock(SelectInterface::class);

    $query->expects($this->once())
      ->method('execute')
      ->willReturn($result);

    // Prepare class mock.
    $class = $this->createClassMock([
      'queryGetRevisionIds',
    ]);

    $class->expects($this->once())
      ->method('queryGetRevisionIds')
      ->with($entity)
      ->willReturn($query);

    // Run twice to check static cache.
    $this->assertEquals($revision_ids, $class->getRevisionIds($entity));
    $this->assertEquals($revision_ids, $class->getRevisionIds($entity));
  }

  /**
   * Data provider for returning all language-specific revisions of entity.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetRevisions(): array {
    return [
      'not-revisionable' => [
        FALSE,
        FALSE,
      ],
      'revisionable' => [
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Test returning all language-specific revisions of the given entity.
   *
   * @param bool $is_revisionable
   *   Whether the entity is revisionable.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getRevisions
   *
   * @dataProvider dataProviderGetRevisions
   */
  public function testGetRevisions(bool $is_revisionable, bool $expected): void {
    $entity_type_id = 'test_entity_type';
    $langcode = 'xyz';
    $revisions = [
      123 => $this->createMock(ContentEntityInterface::class),
      124 => $this->createMock(ContentEntityInterface::class),
      125 => $this->createMock(ContentEntityInterface::class),
      126 => $this->createMock(ContentEntityInterface::class),
    ];

    // Prepare language mock.
    $language = $this->createMock(LanguageInterface::class);

    $language->expects($is_revisionable ? $this->once() : $this->never())
      ->method('getId')
      ->willReturn($langcode);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $entity->expects($is_revisionable ? $this->once() : $this->never())
      ->method('language')
      ->willReturn($language);

    // Prepare storage mock.
    $storage = $is_revisionable ? $this->createMock(RevisionableStorageInterface::class) : $this->createMock(EntityStorageInterface::class);

    if ($is_revisionable && $storage instanceof RevisionableStorageInterface) {
      $storage->expects($this->once())
        ->method('loadMultipleRevisions')
        ->with(array_keys($revisions))
        ->willReturn($revisions);
    }

    // Prepare entity repository mock.
    $this->entityRepository->expects($is_revisionable ? $this->exactly(count($revisions)) : $this->never())
      ->method('getTranslationFromContext')
      // @todo Implement better solution for consecutive calls.
      ->withConsecutive(...array_map(function (ContentEntityInterface $value): array {
        return [$value];
      }, $revisions))
      ->willReturnOnConsecutiveCalls(...$revisions);

    // Prepare entity type manager mock.
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with($entity_type_id)
      ->willReturn($storage);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getRevisionIds',
    ]);

    $class->expects($is_revisionable ? $this->once() : $this->never())
      ->method('getRevisionIds')
      ->with($entity)
      ->willReturn(array_keys($revisions));

    $this->assertEquals($expected ? $revisions : [], $class->getRevisions($entity));
  }

  /**
   * Data provider for returning the transition from current state to state ID.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetTransitionFromCurrentStateTo(): array {
    return [
      'not-has-workflow' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-workflow-type' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-moderation-state-value' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-has-transition' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
      'has-transition' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Test returning the transition from current state to given state ID.
   *
   * @param bool $has_workflow
   *   Whether a related workflow is available.
   * @param bool $has_workflow_type
   *   Whether a related workflow type is available.
   * @param bool $has_moderation_state_value
   *   Whether the current moderation state value is available.
   * @param bool $has_transition
   *   Whether the requested transition is available.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getTransitionFromCurrentStateTo
   *
   * @dataProvider dataProviderGetTransitionFromCurrentStateTo
   */
  public function testGetTransitionFromCurrentStateTo(bool $has_workflow, bool $has_workflow_type, bool $has_moderation_state_value, bool $has_transition, bool $expected): void {
    $state_current_id = 'test_state_current';
    $state_to_id = 'test_state_to';

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    // Prepare workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    $workflow_type->expects($has_workflow && $has_workflow_type && $has_moderation_state_value ? $this->once() : $this->never())
      ->method('hasTransitionFromStateToState')
      ->with($state_current_id, $state_to_id)
      ->willReturn($has_transition);

    $workflow_type->expects($has_workflow && $has_workflow_type && $has_moderation_state_value && $has_transition ? $this->once() : $this->never())
      ->method('getTransitionFromStateToState')
      ->with($state_current_id, $state_to_id)
      ->willReturn($transition);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($has_workflow ? $this->once() : $this->never())
      ->method('getTypePlugin')
      ->willReturn($has_workflow_type ? $workflow_type : NULL);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('getWorkflowForEntity')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow : NULL);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getModerationStateValueFromEntity',
    ]);

    $class->expects($has_workflow && $has_workflow_type ? $this->once() : $this->never())
      ->method('getModerationStateValueFromEntity')
      ->with($entity)
      ->willReturn($has_moderation_state_value ? $state_current_id : NULL);

    $this->assertEquals($expected ? $transition : NULL, $class->getTransitionFromCurrentStateTo($entity, $state_to_id));
  }

  /**
   * Tests getting the workflow for the given content entity.
   *
   * @covers ::getWorkflow
   */
  public function testGetWorkflow(): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('getWorkflowForEntity')
      ->with($entity)
      ->willReturn($workflow);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($workflow, $class->getWorkflow($entity));
  }

  /**
   * Data provider for getting the content moderation workflow type plugin.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetWorkflowType(): array {
    return [
      'has-workflow' => [
        TRUE,
      ],
      'not-has-workflow' => [
        FALSE,
      ],
    ];
  }

  /**
   * Tests getting the content moderation workflow type plugin for an entity.
   *
   * @param bool $has_workflow
   *   Whether the entity is associated to a content moderation workflow.
   *
   * @covers ::getWorkflowType
   *
   * @dataProvider dataProviderGetWorkflowType
   */
  public function testGetWorkflowType(bool $has_workflow): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare content moderation workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($has_workflow ? $this->once() : $this->never())
      ->method('getTypePlugin')
      ->willReturn($workflow_type);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getWorkflow',
    ]);

    $class->expects($this->once())
      ->method('getWorkflow')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow : NULL);

    $this->assertEquals($has_workflow ? $workflow_type : NULL, $class->getWorkflowType($entity));
  }

  /**
   * Data provider for entity has revision in default revision state.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderHasRevisionInDefaultRevisionState(): array {
    return [
      'no-revision-in-default-revision-state' => [
        0,
        FALSE,
      ],
      'revision-in-default-revision-state' => [
        1,
        TRUE,
      ],
    ];
  }

  /**
   * Tests given content entity has revision in default revision state.
   *
   * @param int $count
   *   The number of revisions in default revision state.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::hasRevisionInDefaultRevisionState
   *
   * @dataProvider dataProviderHasRevisionInDefaultRevisionState
   */
  public function testHasRevisionInDefaultRevisionState(int $count, bool $expected): void {
    $entity_type_id = 'test_entity_type';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $entity->expects($this->exactly(2))
      ->method('id')
      ->willReturn($count);

    $entity->expects($this->exactly(2))
      ->method('getRevisionId')
      ->willReturn($count);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    // Prepare query result mock.
    $result = $this->createMock(StatementInterface::class);

    $result->expects($this->once())
      ->method('fetchField')
      ->willReturn($count);

    // Prepare count query mock.
    $count_query = $this->createMock(SelectInterface::class);

    $count_query->expects($this->once())
      ->method('execute')
      ->willReturn($result);

    // Prepare query mock.
    $query = $this->createMock(SelectInterface::class);

    $query->expects($this->once())
      ->method('countQuery')
      ->willReturn($count_query);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getWorkflow',
      'queryHasRevisionInDefaultRevisionState',
    ]);

    $class->expects($this->once())
      ->method('getWorkflow')
      ->with($entity)
      ->willReturn($workflow);

    $class->expects($this->once())
      ->method('queryHasRevisionInDefaultRevisionState')
      ->with($entity, $workflow)
      ->willReturn($query);

    // Run twice to check static cache.
    $this->assertEquals($expected, $class->hasRevisionInDefaultRevisionState($entity));
    $this->assertEquals($expected, $class->hasRevisionInDefaultRevisionState($entity));
  }

  /**
   * Data provider for if given content entity is a discardable latest version.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderIsDiscardableLatestVersion(): array {
    return [
      'published-entity' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-state-current' => [
        FALSE,
        FALSE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'default-revision-state' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'has-pending-revision' => [
        FALSE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
        TRUE,
      ],
      'has-revision-in-default-revision-state' => [
        FALSE,
        TRUE,
        FALSE,
        FALSE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests if the given content entity is a discardable latest version.
   *
   * @param bool $is_published
   *   Whether the entity is published.
   * @param bool $has_state_current
   *   Whether the current content moderation state is available.
   * @param bool $is_default_revision_state
   *   Whether it is the default revision state.
   * @param bool $has_pending_revision
   *   Whether the entity has pending revisions.
   * @param bool $has_revision_in_default_revision_state
   *   Whether the entity has a revision in default revision state.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::isDiscardableLatestVersion
   *
   * @dataProvider dataProviderIsDiscardableLatestVersion
   */
  public function testIsDiscardableLatestVersion(bool $is_published, bool $has_state_current, bool $is_default_revision_state, bool $has_pending_revision, bool $has_revision_in_default_revision_state, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(EditorialContentEntityBase::class);

    $entity->expects($this->once())
      ->method('isPublished')
      ->willReturn($is_published);

    // Prepare current content moderation state mock.
    $state_current = $this->createMock(ContentModerationState::class);

    $state_current->expects(!$is_published && $has_state_current ? $this->once() : $this->never())
      ->method('isDefaultRevisionState')
      ->willReturn($is_default_revision_state);

    // Prepare moderation information mock.
    $this->moderationInformation->expects(!$is_published && $has_state_current && !$is_default_revision_state ? $this->once() : $this->never())
      ->method('hasPendingRevision')
      ->with($entity)
      ->willReturn($has_pending_revision);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getCurrentModerationState',
      'hasRevisionInDefaultRevisionState',
    ]);

    $class->expects(!$is_published ? $this->once() : $this->never())
      ->method('getCurrentModerationState')
      ->with($entity)
      ->willReturn($has_state_current ? $state_current : NULL);

    $class->expects(!$is_published && $has_state_current && !$is_default_revision_state && !$has_pending_revision ? $this->once() : $this->never())
      ->method('hasRevisionInDefaultRevisionState')
      ->with($entity)
      ->willReturn($has_revision_in_default_revision_state);

    $this->assertEquals($expected, $class->isDiscardableLatestVersion($entity));
  }

  /**
   * Data provider for given content entity is restored as latest version.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderIsRestoredAsLatestVersion(): array {
    return [
      'restored-as-latest-revision' => [
        FALSE,
        FALSE,
        TRUE,
        TRUE,
      ],
      'has-pending-revision' => [
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'is-default-revision-state' => [
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-revision-in-default-revision-state' => [
        FALSE,
        FALSE,
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests if given content entity is restored as latest version.
   *
   * @param bool $has_pending_revision
   *   Whether the entity has a pending revision.
   * @param bool $is_default_revision_state
   *   Whether the current moderation state is in default revision state.
   * @param bool $has_revision_in_default_revision_state
   *   Whether the entity has a revision in default revision state.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::isRestoredAsLatestVersion
   *
   * @dataProvider dataProviderIsRestoredAsLatestVersion
   */
  public function testIsRestoredAsLatestVersion(bool $has_pending_revision, bool $is_default_revision_state, bool $has_revision_in_default_revision_state, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare content moderation state mockup.
    $moderation_state = $this->createMock(ContentModerationState::class);

    $moderation_state->expects($this->atMost(1))
      ->method('isDefaultRevisionState')
      ->willReturn($is_default_revision_state);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('hasPendingRevision')
      ->with($entity)
      ->willReturn($has_pending_revision);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getCurrentModerationState',
      'hasRevisionInDefaultRevisionState',
    ]);

    $class->expects($this->once())
      ->method('getCurrentModerationState')
      ->with($entity)
      ->willReturn($moderation_state);

    $class->expects($this->atMost(1))
      ->method('hasRevisionInDefaultRevisionState')
      ->with($entity)
      ->willReturn($has_revision_in_default_revision_state);

    $this->assertEquals($expected, $class->isRestoredAsLatestVersion($entity));
  }

  /**
   * Data provider for performing a moderation transition for the given entity.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderPerformTransition(): array {
    return [
      'not-has-transition' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-has-state-to-transition-to' => [
        TRUE,
        FALSE,
        FALSE,
      ],
      'performs-transition' => [
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests performing a moderation transition for the given content entity.
   *
   * @param bool $has_transition
   *   Whether the workflow transition is available.
   * @param bool $has_state_to
   *   Whether the content moderation state to transition to is available.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::performTransition
   *
   * @dataProvider dataProviderPerformTransition
   */
  public function testPerformTransition(bool $has_transition, bool $has_state_to, bool $expected): void {
    $revision_log_message = 'test message';
    $state_to_id = 'test_state_to';
    $is_default_revision_state = TRUE;

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare content moderation state to transition to mock.
    $state_to = $this->createMock(ContentModerationState::class);

    $state_to->expects($has_transition && $has_state_to ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($state_to_id);

    $state_to->expects($has_transition && $has_state_to ? $this->once() : $this->never())
      ->method('isDefaultRevisionState')
      ->willReturn($is_default_revision_state);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($has_transition ? $this->once() : $this->never())
      ->method('to')
      ->willReturn($has_state_to ? $state_to : NULL);

    // Prepare entity revision mock.
    $revision = $this->createMock(ContentEntityInterface::class);

    $revision->expects($has_transition && $has_state_to ? $this->once() : $this->never())
      ->method('isDefaultRevision')
      ->with($is_default_revision_state);

    $revision->expects($has_transition && $has_state_to ? $this->once() : $this->never())
      ->method('isDefaultRevision')
      ->with($is_default_revision_state);

    $revision->expects($has_transition && $has_state_to ? $this->once() : $this->never())
      ->method('set')
      ->with('moderation_state', $state_to_id);

    $revision->expects($has_transition && $has_state_to ? $this->once() : $this->never())
      ->method('save');

    // Prepare class mock.
    $class = $this->createClassMock([
      'getTransitionFromCurrentStateTo',
      'prepareNewRevision',
    ]);

    $class->expects($this->once())
      ->method('getTransitionFromCurrentStateTo')
      ->with($entity, $state_to_id)
      ->willReturn($has_transition ? $transition : NULL);

    $class->expects($has_transition && $has_state_to ? $this->once() : $this->never())
      ->method('prepareNewRevision')
      ->with($entity, $revision_log_message)
      ->willReturn($revision);

    $this->assertEquals($expected, $class->performTransition($entity, $state_to_id, $revision_log_message));
  }

  /**
   * Data provider for preparing a new revision of a given content entity.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderPrepareNewRevision(): array {
    return [
      'revisionable-with-revision-log-message' => [
        TRUE,
        'Test revision log message',
      ],
      'revisionable-without-revision-log-message' => [
        TRUE,
        NULL,
      ],
      'not-revisionable' => [
        FALSE,
        NULL,
      ],
    ];
  }

  /**
   * Test preparing a new revision of a given content entity.
   *
   * @param bool $revisionable
   *   Whether the entity type is revisionable.
   * @param string|null $revision_log_message
   *   The revision log message (or NULL for no revision log message).
   *
   * @covers ::prepareNewRevision
   *
   * @dataProvider dataProviderPrepareNewRevision
   */
  public function testPrepareNewRevision(bool $revisionable, ?string $revision_log_message): void {
    // Prepare current user mock.
    $uid = 123;
    $this->currentUser->expects($revisionable ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($uid);

    // Prepare time service mock.
    $request_time = 12345;
    $this->time->expects($revisionable ? $this->once() : $this->never())
      ->method('getRequestTime')
      ->willReturn($request_time);

    // Prepare entity mock.
    $entity_type_id = 'test_entity_type';
    $entity = $this->createMock(NodeInterface::class);

    $entity->expects($this->once())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    // Prepare entity revision mock.
    $revision = $this->createMock(NodeInterface::class);

    $revision->expects($revisionable && isset($revision_log_message) ? $this->once() : $this->never())
      ->method('setRevisionLogMessage')
      ->with($revision_log_message);

    $revision->expects($revisionable ? $this->once() : $this->never())
      ->method('setRevisionCreationTime')
      ->with($request_time);

    $revision->expects($revisionable ? $this->once() : $this->never())
      ->method('setRevisionUserId')
      ->with($uid);

    // Prepare entity storage mock.
    if ($revisionable) {
      $entity_storage = $this->createMock(ContentEntityStorageInterface::class);

      $entity_storage->expects($this->once())
        ->method('createRevision')
        ->with($entity)
        ->willReturn($revision);
    }
    else {
      $entity_storage = $this->createMock(EntityStorageInterface::class);
    }

    // Prepare entity type manager mock.
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with($entity_type_id)
      ->willReturn($entity_storage);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($revision, $class->prepareNewRevision($entity, $revision_log_message));
  }

  /**
   * Data provider for preparing the all language-specific revision IDs query.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderQueryGetRevisionIds(): array {
    return [
      'not-revisionable' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-entity-key-id' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-entity-key-revision-id' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-entity-key-langcode' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-has-entity-revision-table' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
      'all-requirements' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Test preparing the query for all language-specific revision IDs of entity.
   *
   * @param bool $is_revisionable
   *   Whether the entity is revisionable.
   * @param bool $has_entity_key_id
   *   Whether the 'id' entity key is available.
   * @param bool $has_entity_key_revision_id
   *   Whether the 'revision' entity key is available.
   * @param bool $has_entity_key_langcode
   *   Whether the 'langcode' entity key is available.
   * @param bool $has_entity_revision_table
   *   Whether the entity revision table is available.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::queryGetRevisionIds
   *
   * @dataProvider dataProviderQueryGetRevisionIds
   */
  public function testQueryGetRevisionIds(bool $is_revisionable, bool $has_entity_key_id, bool $has_entity_key_revision_id, bool $has_entity_key_langcode, bool $has_entity_revision_table, bool $expected): void {
    $has_all_requirements = $is_revisionable && $has_entity_key_id && $has_entity_key_revision_id && $has_entity_key_langcode && $has_entity_revision_table;
    $entity_id = 123;
    $entity_revision_id = 456;
    $entity_langcode = 'xyz';
    $entity_key_id = 'test_id';
    $entity_key_revision_id = 'test_revision_id';
    $entity_key_langcode = 'test_langcode';
    $entity_revision_table = 'test_revision_table';

    // Prepare entity type mock.
    $entity_type = $this->createMock(ContentEntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('isRevisionable')
      ->willReturn($is_revisionable);

    $entity_type->expects($is_revisionable ? $this->exactly(3) : $this->never())
      ->method('getKey')
      // @todo Implement better solution for consecutive calls.
      ->willReturnCallback(fn(string $key) => match ($key) {
        'id' => $has_entity_key_id ? $entity_key_id : FALSE,
        'revision' => $has_entity_key_revision_id ? $entity_key_revision_id : FALSE,
        'langcode' => $has_entity_key_langcode ? $entity_key_langcode : FALSE,
        default => throw new \LogicException('Parameter does not match expected value.'),
      });

    $entity_type->expects($is_revisionable ? $this->once() : $this->never())
      ->method('getRevisionTable')
      ->willReturn($has_entity_revision_table ? $entity_revision_table : NULL);

    // Prepare language mock.
    $language = $this->createMock(LanguageInterface::class);

    $language->expects($is_revisionable ? $this->once() : $this->never())
      ->method('getId')
      ->willReturn($entity_langcode);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('getEntityType')
      ->willReturn($entity_type);

    $entity->expects($is_revisionable ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($entity_id);

    $entity->expects($is_revisionable ? $this->once() : $this->never())
      ->method('getRevisionId')
      ->willReturn($entity_revision_id);

    $entity->expects($is_revisionable ? $this->once() : $this->never())
      ->method('language')
      ->willReturn($language);

    // Prepare database query mock.
    $query = $this->createMock(SelectInterface::class);

    $query->expects($has_all_requirements ? $this->once() : $this->never())
      ->method('fields')
      ->with('r', [$entity_key_revision_id])
      ->willReturn($query);

    $query->expects($has_all_requirements ? $this->exactly(3) : $this->never())
      ->method('condition')
      // @todo Implement better solution for consecutive calls.
      ->willReturnCallback(fn(string $field, $value = NULL, string $operator = '=') => match (TRUE) {
        $field === $entity_key_id && $value === (string) $entity_id => $query,
        $field === $entity_key_revision_id && $value === (string) $entity_revision_id && $operator === '<>' => $query,
        $field === $entity_key_langcode && $value === $entity_langcode => $query,
        default => throw new \LogicException('Parameters do not match expected values.'),
      });

    $query->expects($has_all_requirements ? $this->once() : $this->never())
      ->method('orderBy')
      ->with($entity_key_revision_id, 'DESC')
      ->willReturn($query);

    // Prepare database mock.
    $this->database->expects($has_all_requirements ? $this->once() : $this->never())
      ->method('select')
      ->with($entity_revision_table, 'r')
      ->willReturn($query);

    // Prepare class mock.
    $class = $this->createClassMock();

    $actual = $class->queryGetRevisionIds($entity);

    if ($expected) {
      $this->assertInstanceOf(SelectInterface::class, $actual);
      $this->assertEquals($query, $actual);
    }
    else {
      $this->assertNull($actual);
    }
  }

  /**
   * Data provider for preparing the 'has revision in default state' query.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderQueryHasRevisionInDefaultRevisionState(): array {
    return [
      'not-has-content-moderation-state-revision-data-table' => [
        FALSE,
        FALSE,
        FALSE,
        FALSE,
      ],
      'new-revision' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
      'not-new-revision' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
      ],
      'not-translatable' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests preparing the query for if it has revision in default revision state.
   *
   * @param bool $has_content_moderation_state_revision_data_table
   *   Whether the content moderation state revision data table is available.
   * @param bool $is_new_revision
   *   Whether it is a new revision.
   * @param bool $is_translatable
   *   Whether the entity is translatable.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::queryHasRevisionInDefaultRevisionState
   *
   * @dataProvider dataProviderQueryHasRevisionInDefaultRevisionState
   */
  public function testQueryHasRevisionInDefaultRevisionState(bool $has_content_moderation_state_revision_data_table, bool $is_new_revision, bool $is_translatable, bool $expected): void {
    $entity_type_id = 'test_entity_type';
    $entity_id = 1234;
    $entity_revision_id = 456;
    $entity_revision_id_loaded = 789;
    $workflow_id = 'workflow_id';
    $langcode = 'xyz';
    $moderation_state_with_default_revision_state_id = 987;
    $content_moderation_state_revision_data_table = 'test_content_moderation_state_revision_data_table';

    // Prepare language mock.
    $language = $this->createMock(LanguageInterface::class);

    $language->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('getId')
      ->willReturn($langcode);

    // Prepare content moderation state(s) with default revision state mock.
    $moderation_state_with_default_revision_state = $this->createMock(ContentModerationState::class);

    $moderation_states_with_default_revision_state = [
      $moderation_state_with_default_revision_state_id => $moderation_state_with_default_revision_state,
    ];

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $entity->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($entity_id);

    $entity->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('isNewRevision')
      ->willReturn($is_new_revision);

    $entity->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('getRevisionId')
      ->willReturn($entity_revision_id);

    $entity->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('getLoadedRevisionId')
      ->willReturn($entity_revision_id_loaded);

    $entity->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('language')
      ->willReturn($language);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($workflow_id);

    // Prepare content moderation state entity type definition mock.
    $entity_type_content_moderation_state = $this->createMock(EntityTypeInterface::class);

    $entity_type_content_moderation_state->expects($this->once())
      ->method('getRevisionDataTable')
      ->willReturn($has_content_moderation_state_revision_data_table ? $content_moderation_state_revision_data_table : NULL);

    $entity_type_content_moderation_state->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('isTranslatable')
      ->willReturn($is_translatable);

    // Prepare entity type manager mock.
    $this->entityTypeManager->expects($this->once())
      ->method('getDefinition')
      ->with('content_moderation_state')
      ->willReturn($entity_type_content_moderation_state);

    // Prepare database query mock.
    $query = $this->createMock(SelectInterface::class);

    $consecutive_condition_args = [
      ['workflow', $workflow_id],
      ['moderation_state', [$moderation_state_with_default_revision_state_id]],
      ['content_entity_type_id', $entity_type_id],
      ['content_entity_id', $entity_id],
      [
        'content_entity_revision_id',
        $is_new_revision ? $entity_revision_id_loaded : $entity_revision_id,
        '<>',
      ],
    ];

    if ($is_translatable) {
      $consecutive_condition_args[] = ['langcode', $langcode];
    }

    $query->expects($has_content_moderation_state_revision_data_table ? $this->exactly(count($consecutive_condition_args)) : $this->never())
      ->method('condition')
      // @todo Implement better solution for consecutive calls.
      ->withConsecutive(...$consecutive_condition_args)
      ->willReturnOnConsecutiveCalls(...array_fill(0, count($consecutive_condition_args), $query));

    // Prepare database mock.
    $this->database->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('select')
      ->with($content_moderation_state_revision_data_table)
      ->willReturn($query);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getModerationStatesWithDefaultRevisionState',
    ]);

    $class->expects($has_content_moderation_state_revision_data_table ? $this->once() : $this->never())
      ->method('getModerationStatesWithDefaultRevisionState')
      ->with($entity)
      ->willReturn($moderation_states_with_default_revision_state);

    $actual = $class->queryHasRevisionInDefaultRevisionState($entity, $workflow);

    if ($expected) {
      $this->assertInstanceOf(SelectInterface::class, $actual);
      $this->assertEquals($query, $actual);
    }
    else {
      $this->assertNull($actual);
    }
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\ModeratedEntityHelper_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModeratedEntityHelper_Test|MockObject {
    return $this->getMockBuilder(ModeratedEntityHelper_Test::class)
      ->setConstructorArgs([
        $this->entityTypeManager,
        $this->entityRepository,
        $this->moderationInformation,
        $this->database,
        $this->currentUser,
        $this->time,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked moderated entity helper class for tests.
 */
class ModeratedEntityHelper_Test extends ModeratedEntityHelper {

  /**
   * {@inheritdoc}
   */
  public function getModerationStateValueFromEntity(ContentEntityInterface $entity): ?string {
    return parent::getModerationStateValueFromEntity($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getModerationStatesWithDefaultRevisionState(ContentEntityInterface $entity): array {
    return parent::getModerationStatesWithDefaultRevisionState($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionIds(ContentEntityInterface $entity): array {
    return parent::getRevisionIds($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisions(ContentEntityInterface $entity): array {
    return parent::getRevisions($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitionFromCurrentStateTo(ContentEntityInterface $entity, string $state_to_id): ?TransitionInterface {
    return parent::getTransitionFromCurrentStateTo($entity, $state_to_id);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareNewRevision(ContentEntityInterface $entity, ?string $revision_log_message = NULL): ContentEntityInterface {
    return parent::prepareNewRevision($entity, $revision_log_message);
  }

  /**
   * {@inheritdoc}
   */
  public function queryGetRevisionIds(ContentEntityInterface $entity): ?SelectInterface {
    return parent::queryGetRevisionIds($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function queryHasRevisionInDefaultRevisionState(ContentEntityInterface $entity, WorkflowInterface $workflow): ?SelectInterface {
    return parent::queryHasRevisionInDefaultRevisionState($entity, $workflow);
  }

}
// @codingStandardsIgnoreEnd
