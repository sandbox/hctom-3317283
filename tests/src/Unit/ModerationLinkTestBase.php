<?php

namespace Drupal\Tests\content_moderation_links\Unit;

use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\ModeratedEntityHelperInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\workflows\TransitionInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Base class for content moderation link unit tests.
 */
abstract class ModerationLinkTestBase extends UnitTestCase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The entity type repository.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeRepository;

  /**
   * The moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderationInformation;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Initialize entity repository mock.
    $this->entityTypeRepository = $this->createMock(EntityTypeRepositoryInterface::class);

    // Initialize entity type manager mock.
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);

    // Initialize moderation information.
    $this->moderationInformation = $this->createMock(ModerationInformationInterface::class);

    // Initialize container with string translation stub and entity type manager
    // mock.
    $container = new ContainerBuilder();
    $container->set('entity_type.repository', $this->entityTypeRepository);
    $container->set('entity_type.manager', $this->entityTypeManager);
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);
  }

  /**
   * Creates and returns a test content moderation transition mock.
   *
   * @param string $state_from_id
   *   The ID of the content moderation state to transition from (defaults to
   *   'test_from').
   * @param string $state_to_id
   *   The ID of the content moderation state to transition to (defaults to
   *   'test_to').
   * @param string $transition_label
   *   Optional label of the mocked transition (defaults to 'Test transition').
   * @param int $transition_weight
   *   Optional weight of the mocked transition (defaults to '0').
   *
   * @return \Drupal\workflows\TransitionInterface|\PHPUnit\Framework\MockObject\MockObject
   *   THe mocked content moderation state transition.
   */
  protected function createTransitionMock(string $state_from_id = 'test_from', string $state_to_id = 'test_to', string $transition_label = 'Test transition', int $transition_weight = 0) {
    // Prepare 'from' content moderation state mockup.
    $state_from = $this->createMock(ContentModerationState::class);

    $state_from->expects($this->atLeastOnce())
      ->method('id')
      ->willReturn($state_from_id);

    // Prepare 'to' content moderation state mockup.
    $state_to = $this->createMock(ContentModerationState::class);

    $state_to->expects($this->atLeastOnce())
      ->method('id')
      ->willReturn($state_to_id);

    // Prepare content moderation state transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->atLeastOnce())
      ->method('from')
      ->willReturn([
        $state_from_id => $state_from,
      ]);

    $transition->expects($this->atLeastOnce())
      ->method('to')
      ->willReturn($state_to);

    $transition->expects($this->atLeastOnce())
      ->method('label')
      ->willReturn($transition_label);

    $transition->expects($this->atLeastOnce())
      ->method('weight')
      ->willReturn($transition_weight);

    return $transition;
  }

  /**
   * Creates/initializes the Workflow::loadMultipleByType() mock.
   *
   * @param string $workflow_id
   *   The ID of the workflow mock.
   * @param \Drupal\workflows\WorkflowInterface|\PHPUnit\Framework\MockObject\MockObject $workflow
   *   The workflow mock.
   */
  protected function createWorkflowLoadMultipleByTypeMock(string $workflow_id, $workflow): void {
    // Prepare entity type repository mock.
    $this->entityTypeRepository->expects($this->atLeastOnce())
      ->method('getEntityTypeFromClass')
      ->willReturn('workflow');

    // Prepare workflow entity query mock.
    $workflow_query = $this->createMock(QueryInterface::class);

    $workflow_query->expects($this->atLeastOnce())
      ->method('condition')
      ->with('type', ModeratedEntityHelperInterface::WORKFLOW_TYPE)
      ->willReturn($workflow_query);

    $workflow_query->expects($this->atLeastOnce())
      ->method('execute')
      ->willReturn([
        $workflow_id => $workflow,
      ]);

    // Prepare workflow storage mock.
    $workflow_storage = $this->createMock(EntityStorageInterface::class);

    $workflow_storage->expects($this->atLeastOnce())
      ->method('getQuery')
      ->with('AND')
      ->willReturn($workflow_query);

    $workflow_storage->expects($this->atLeastOnce())
      ->method('loadMultiple')
      ->willReturn([
        $workflow_id => $workflow,
      ]);

    // Prepare entity type manager mock.
    $this->entityTypeManager->expects($this->atLeastOnce())
      ->method('getStorage')
      ->with('workflow')
      ->willReturn($workflow_storage);
  }

  /**
   * Creates and returns a workflow mock.
   *
   * @param string $workflow_id
   *   The ID of the mocked workflow.
   * @param \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface|\PHPUnit\Framework\MockObject\MockObject|null $workflow_type
   *   Optional type plugin of the mocked workflow (defaults to none).
   * @param array $workflow_cache_contexts
   *   Optional cache contexts for the mocked workflow (defaults to none).
   * @param array $workflow_cache_tags
   *   Optional cache tags for the mocked workflow (defaults to none).
   * @param int $workflow_cache_max_age
   *   Optional cache max-age for the mocked workflow (defaults to
   *   Cache::PERMANENT).
   *
   * @return \Drupal\workflows\WorkflowInterface|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked workflow.
   */
  protected function createWorkflowMock(string $workflow_id, $workflow_type = NULL, array $workflow_cache_contexts = [], array $workflow_cache_tags = [], int $workflow_cache_max_age = Cache::PERMANENT) {
    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($this->any())
      ->method('id')
      ->willReturn($workflow_id);

    $workflow->expects($this->any())
      ->method('getCacheContexts')
      ->willReturn($workflow_cache_contexts);

    $workflow->expects($this->any())
      ->method('getCacheTags')
      ->willReturn($workflow_cache_tags);

    $workflow->expects($this->any())
      ->method('getCacheMaxAge')
      ->willReturn($workflow_cache_max_age);

    if ($workflow_type instanceof ContentModerationInterface) {
      $workflow->expects($this->any())
        ->method('getTypePlugin')
        ->willReturn($workflow_type);
    }

    return $workflow;
  }

}
