<?php

namespace Drupal\Tests\content_moderation_links\Unit\Hook;

use Drupal\content_moderation_links\Hook\WorkflowPresaveHook;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the 'hook_ENTITY_TYPE_presave' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\WorkflowPresaveHook
 * @group content_moderation_links
 */
class WorkflowPresaveHookTest extends UnitTestCase {

  /**
   * Tests acting on a specific type of entity before it is created or updated.
   *
   * @covers ::workflowPresave
   */
  public function testWorkflowPresave(): void {
    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    // Prepare cache tags invalidator mock.
    $cache_tags_invalidator = $this->createMock(CacheTagsInvalidatorInterface::class);

    $cache_tags_invalidator->expects($this->once())
      ->method('invalidateTags')
      ->with([
        'local_action',
      ]);

    $this->getContainerWithCacheTagsInvalidator($cache_tags_invalidator);

    // Prepare class mock.
    $class = $this->createClassMock();

    $class->workflowPresave($entity);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\content_moderation_links\Hook\WorkflowPresaveHook|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): WorkflowPresaveHook|MockObject {
    return $this->getMockBuilder(WorkflowPresaveHook::class)
      ->disableOriginalConstructor()
      ->onlyMethods($only_methods)
      ->getMock();
  }

}
