<?php

namespace Drupal\Tests\content_moderation_links\Unit\Hook;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\Entity\Routing\ModerationLinksRouteSubscriber;
use Drupal\content_moderation_links\Form\DiscardLatestVersionConfirmForm;
use Drupal\content_moderation_links\Form\ModerationStateTransitionConfirmForm;
use Drupal\content_moderation_links\Hook\EntityTypeAlterHook;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the 'hook_entity_type_alter' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\EntityTypeAlterHook
 * @group content_moderation_links
 */
class EntityTypeAlterHookTest extends UnitTestCase {

  /**
   * Data provider for altering the entity type definitions.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderEntityTypeAlter(): array {
    return [
      'not-revisionable' => [
        FALSE,
        FALSE,
        'test_entity_type',
        FALSE,
      ],
      'internal' => [
        TRUE,
        TRUE,
        'test_entity_type',
        FALSE,
      ],
      'path-alias' => [
        TRUE,
        FALSE,
        'path_alias',
        FALSE,
      ],
      'workspace' => [
        TRUE,
        FALSE,
        'workspace',
        FALSE,
      ],
      'alter' => [
        TRUE,
        FALSE,
        'test_entity',
        TRUE,
      ],
    ];
  }

  /**
   * Tests altering the entity type definitions.
   *
   * @param bool $is_revisionable
   *   Whether the entity type is revisionable.
   * @param bool $is_internal
   *   Whether it is an internal entity type.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param bool $expected
   *   The expected valued.
   *
   * @covers ::entityTypeAlter
   *
   * @dataProvider dataProviderEntityTypeAlter
   */
  public function testEntityTypeAlter(bool $is_revisionable, bool $is_internal, string $entity_type_id, bool $expected): void {
    // Prepare entity type mock.
    $entity_type = $this->createMock(ContentEntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('id')
      ->willReturn($entity_type_id);

    $entity_type->expects($this->once())
      ->method('isRevisionable')
      ->willReturn($is_revisionable);

    $entity_type->expects($is_revisionable ? $this->once() : $this->never())
      ->method('isInternal')
      ->willReturn($is_internal);

    // Prepare entity type list mock.
    $entity_types = [
      $entity_type_id => $entity_type,
    ];

    // Prepare class mock.
    $class = $this->createClassMock([
      'addModerationLinksToEntityType',
    ]);

    $class->expects($expected ? $this->once() : $this->never())
      ->method('addModerationLinksToEntityType')
      ->with($entity_type);

    $class->entityTypeAlter($entity_types);
  }

  /**
   * Data provider for modifying entity definition for moderation links support.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAddModerationLinksToEntityType(): array {
    return [
      'no-latest-version-link-template' => [
        FALSE,
        TRUE,
      ],
      'already-has-discard-latest-version-link-template' => [
        TRUE,
        TRUE,
      ],
      'no-discard-latest-version-link-template' => [
        TRUE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests modifying an entity definition to include moderation links support.
   *
   * @param bool $has_latest_version_link_template
   *   Whether the 'Latest version' entity link template exists.
   * @param bool $has_discard_latest_version_link_template
   *   Whether the 'Discard latest version' entity link template exists.
   *
   * @covers ::addModerationLinksToEntityType
   *
   * @dataProvider dataProviderAddModerationLinksToEntityType
   */
  public function testAddModerationLinksToEntityType(bool $has_latest_version_link_template, bool $has_discard_latest_version_link_template): void {
    $adds_link_templates = $has_latest_version_link_template && !$has_discard_latest_version_link_template;
    $link_template_canonical = '/test/canonical';
    $link_template_latest_version = '/test/latest-version';

    // Prepare entity type mock.
    $entity_type = $this->createMock(ContentEntityTypeInterface::class);

    $entity_type->expects($has_latest_version_link_template ? $this->exactly(2) : $this->once())
      ->method('hasLinkTemplate')
      // @todo Implement better solution for consecutive calls.
      ->with(
        $this->logicalOr(
          'latest-version',
          LinkTemplate::DISCARD_LATEST_VERSION,
        ),
      )
      ->willReturnCallback(fn(string $key) => match($key) {
        'latest-version' => $has_latest_version_link_template,
        LinkTemplate::DISCARD_LATEST_VERSION => $has_discard_latest_version_link_template,
        default => throw new \LogicException('Parameter does not match expected value.'),
      });

    $consecutive_get_link_template = [];
    $return_consecutive_get_link_template = [];

    if ($adds_link_templates) {
      $consecutive_get_link_template[] = ['latest-version'];
      $return_consecutive_get_link_template[] = $link_template_latest_version;
    }

    $consecutive_get_link_template[] = ['canonical'];
    $return_consecutive_get_link_template[] = $link_template_canonical;

    $entity_type->expects($adds_link_templates ? $this->exactly(2) : $this->once())
      ->method('getLinkTemplate')
      // @todo Implement better solution for consecutive calls.
      ->withConsecutive(...$consecutive_get_link_template)
      ->willReturnOnConsecutiveCalls(...$return_consecutive_get_link_template);

    $consecutive_set_link_template = [];

    if ($adds_link_templates) {
      $consecutive_set_link_template[] = [
        LinkTemplate::DISCARD_LATEST_VERSION,
        $link_template_latest_version . '/discard',
      ];
    }

    $consecutive_set_link_template[] = [
      LinkTemplate::MODERATION_STATE_TRANSITION,
      $link_template_canonical . '/transition/from/{from}/to/{to}',
    ];

    $entity_type->expects($adds_link_templates ? $this->exactly(2) : $this->once())
      ->method('setLinkTemplate')
      // @todo Implement better solution for consecutive calls.
      ->withConsecutive(...$consecutive_set_link_template);

    $entity_type->expects($this->exactly(2))
      ->method('setFormClass')
      // @todo Implement better solution for consecutive calls.
      ->willReturnCallback(fn(string $operation, string $class) => match(TRUE) {
        $operation === LinkTemplate::DISCARD_LATEST_VERSION && $class === DiscardLatestVersionConfirmForm::class => TRUE,
        $operation === LinkTemplate::MODERATION_STATE_TRANSITION && $class === ModerationStateTransitionConfirmForm::class => TRUE,
        default => throw new \LogicException('Parameters do not match expected values.'),
      });

    $entity_type->expects($this->once())
      ->method('getRouteProviderClasses')
      ->willReturn(NULL);

    $entity_type->expects($this->once())
      ->method('setHandlerClass')
      ->with('route_provider', [
        'content_moderation_links' => ModerationLinksRouteSubscriber::class,
      ]);

    // Prepare class mock.
    $class = $this->createClassMock();

    $actual = $class->addModerationLinksToEntityType($entity_type);

    $this->assertInstanceOf(ContentEntityTypeInterface::class, $actual);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Hook\EntityTypeAlterHook_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): EntityTypeAlterHook_Test|MockObject {
    return $this->getMockBuilder(EntityTypeAlterHook_Test::class)
      ->disableOriginalConstructor()
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked entity operation hook class for tests.
 */
class EntityTypeAlterHook_Test extends EntityTypeAlterHook {

  /**
   * {@inheritdoc}
   */
  public function addModerationLinksToEntityType(ContentEntityTypeInterface $entity_type): ContentEntityTypeInterface {
    return parent::addModerationLinksToEntityType($entity_type);
  }

}
// @codingStandardsIgnoreEnd
