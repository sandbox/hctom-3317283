<?php

namespace Drupal\Tests\content_moderation_links\Unit\Hook;

use Drupal\content_moderation_links\Hook\ModuleImplementsAlterHook;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the 'hook_module_implements_alter' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\ModuleImplementsAlterHook
 * @group content_moderation_links
 */
class ModuleImplementsAlterHookTest extends UnitTestCase {

  /**
   * Data provider for altering the registry of modules implementing a hook.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderModuleImplementsAlter(): array {
    return [
      'with-implementation' => [
        [
          'content_moderation_links' => FALSE,
          'test_module_1' => FALSE,
          'test_module_2' => FALSE,
        ],
        'entity_type_alter',
        [
          'test_module_1' => FALSE,
          'test_module_2' => FALSE,
          'content_moderation_links' => FALSE,
        ],
      ],
      'with-implementation-requests-other-hook' => [
        [
          'content_moderation_links' => FALSE,
          'test_module_1' => FALSE,
        ],
        'not_entity_type_alter',
        [
          'content_moderation_links' => FALSE,
          'test_module_1' => FALSE,
        ],
      ],
      'without-implementation' => [
        [
          'test_module_1' => FALSE,
        ],
        'entity_type_alter',
        [
          'test_module_1' => FALSE,
        ],
      ],
    ];
  }

  /**
   * Tests altering the registry of modules implementing a hook.
   *
   * @param array $implementations
   *   An array keyed by the module's name. The value of each item corresponds
   *   to a $group, which is usually FALSE.
   * @param string $hook
   *   The name of the module hook being implemented.
   * @param array $expected
   *   The expected value.
   *
   * @covers ::moduleImplementsAlter
   *
   * @dataProvider dataProviderModuleImplementsAlter
   */
  public function testModuleImplementsAlter(array $implementations, string $hook, array $expected): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $class->moduleImplementsAlter($implementations, $hook);

    $this->assertSame($expected, $implementations);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\content_moderation_links\Hook\ModuleImplementsAlterHook|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModuleImplementsAlterHook|MockObject {
    return $this->getMockBuilder(ModuleImplementsAlterHook::class)
      ->disableOriginalConstructor()
      ->onlyMethods($only_methods)
      ->getMock();
  }

}
