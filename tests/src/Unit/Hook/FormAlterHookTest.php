<?php

namespace Drupal\Tests\content_moderation_links\Unit\Hook;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\Hook\FormAlterHook;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\entity_usage\EntityUsageInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the 'hook_form_alter' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\FormAlterHook
 * @group content_moderation_links
 */
class FormAlterHookTest extends UnitTestCase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The entity usage statistics.
   *
   * @var \Drupal\entity_usage\EntityUsageInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityUsage;

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create/register string translation mock.
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    // Initialize config factory mock.
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);

    // Initialize entity usage statistics mock.
    $this->entityUsage = $this->createMock(EntityUsageInterface::class);

    // Initialize URL generator mock.
    $this->urlGenerator = $this->createMock(UrlGeneratorInterface::class);
  }

  /**
   * Data provider for checking if entity usage data exists.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderEntityUsageDataExists(): array {
    return [
      'data' => [
        TRUE,
        TRUE,
      ],
      'no-data' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests if entity usage data exists.
   *
   * @param bool $has_data
   *   Whether entity usage data is available.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::entityUsageDataExists
   *
   * @dataProvider dataProviderEntityUsageDataExists
   */
  public function testEntityUsageDataExists(bool $has_data, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    // Prepare entity usage statistics mock.
    $this->entityUsage->expects($this->once())
      ->method('listSources')
      ->with($entity)
      ->willReturn($has_data ? ['test data'] : []);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected, $class->entityUsageDataExists($entity));
  }

  /**
   * Data provider for checking if entity usage notice is enabled.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderEntityUsageNoticeIsEnabledForEntity(): array {
    return [
      'enabled' => [
        TRUE,
        TRUE,
      ],
      'disabled' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests if entity usage notice is enabled.
   *
   * @param bool $enabled
   *   Whether it is enabled.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::entityUsageNoticeIsEnabledForEntity
   *
   * @dataProvider dataProviderEntityUsageNoticeIsEnabledForEntity
   */
  public function testEntityUsageNoticeIsEnabledForEntity(bool $enabled, bool $expected): void {
    $entity_type_id = 'test_entity_type';

    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    $entity->expects($this->once())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    // Prepare config mock.
    $config = $this->createMock(ImmutableConfig::class);

    $config->expects($this->once())
      ->method('get')
      ->with('edit_warning_message_entity_types')
      ->willReturn($enabled ? [$entity_type_id] : NULL);

    // Prepare config factory mock.
    $this->configFactory->expects($this->once())
      ->method('get')
      ->with('entity_usage.settings')
      ->willReturn($config);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected, $class->entityUsageNoticeIsEnabledForEntity($entity));
  }

  /**
   * Data provider for performing alterations before a form is rendered.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderFormAlter(): array {
    return [
      'entity' => [
        TRUE,
      ],
      'no-entity' => [
        FALSE,
      ],
    ];
  }

  /**
   * Tests performing alterations before a form is rendered.
   *
   * @param bool $has_entity
   *   Whether the form has an associated entity.
   *
   * @covers ::formAlter
   *
   * @dataProvider dataProviderFormAlter
   */
  public function testFormAlter(bool $has_entity): void {
    $entity_id = 123;
    $entity_type_id = 'test_entity_type';
    $entity_usage_url = '/test/entity/usage/url';
    $form_id = 'test_form';

    $expected = $has_entity ? [
      'entity_usage_edit_warning' => [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            'Modifications on this form will affect all <a href="' . $entity_usage_url . '" target="_blank">existing usages</a> of this entity.',
          ],
        ],
        '#status_headings' => [
          'warning' => 'Warning message',
        ],
        '#weight' => -201,
      ],
    ] : [];

    // Prepare form mock.
    $form = [];

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    $entity->expects($this->atMost(1))
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $entity->expects($this->atMost(1))
      ->method('id')
      ->willReturn($entity_id);

    // Prepare URL generator mock.
    $this->urlGenerator->expects($this->atMost(1))
      ->method('generateFromRoute')
      ->with('entity_usage.usage_list', [
        'entity_type' => $entity_type_id,
        'entity_id' => $entity_id,
      ])
      ->willReturn($entity_usage_url);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'shouldHaveEntityUsageNotice',
    ]);

    $class->expects($this->atMost(1))
      ->method('getEntity')
      ->with($form_state)
      ->willReturn($has_entity ? $entity : NULL);

    $class->expects($this->atMost(1))
      ->method('shouldHaveEntityUsageNotice')
      ->with($form_state)
      ->willReturn(TRUE);

    $class->formAlter($form, $form_state, $form_id);
    $this->assertEquals($expected, $form);
  }

  /**
   * Data provider for returning the entity the form is for (if any).
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetEntity(): array {
    return [
      'no-entity-form' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'no-entity' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'entity' => [
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests returning the entity the form is for (if any).
   *
   * @param bool $is_entity_form
   *   Whether the form is an entity form.
   * @param bool $has_entity
   *   Whether the form has an entity.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getEntity
   *
   * @dataProvider dataProviderGetEntity
   */
  public function testGetEntity(bool $is_entity_form, bool $has_entity, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    // Initialize form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    // Prepare entity form object mock.
    $entity_form = $this->createMock(EntityFormInterface::class);

    $entity_form->expects($is_entity_form ? $this->once() : $this->never())
      ->method('getEntity')
      ->willReturn($has_entity ? $entity : NULL);

    // Prepare form state mock.
    $form_state->expects($this->once())
      ->method('getFormObject')
      ->willReturn($is_entity_form ? $entity_form : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $entity : NULL, $class->getEntity($form_state));
  }

  /**
   * Data provider for checking, if form should have the entity usage notice.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderShouldHaveEntityUsageNotice(): array {
    return [
      'discard-latest-version' => [
        TRUE,
        TRUE,
        TRUE,
        LinkTemplate::DISCARD_LATEST_VERSION,
        TRUE,
      ],
      'moderation-state-transition' => [
        TRUE,
        TRUE,
        TRUE,
        LinkTemplate::MODERATION_STATE_TRANSITION,
        TRUE,
      ],
      'unknown-operation' => [
        TRUE,
        TRUE,
        TRUE,
        'unknown',
        FALSE,
      ],
      'no-entity' => [
        FALSE,
        TRUE,
        TRUE,
        LinkTemplate::DISCARD_LATEST_VERSION,
        FALSE,
      ],
      'entity-usage-notice-not-enabled' => [
        TRUE,
        FALSE,
        TRUE,
        LinkTemplate::DISCARD_LATEST_VERSION,
        FALSE,
      ],
      'no-entity-usage-data' => [
        TRUE,
        TRUE,
        FALSE,
        LinkTemplate::DISCARD_LATEST_VERSION,
        FALSE,
      ],
    ];
  }

  /**
   * Tests if form should have the entity usage notice.
   *
   * @param bool $has_entity
   *   Whether the form has an entity.
   * @param bool $entity_usage_notice_is_enabled_for_entity_type
   *   Whether the entity usage notice is enabled for the entity type.
   * @param bool $has_entity_usage_data
   *   Whether entity usage data is available.
   * @param string $operation
   *   The form operation.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::shouldHaveEntityUsageNotice
   *
   * @dataProvider dataProviderShouldHaveEntityUsageNotice
   */
  public function testShouldHaveEntityUsageNotice(bool $has_entity, bool $entity_usage_notice_is_enabled_for_entity_type, bool $has_entity_usage_data, string $operation, bool $expected): void {
    // Prepare form object mock.
    $form_object = $this->createMock(EntityFormInterface::class);

    $form_object->expects($this->atMost(1))
      ->method('getOperation')
      ->willReturn($operation);

    // Prepare form state mock.
    $form_state = $this->createMock(FormStateInterface::class);

    $form_state->expects($this->once())
      ->method('getFormObject')
      ->willReturn($form_object);

    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    // Prepare class mock.
    $class = $this->createClassMock([
      'entityUsageDataExists',
      'entityUsageNoticeIsEnabledForEntity',
      'getEntity',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->with($form_state)
      ->willReturn($has_entity ? $entity : NULL);

    $class->expects($this->atMost(1))
      ->method('entityUsageNoticeIsEnabledForEntity')
      ->with($entity)
      ->willReturn($entity_usage_notice_is_enabled_for_entity_type);

    $class->expects($this->atMost(1))
      ->method('entityUsageDataExists')
      ->with($entity)
      ->willReturn($has_entity_usage_data);

    $this->assertEquals($expected, $class->shouldHaveEntityUsageNotice($form_state));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Hook\FormAlterHook_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): FormAlterHook_Test|MockObject {
    return $this->getMockBuilder(FormAlterHook_Test::class)
      ->setConstructorArgs([
        $this->configFactory,
        $this->entityUsage,
        $this->urlGenerator,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked form alter hook class for tests.
 */
class FormAlterHook_Test extends FormAlterHook {

  /**
   * {@inheritdoc}
   */
  public function entityUsageDataExists(EntityInterface $entity): bool {
    return parent::entityUsageDataExists($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function entityUsageNoticeIsEnabledForEntity(EntityInterface $entity): bool {
    return parent::entityUsageNoticeIsEnabledForEntity($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(FormStateInterface $form_state): ?EntityInterface {
    return parent::getEntity($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function shouldHaveEntityUsageNotice(FormStateInterface $form_state): bool {
    return parent::shouldHaveEntityUsageNotice($form_state);
  }

}
// @codingStandardsIgnoreEnd
