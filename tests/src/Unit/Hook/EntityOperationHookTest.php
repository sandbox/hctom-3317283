<?php

namespace Drupal\Tests\content_moderation_links\Unit\Hook;

use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\Hook\EntityOperationHook;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\Tests\UnitTestCase;
use Drupal\workflows\TransitionInterface;
use Drupal\workflows\WorkflowInterface;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the 'hook_entity_operation' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\EntityOperationHook
 * @group content_moderation_links
 */
class EntityOperationHookTest extends UnitTestCase {

  /**
   * The moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderationInformation;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create/register string translation mock.
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    // Initialize moderation information mock.
    $this->moderationInformation = $this->createMock(ModerationInformationInterface::class);
  }

  /**
   * Data provider for adding 'Discard latest version' operation.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAddDiscardLatestVersionOperation(): array {
    return [
      'allowed' => [
        TRUE,
        TRUE,
      ],
      'not-allowed' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Test adding 'Discard latest version' operation.
   *
   * @param bool $access
   *   Whether access is granted.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::addDiscardLatestVersionOperation
   *
   * @dataProvider dataProviderAddDiscardLatestVersionOperation
   */
  public function testAddDiscardLatestVersionOperation(bool $access, bool $expected): void {
    $operations = [];
    $key = 'content_moderation_links.discard_latest_version';

    // Prepare URL mock.
    $url = $this->createMock(Url::class);

    $url->expects($this->once())
      ->method('access')
      ->willReturn($access);

    // Prepare class mock.
    $class = $this->createClassMock();

    $class->addDiscardLatestVersionOperation($operations, $url);

    if ($expected) {
      $this->assertArrayHasKey($key, $operations);
      $this->assertEquals([
        'title' => 'Discard latest version',
        'url' => $url,
        'weight' => EntityOperationHook::WEIGHT_OFFSET,
      ], $operations[$key]);
    }
    else {
      $this->assertArrayNotHasKey($key, $operations);
    }
  }

  /**
   * Data provider for adding 'Moderation state transition' operation(s).
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAddModerationStateTransitionOperations(): array {
    return [
      'allowed' => [
        TRUE,
        TRUE,
      ],
      'not-allowed' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests adding 'Moderation state transition' operation(s).
   *
   * @param bool $access
   *   Whether access is granted.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::addModerationStateTransitionOperations
   *
   * @dataProvider dataProviderAddModerationStateTransitionOperations
   */
  public function testAddModerationStateTransitionOperations(bool $access, bool $expected): void {
    $operations = [];
    $state_from_id = 'test_state_from';
    $state_to_id = 'test_state_to';
    $transition_label = 'test transition label';
    $transition_id = 'test_transition';
    $transition_weight = 123;
    $key = 'content_moderation_links.moderation_state_transition.' . $transition_id;

    // Prepare from state mock.
    $state_from = $this->createMock(ContentModerationState::class);

    $state_from->expects($this->once())
      ->method('id')
      ->willReturn($state_from_id);

    // Prepare to state mock.
    $state_to = $this->createMock(ContentModerationState::class);

    $state_to->expects($this->once())
      ->method('id')
      ->willReturn($state_to_id);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($this->once())
      ->method('from')
      ->willReturn([$state_from]);

    $transition->expects($this->once())
      ->method('to')
      ->willReturn($state_to);

    $transition->expects($this->once())
      ->method('id')
      ->willReturn($transition_id);

    $transition->expects($this->once())
      ->method('label')
      ->willReturn($transition_label);

    $transition->expects($this->once())
      ->method('weight')
      ->willReturn($transition_weight);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getTransitions',
    ]);

    // Prepare URL mock.
    $url = $this->createMock(Url::class);

    $url->expects($this->exactly(2))
      ->method('setRouteParameter')
      // @todo Implement better solution for consecutive calls.
      ->willReturnCallback(fn(string $key, string $value): bool => match (TRUE) {
        $key === 'from' && $value === $state_from_id => TRUE,
        $key === 'to' && $value === $state_to_id => TRUE,
        default => throw new \LogicException('Parameters do not match expected values.'),
      });

    $url->expects($this->once())
      ->method('access')
      ->willReturn($access);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects($this->once())
      ->method('toUrl')
      ->with(LinkTemplate::MODERATION_STATE_TRANSITION)
      ->willReturn($url);

    $class->expects($this->once())
      ->method('getTransitions')
      ->with($entity)
      ->willReturn([$transition]);

    $class->addModerationStateTransitionOperations($operations, $entity);

    if ($expected) {
      $this->assertArrayHasKey($key, $operations);
      $this->assertEquals([
        'title' => $transition_label,
        'url' => $url,
        'weight' => EntityOperationHook::WEIGHT_OFFSET + $transition_weight,
      ], $operations[$key]);
    }
    else {
      $this->assertArrayNotHasKey($key, $operations);
    }
  }

  /**
   * Tests the class constants.
   */
  public function testConstants(): void {
    $this->assertEquals(200, EntityOperationHook::WEIGHT_OFFSET);
  }

  /**
   * Data provider for declaring entity operations.
   *
   * @return array
   *   The test data.
   */
  public function dataProvidernEntityOperation(): array {
    return [
      'no-content-entity' => [
        FALSE,
        TRUE,
      ],
      'no-moderated-entity' => [
        TRUE,
        FALSE,
      ],
      'moderated-entity' => [
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests declaring entity operations.
   *
   * @param bool $is_content_entity
   *   Whether it is a content entity.
   * @param bool $is_moderated_entity
   *   Whether it is a moderated entity.
   *
   * @covers ::entityOperation
   *
   * @dataProvider dataProvidernEntityOperation
   */
  public function testEntityOperation(bool $is_content_entity, bool $is_moderated_entity): void {
    $operations = [];

    // Prepare URL mock.
    $url = $this->createMock(Url::class);

    // Prepare entity mock.
    $entity = $is_content_entity ? $this->createMock(ContentEntityInterface::class) : $this->createMock(EntityInterface::class);

    $entity->expects($is_content_entity && $is_moderated_entity ? $this->once() : $this->never())
      ->method('toUrl')
      ->with(LinkTemplate::DISCARD_LATEST_VERSION)
      ->willReturn($url);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($is_content_entity ? $this->once() : $this->never())
      ->method('isModeratedEntity')
      ->willReturn($is_moderated_entity);

    // Prepare class mock.
    $class = $this->createClassMock([
      'addDiscardLatestVersionOperation',
      'addModerationStateTransitionOperations',
    ]);

    $class->expects($is_content_entity && $is_moderated_entity ? $this->once() : $this->never())
      ->method('addDiscardLatestVersionOperation')
      ->with($operations, $url);

    $class->expects($is_content_entity && $is_moderated_entity ? $this->once() : $this->never())
      ->method('addModerationStateTransitionOperations')
      ->with($operations, $entity);

    $this->assertEquals($operations, $class->entityOperation($entity));
  }

  /**
   * Data provider for returning available workflow transitions.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetTransitions(): array {
    return [
      'no-workflow' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'no-workflow-type' => [
        TRUE,
        FALSE,
        FALSE,
      ],
      'transitions' => [
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests returning available workflow transitions.
   *
   * @param bool $has_workflow
   *   Whether a related workflow exists.
   * @param bool $has_workflow_type
   *   Whether a related workflow type exists.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getTransitions
   *
   * @dataProvider dataProviderGetTransitions
   */
  public function testGetTransitions(bool $has_workflow, bool $has_workflow_type, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare transition list mock.
    $transitions = [
      $this->createMock(TransitionInterface::class),
      $this->createMock(TransitionInterface::class),
    ];

    // Prepare workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    $workflow_type->expects($has_workflow && $has_workflow_type ? $this->once() : $this->never())
      ->method('getTransitions')
      ->with(NULL)
      ->willReturn($transitions);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($has_workflow ? $this->once() : $this->never())
      ->method('getTypePlugin')
      ->willReturn($has_workflow_type ? $workflow_type : NULL);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('getWorkflowForEntity')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $transitions : [], $class->getTransitions($entity));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Hook\EntityOperationHook_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): EntityOperationHook_Test|MockObject {
    return $this->getMockBuilder(EntityOperationHook_Test::class)
      ->setConstructorArgs([
        $this->moderationInformation,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked entity operation hook class for tests.
 */
class EntityOperationHook_Test extends EntityOperationHook {

  /**
   * {@inheritdoc}
   */
  public function addDiscardLatestVersionOperation(array &$operations, Url $url): void {
    parent::addDiscardLatestVersionOperation($operations, $url);
  }

  /**
   * {@inheritdoc}
   */
  public function addModerationStateTransitionOperations(array &$operations, ContentEntityInterface $entity): void {
    parent::addModerationStateTransitionOperations($operations, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitions(ContentEntityInterface $entity): array {
    return parent::getTransitions($entity);
  }

}
// @codingStandardsIgnoreEnd
