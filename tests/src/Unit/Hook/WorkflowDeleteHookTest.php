<?php

namespace Drupal\Tests\content_moderation_links\Unit\Hook;

use Drupal\content_moderation_links\Hook\WorkflowDeleteHook;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the 'hook_ENTITY_TYPE_delete' hook implementation class.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Hook\WorkflowDeleteHook
 * @group content_moderation_links
 */
class WorkflowDeleteHookTest extends UnitTestCase {

  /**
   * Tests response to workflow entity deletion.
   *
   * @covers ::workflowDelete
   */
  public function testWorkflowDelete(): void {
    // Prepare entity mock.
    $entity = $this->createMock(EntityInterface::class);

    // Prepare cache tags invalidator mock.
    $cache_tags_invalidator = $this->createMock(CacheTagsInvalidatorInterface::class);

    $cache_tags_invalidator->expects($this->once())
      ->method('invalidateTags')
      ->with([
        'local_action',
      ]);

    $this->getContainerWithCacheTagsInvalidator($cache_tags_invalidator);

    // Prepare class mock.
    $class = $this->createClassMock();

    $class->workflowDelete($entity);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\content_moderation_links\Hook\WorkflowDeleteHook|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): WorkflowDeleteHook|MockObject {
    return $this->getMockBuilder(WorkflowDeleteHook::class)
      ->disableOriginalConstructor()
      ->onlyMethods($only_methods)
      ->getMock();
  }

}
