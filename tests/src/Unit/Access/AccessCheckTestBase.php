<?php

namespace Drupal\Tests\content_moderation_links\Unit\Access;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_links\ModeratedEntityHelperInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\DependencyInjection\Container;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Provides a base class for content moderation link access unit test classes.
 */
abstract class AccessCheckTestBase extends UnitTestCase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $currentRouteMatch;

  /**
   * The moderated entity helper.
   *
   * @var \Drupal\content_moderation_links\ModeratedEntityHelperInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderatedEntityHelper;

  /**
   * The moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderationInformation;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create/register cache context manager mock.
    $cache_contexts_manager = $this->createMock(CacheContextsManager::class);
    $cache_contexts_manager->expects($this->any())
      ->method('assertValidTokens')
      ->willReturn(TRUE);
    $container = new Container();
    $container->set('cache_contexts_manager', $cache_contexts_manager);
    \Drupal::setContainer($container);

    // Initialize current route match mock.
    $this->currentRouteMatch = $this->createMock(RouteMatchInterface::class);

    // Initialize moderated entity helper mock.
    $this->moderatedEntityHelper = $this->createMock(ModeratedEntityHelperInterface::class);

    // Initialize moderation information mock.
    $this->moderationInformation = $this->createMock(ModerationInformationInterface::class);
  }

}
