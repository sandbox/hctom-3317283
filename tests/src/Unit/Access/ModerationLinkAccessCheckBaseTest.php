<?php

namespace Drupal\Tests\content_moderation_links\Unit\Access;

use Drupal\content_moderation_links\Access\ModerationLinkAccessCheckBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\workflows\WorkflowInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Routing\Route;

/**
 * Tests the base class for content moderation link access checks.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Access\ModerationLinkAccessCheckBase
 * @group content_moderation_links
 */
class ModerationLinkAccessCheckBaseTest extends AccessCheckTestBase {

  /**
   * Data provider for checking content moderation link access.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAccess(): array {
    return [
      'no-entity' => [
        FALSE,
      ],
      'entity' => [
        TRUE,
      ],
    ];
  }

  /**
   * Tests checking content moderation link access.
   *
   * @param bool $has_entity
   *   Whether a content entity exists.
   *
   * @covers ::access
   * @covers ::checkAccess
   *
   * @dataProvider dataProviderAccess
   */
  public function testAccess(bool $has_entity): void {
    // Prepare route mock.
    $route = $this->createMock(Route::class);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    // Prepare account mock.
    $account = $this->createMock(AccountInterface::class);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare access result mock.
    $access_result = $this->createMock(AccessResult::class);

    $access_result->expects($has_entity ? $this->exactly(2) : $this->never())
      ->method('andIf')
      ->with($access_result)
      ->willReturn($access_result);

    $access_result->expects($has_entity ? $this->once() : $this->never())
      ->method('addCacheableDependency')
      ->with($entity)
      ->willReturn($access_result);

    // Prepare class mock.
    $class = $this->createClassMock([
      'allowedIfEntityCanonicalRoute',
      'allowedIfEntityUpdateAllowed',
      'allowedIfModeratedEntity',
      'getEntity',
      'mergeWorkflow',
    ]);

    $class->expects($this->once())
      ->method('getEntity')
      ->willReturn($has_entity ? $entity : NULL);

    $class->expects($has_entity ? $this->once() : $this->never())
      ->method('allowedIfModeratedEntity')
      ->with($entity)
      ->willReturn($access_result);

    $class->expects($has_entity ? $this->once() : $this->never())
      ->method('allowedIfEntityUpdateAllowed')
      ->with($entity, $account)
      ->willReturn($access_result);

    $class->expects($has_entity ? $this->once() : $this->never())
      ->method('allowedIfEntityCanonicalRoute')
      ->with($entity)
      ->willReturn($access_result);

    $class->expects($has_entity ? $this->once() : $this->never())
      ->method('mergeWorkflow')
      ->willReturn($access_result);

    $actual = $class->access($route, $route_match, $account);

    $this->assertInstanceOf(AccessResultInterface::class, $actual);

    if (!$has_entity) {
      $this->assertInstanceOf(AccessResultReasonInterface::class, $actual);
      $this->assertEquals('Not a content entity.', $actual->getReason());
    }
  }

  /**
   * Data provider for checking, if on entity's canonical route.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfEntityCanonicalRoute(): array {
    return [
      'canonical-not-allowed' => [
        TRUE,
        FALSE,
        FALSE,
      ],
      'not-canonical' => [
        FALSE,
        TRUE,
        TRUE,
      ],
      'canonical' => [
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests access result, if on entity's canonical route.
   *
   * @param bool $is_canonical_route
   *   Whether it is the entity's canonical route.
   * @param bool $is_allowed_on_canonical_route
   *   Whether it is allowed on entity's canonical route.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfEntityCanonicalRoute
   *
   * @dataProvider dataProviderAllowedIfEntityCanonicalRoute
   */
  public function testAllowedIfEntityCanonicalRoute(bool $is_canonical_route, bool $is_allowed_on_canonical_route, bool $expected): void {
    $route_name_canonical = 'canonical';
    $route_name_current = $is_canonical_route ? $route_name_canonical : 'not_canonical';

    // Prepare entity URL mock.
    $entity_url = $this->createMock(Url::class);

    $entity_url->expects($this->atMost(1))
      ->method('getRouteName')
      ->willReturn($route_name_canonical);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare current route match mock.
    $this->currentRouteMatch->expects($this->atMost(1))
      ->method('getRouteName')
      ->willReturn($route_name_current);

    $entity->expects($this->atMost(1))
      ->method('toUrl')
      ->with('canonical')
      ->willReturn($entity_url);

    // Prepare class mock.
    $class = $this->createClassMock([
      'isAllowedOnCanonicalRoute',
    ]);

    $class->expects($this->atMost(1))
      ->method('isAllowedOnCanonicalRoute')
      ->with($entity)
      ->willReturn($is_allowed_on_canonical_route);

    $access_result = $class->allowedIfEntityCanonicalRoute($entity);

    $this->assertTrue($expected ? $access_result->isAllowed() : $access_result->isNeutral());

    if ($access_result instanceof RefinableCacheableDependencyInterface) {
      $this->assertEquals($is_canonical_route, in_array('route.name', $access_result->getCacheContexts(), TRUE));
    }
  }

  /**
   * Data provider for checking, if entity update is allowed.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfEntityUpdateAllowed(): array {
    return [
      'allowed' => [
        TRUE,
        TRUE,
      ],
      'not-allowed' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests access result, if entity update is allowed.
   *
   * @param bool $entity_update_allowed
   *   Whether entity updates are allowed.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfEntityUpdateAllowed
   *
   * @dataProvider dataProviderAllowedIfEntityUpdateAllowed
   */
  public function testAllowedIfEntityUpdateAllowed(bool $entity_update_allowed, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare account mock.
    $account = $this->createMock(AccountInterface::class);

    $entity->expects($this->atMost(1))
      ->method('access')
      ->with('update', $account, TRUE)
      ->willReturn($entity_update_allowed ? AccessResult::allowed() : AccessResult::neutral());

    // Prepare class mock.
    $class = $this->createClassMock();

    $access_result = $class->allowedIfEntityUpdateAllowed($entity, $account);

    $this->assertTrue($expected ? $access_result->isAllowed() : $access_result->isNeutral());
  }

  /**
   * Data provider for checking, if entity is moderated.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfModeratedEntity(): array {
    return [
      'moderated-entity' => [
        TRUE,
        TRUE,
      ],
      'entity' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests access result, if entity is moderated.
   *
   * @param bool $is_moderated_entity
   *   Whether it is a moderated entity.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfModeratedEntity
   *
   * @dataProvider dataProviderAllowedIfModeratedEntity
   */
  public function testAllowedIfModeratedEntity(bool $is_moderated_entity, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->atMost(1))
      ->method('isModeratedEntity')
      ->with($entity)
      ->willReturn($is_moderated_entity);

    // Prepare class mock.
    $class = $this->createClassMock();

    $access_result = $class->allowedIfModeratedEntity($entity);
    $this->assertTrue($expected ? $access_result->isAllowed() : $access_result->isNeutral());
  }

  /**
   * Data provider for getting the content entity object from route parameters.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetEntity(): array {
    return [
      'not-has-route-option' => [
        NULL,
        TRUE,
        FALSE,
      ],
      'not-has-entity' => [
        'test',
        FALSE,
        FALSE,
      ],
      'entity' => [
        'test',
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests getting the content entity object from route parameters.
   *
   * @covers ::getEntity
   *
   * @dataProvider dataProviderGetEntity
   */
  public function testGetEntity(?string $entity_type_id, bool $has_entity, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare route mock.
    $route = $this->createMock(Route::class);

    $route->expects($this->once())
      ->method('getOption')
      ->with('_content_moderation_links_entity_type')
      ->willReturn($entity_type_id);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    $route_match->expects($this->atMost(1))
      ->method('getParameter')
      ->with($entity_type_id)
      ->willReturn($has_entity ? $entity : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $entity : NULL, $class->getEntity($route, $route_match));
  }

  /**
   * Data provider for merging associated workflow metadata.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderMergeWorkflow(): array {
    return [
      'workflow' => [
        TRUE,
      ],
      'no-workflow' => [
        FALSE,
      ],
    ];
  }

  /**
   * Tests merging associated workflow metadata.
   *
   * @param bool $has_workflow
   *   Whether a related workflow exists.
   *
   * @covers ::mergeWorkflow
   *
   * @dataProvider dataProviderMergeWorkflow
   */
  public function testMergeWorkflow(bool $has_workflow): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('getWorkflowForEntity')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow : NULL);

    // Prepare access result mock.
    $access_result = $this->createMock(AccessResultAllowed::class);

    $access_result->expects($has_workflow ? $this->once() : $this->never())
      ->method('addCacheableDependency')
      ->with($workflow)
      ->willReturn($access_result);

    // Prepare class mock.
    $class = $this->createClassMock();

    $class->mergeWorkflow($access_result, $entity);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Access\ModerationLinkAccessCheckBase_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModerationLinkAccessCheckBase_Test|MockObject {
    return $this->getMockBuilder(ModerationLinkAccessCheckBase_Test::class)
      ->setConstructorArgs([
        $this->moderatedEntityHelper,
        $this->moderationInformation,
        $this->currentRouteMatch,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked moderation link access check base class for tests.
 */
class ModerationLinkAccessCheckBase_Test extends ModerationLinkAccessCheckBase {

  /**
   * {@inheritdoc}
   */
  public function allowedIfEntityCanonicalRoute(ContentEntityInterface $entity): AccessResultInterface {
    return parent::allowedIfEntityCanonicalRoute($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedIfEntityUpdateAllowed(ContentEntityInterface $entity, AccountInterface $account): AccessResultInterface {
    return parent::allowedIfEntityUpdateAllowed($entity, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedIfModeratedEntity(ContentEntityInterface $entity): AccessResultInterface {
    return parent::allowedIfModeratedEntity($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(Route $route, RouteMatchInterface $route_match): ?ContentEntityInterface {
    return parent::getEntity($route, $route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function isAllowedOnCanonicalRoute(ContentEntityInterface $entity): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeWorkflow(AccessResultInterface $access_result, ContentEntityInterface $entity): AccessResultInterface {
    return parent::mergeWorkflow($access_result, $entity);
  }

}
// @codingStandardsIgnoreEnd
