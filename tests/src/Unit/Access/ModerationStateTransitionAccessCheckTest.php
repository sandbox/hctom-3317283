<?php

namespace Drupal\Tests\content_moderation_links\Unit\Access;

use Drupal\content_moderation\ContentModerationState;
use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\Access\ModerationStateTransitionAccessCheck;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\workflows\StateInterface;
use Drupal\workflows\TransitionInterface;
use Drupal\workflows\WorkflowInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Routing\Route;

/**
 * Tests access check for content moderation state transitions.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Access\ModerationStateTransitionAccessCheck
 * @group content_moderation_links
 */
class ModerationStateTransitionAccessCheckTest extends AccessCheckTestBase {

  /**
   * Data provider for checking content moderation link access.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAccess(): array {
    return [
      'parent-not-allowed' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'invalid-state-data' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'cannot-transition' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-allowed' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
      'allowed' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests checking content moderation link access.
   *
   * @param bool $parent_is_allowed
   *   Whether the parent class grants access.
   * @param bool $state_data_is_valid
   *   Whether the content moderation state data is valid.
   * @param bool $can_transition
   *   Whether the transition can be performed.
   * @param bool $allowed
   *   Whether the transition is allowed.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::access
   *
   * @dataProvider dataProviderAccess
   */
  public function testAccess(bool $parent_is_allowed, bool $state_data_is_valid, bool $can_transition, bool $allowed, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare route mock.
    $route = $this->createMock(Route::class);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    // Prepare account mock.
    $account = $this->createMock(AccountInterface::class);

    // Prepare class mock.
    $class = $this->createClassMock([
      'allowedIfCanTransition',
      'allowedIfStateDataIsValid',
      'allowedIfTransitionAllowed',
      'getEntity',
      'checkAccess',
    ]);

    $class->expects($this->once())
      ->method('checkAccess')
      ->with($route, $route_match, $account)
      ->willReturn($parent_is_allowed ? AccessResult::allowed() : AccessResult::neutral());

    $class->expects($parent_is_allowed ? $this->once() : $this->never())
      ->method('getEntity')
      ->with($route, $route_match)
      ->willReturn($entity);

    $class->expects($parent_is_allowed ? $this->once() : $this->never())
      ->method('allowedIfStateDataIsValid')
      ->with($entity, $route_match)
      ->willReturn($state_data_is_valid ? AccessResult::allowed() : AccessResult::neutral());

    $class->expects($parent_is_allowed && $state_data_is_valid ? $this->once() : $this->never())
      ->method('allowedIfCanTransition')
      ->with($entity, $route_match)
      ->willReturn($can_transition ? AccessResult::allowed() : AccessResult::neutral());

    $class->expects($parent_is_allowed && $state_data_is_valid && $can_transition ? $this->once() : $this->never())
      ->method('allowedIfTransitionAllowed')
      ->with($entity, $route_match, $account)
      ->willReturn($allowed ? AccessResult::allowed() : AccessResult::neutral());

    $actual = $class->access($route, $route_match, $account);

    $this->assertTrue($expected ? $actual->isAllowed() : $actual->isNeutral());
  }

  /**
   * Data provider for checking given moderation may transition.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfCanTransition(): array {
    return [
      'state-current-is-not-state-from' => [
        'test_state_current',
        'test_state_from',
        'test_state_to',
        TRUE,
        FALSE,
      ],
      'state-current-is-state-to' => [
        'test_state',
        'test_state',
        'test_state',
        TRUE,
        FALSE,
      ],
      'cannot-transition' => [
        'test_state_from',
        'test_state_from',
        'test_state_to',
        FALSE,
        FALSE,
      ],
      'can-transition' => [
        'test_state_from',
        'test_state_from',
        'test_state_to',
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests access result, if given moderation states are allowed to transition.
   *
   * @param string $state_current_id
   *   The ID of the current content moderation state.
   * @param string $state_from_id
   *   The ID of the content moderation state to transition from.
   * @param string $state_to_id
   *   The ID of the content moderation state to transition to.
   * @param bool $can_transition
   *   Whether the transition can be performed.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfCanTransition
   *
   * @dataProvider dataProviderAllowedIfCanTransition
   */
  public function testAllowedIfCanTransition(string $state_current_id, string $state_from_id, string $state_to_id, bool $can_transition, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    // Prepare current moderation state mock.
    $state_current = $this->createMock(StateInterface::class);

    $state_current->expects($this->atMost(2))
      ->method('id')
      ->willReturn($state_current_id);

    $state_current->expects($this->atMost(2))
      ->method('canTransitionTo')
      ->with($state_to_id)
      ->willReturn($can_transition);

    // Prepare moderation state to transition from mock.
    $state_from = $this->createMock(StateInterface::class);

    $state_from->expects($this->once())
      ->method('id')
      ->willReturn($state_from_id);

    // Prepare moderation state to transition to mock.
    $state_to = $this->createMock(StateInterface::class);

    $state_to->expects($this->any())
      ->method('id')
      ->willReturn($state_to_id);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getStateCurrent',
      'getStateFrom',
      'getStateTo',
    ]);

    $class->expects($this->once())
      ->method('getStateCurrent')
      ->with($entity)
      ->willReturn($state_current);

    $class->expects($this->once())
      ->method('getStateFrom')
      ->with($route_match)
      ->willReturn($state_from);

    $class->expects($this->once())
      ->method('getStateTo')
      ->with($route_match)
      ->willReturn($state_to);

    $actual = $class->allowedIfCanTransition($entity, $route_match);

    $this->assertTrue($expected ? $actual->isAllowed() : $actual->isNeutral());
  }

  /**
   * Data provider for checking, if moderation state data is available/valid.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfStateDataIsValid(): array {
    return [
      'no-workflow-type' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-state-current' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-state-from' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-state-to' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'state-from-not-exists' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'state-to-not-exists' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
      'valid' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests access result, if moderation state data is available and valid.
   *
   * @param bool $has_workflow_type
   *   Whether an associated workflow type is available.
   * @param bool $has_state_current
   *   Whether a current content moderation state is available.
   * @param bool $has_state_from
   *   Whether a content moderation state to transition from is available.
   * @param bool $has_state_to
   *   Whether a content moderation state to transition to is available.
   * @param bool $state_from_exists
   *   Whether the content moderation state from transition to exists.
   * @param bool $state_to_exists
   *   Whether the content moderation state to transition to exists.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfStateDataIsValid
   *
   * @dataProvider dataProviderAllowedIfStateDataIsValid
   */
  public function testAllowedIfStateDataIsValid(bool $has_workflow_type, bool $has_state_current, bool $has_state_from, bool $has_state_to, bool $state_from_exists, bool $state_to_exists, bool $expected): void {
    $has_all_required_data = $has_workflow_type && $has_state_current && $has_state_from && $has_state_to;
    $state_from_id = 'test_state_from';
    $state_to_id = 'test_state_to';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    $workflow_type->expects($has_all_required_data ? $this->exactly($state_from_exists ? 2 : 1) : $this->never())
      ->method('hasState')
      // @todo Implement better solution for consecutive calls.
      ->willReturnCallback(fn(string $state_id): bool => match ($state_id) {
        $state_from_id => $state_from_exists,
        $state_to_id  => $state_to_exists,
        default => throw new \LogicException('Parameters do not match expected values.'),
      });

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($this->once())
      ->method('getWorkflowType')
      ->with($entity)
      ->willReturn($has_workflow_type ? $workflow_type : NULL);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    // Prepare current moderation state mock.
    $state_current = $this->createMock(StateInterface::class);

    // Prepare moderation state to transition from mock.
    $state_from = $this->createMock(StateInterface::class);

    $state_from->expects($has_all_required_data ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($state_from_id);

    // Prepare moderation state to transition to mock.
    $state_to = $this->createMock(StateInterface::class);

    $state_to->expects($has_all_required_data && $state_from_exists ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($state_to_id);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getStateCurrent',
      'getStateFrom',
      'getStateTo',
    ]);

    $class->expects($has_workflow_type ? $this->once() : $this->never())
      ->method('getStateCurrent')
      ->with($entity)
      ->willReturn($has_state_current ? $state_current : NULL);

    $class->expects($has_workflow_type ? $this->once() : $this->never())
      ->method('getStateFrom')
      ->with($route_match)
      ->willReturn($has_state_from ? $state_from : NULL);

    $class->expects($has_workflow_type ? $this->once() : $this->never())
      ->method('getStateTo')
      ->with($route_match)
      ->willReturn($has_state_to ? $state_to : NULL);

    $actual = $class->allowedIfStateDataIsValid($entity, $route_match);

    $this->assertTrue($expected ? $actual->isAllowed() : $actual->isNeutral());
  }

  /**
   * Data provider for checking, if user is allowed to use workflow transition.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfTransitionAllowed(): array {
    return [
      'no-workflow' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-allowed' => [
        TRUE,
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests access result, if user is allowed to use workflow transition.
   *
   * @param bool $has_workflow
   *   Whether an associated workflow type is available.
   * @param bool $has_permission
   *   Whether the current user has the required permission.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfTransitionAllowed
   *
   * @dataProvider dataProviderAllowedIfTransitionAllowed
   */
  public function testAllowedIfTransitionAllowed(bool $has_workflow, bool $has_permission, bool $expected): void {
    $workflow_id = 'test_workflow';
    $transition_id = 'test_transition';
    $state_to_id = 'test_state_to';

    // Prepare entity mockup.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($has_workflow ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($workflow_id);

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($this->once())
      ->method('getWorkflow')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow : NULL);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    // Prepare account mock.
    $account = $this->createMock(AccountInterface::class);

    $account->expects($has_workflow ? $this->once() : $this->never())
      ->method('hasPermission')
      ->with(sprintf('use %s transition %s', $workflow_id, $transition_id))
      ->willReturn($has_permission);

    // Prepare transition mock.
    $transition = $this->createMock(TransitionInterface::class);

    $transition->expects($has_workflow ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($transition_id);

    // Prepare current moderation state mock.
    $state_current = $this->createMock(StateInterface::class);

    $state_current->expects($has_workflow ? $this->once() : $this->never())
      ->method('getTransitionTo')
      ->with($state_to_id)
      ->willReturn($transition);

    // Prepare moderation state to transition to mock.
    $state_to = $this->createMock(StateInterface::class);

    $state_to->expects($has_workflow ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($state_to_id);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getStateCurrent',
      'getStateTo',
    ]);

    $class->expects($has_workflow ? $this->once() : $this->never())
      ->method('getStateCurrent')
      ->with($entity)
      ->willReturn($state_current);

    $class->expects($has_workflow ? $this->once() : $this->never())
      ->method('getStateTo')
      ->with($route_match)
      ->willReturn($state_to);

    $actual = $class->allowedIfTransitionAllowed($entity, $route_match, $account);

    $this->assertTrue($expected ? $actual->isAllowed() : $actual->isNeutral());
  }

  /**
   * Data provider for returning current content moderation state.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetStateCurrent(): array {
    return [
      'state-exists' => [
        TRUE,
        TRUE,
      ],
      'state-does-not-exist' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests returning the current content moderation state.
   *
   * @param bool $exists
   *   Whether the content moderation state exists.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getStateCurrent
   *
   * @dataProvider dataProviderGetStateCurrent
   */
  public function testGetStateCurrent(bool $exists, bool $expected): void {
    // Prepare content moderation state mock.
    $state = $this->createMock(ContentModerationState::class);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($this->once())
      ->method('getCurrentModerationState')
      ->with($entity)
      ->willReturn($exists ? $state : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $actual = $class->getStateCurrent($entity);

    if ($expected) {
      $this->assertInstanceOf(ContentModerationState::class, $actual);
      $this->assertEquals($state, $actual);
    }
    else {
      $this->assertFalse(isset($actual));
    }
  }

  /**
   * Data provider for returning content moderation state to transition from.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetStateFrom(): array {
    return [
      'state-exists' => [
        TRUE,
        TRUE,
      ],
      'state-does-not-exist' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests returning the content moderation state to transition to.
   *
   * @param bool $exists
   *   Whether the content moderation state exists.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getStateFrom
   *
   * @dataProvider dataProviderGetStateFrom
   */
  public function testGetStateFrom(bool $exists, bool $expected): void {
    // Prepare content moderation state mock.
    $state = $this->createMock(StateInterface::class);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    $route_match->expects($this->once())
      ->method('getParameter')
      ->with('from')
      ->willReturn($exists ? $state : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $actual = $class->getStateFrom($route_match);

    if ($expected) {
      $this->assertInstanceOf(StateInterface::class, $actual);
      $this->assertEquals($state, $actual);
    }
    else {
      $this->assertFalse(isset($actual));
    }
  }

  /**
   * Data provider for returning the content moderation state to transition to.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetStateTo(): array {
    return [
      'state-exists' => [
        TRUE,
        TRUE,
      ],
      'state-does-not-exist' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests returning the content moderation state to transition to.
   *
   * @param bool $exists
   *   Whether the content moderation state exists.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getStateTo
   *
   * @dataProvider dataProviderGetStateTo
   */
  public function testGetStateTo(bool $exists, bool $expected): void {
    // Prepare content moderation state mock.
    $state = $this->createMock(StateInterface::class);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    $route_match->expects($this->once())
      ->method('getParameter')
      ->with('to')
      ->willReturn($exists ? $state : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $actual = $class->getStateTo($route_match);

    if ($expected) {
      $this->assertInstanceOf(StateInterface::class, $actual);
      $this->assertEquals($state, $actual);
    }
    else {
      $this->assertFalse(isset($actual));
    }
  }

  /**
   * Data provider for moderation link is allowed on entity's canonical route.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderIsAllowedOnCanonicalRoute(): array {
    $entity_type_id = 'test_entity_type';

    return [
      'pending-revision' => [
        TRUE,
        $entity_type_id,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-pending-revision' => [
        FALSE,
        $entity_type_id,
        TRUE,
        TRUE,
        TRUE,
      ],
      'media-no-route' => [
        FALSE,
        'media',
        FALSE,
        TRUE,
        TRUE,
      ],
      'media-entity-form-route' => [
        FALSE,
        'media',
        TRUE,
        TRUE,
        FALSE,
      ],
      'media-entity-not-form-route' => [
        FALSE,
        'media',
        TRUE,
        FALSE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests content moderation link is allowed on entity's canonical route.
   *
   * @param bool $has_pending_revision
   *   Whether a pending revision is available.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param bool $has_route
   *   Whether a route object is available.
   * @param bool $is_entity_form
   *   Whether the current route is an entity form route?
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::isAllowedOnCanonicalRoute
   *
   * @dataProvider dataProviderIsAllowedOnCanonicalRoute
   */
  public function testIsAllowedOnCanonicalRoute(bool $has_pending_revision, string $entity_type_id, bool $has_route, bool $is_entity_form, bool $expected): void {
    $is_media = $entity_type_id === 'media';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    $entity->expects(!$has_pending_revision ? $this->once() : $this->never())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('hasPendingRevision')
      ->with($entity)
      ->willReturn($has_pending_revision);

    // Prepare route mock.
    $route = $this->createMock(Route::class);

    $route->expects($has_route && $is_media ? $this->once() : $this->never())
      ->method('hasDefault')
      ->with('_entity_form')
      ->willReturn($is_entity_form);

    // Prepare current route match mock.
    $this->currentRouteMatch->expects(!$has_pending_revision && $is_media ? $this->once() : $this->never())
      ->method('getRouteObject')
      ->willReturn($has_route ? $route : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected, $class->isAllowedOnCanonicalRoute($entity));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Access\ModerationStateTransitionAccessCheck_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModerationStateTransitionAccessCheck_Test|MockObject {
    return $this->getMockBuilder(ModerationStateTransitionAccessCheck_Test::class)
      ->setConstructorArgs([
        $this->moderatedEntityHelper,
        $this->moderationInformation,
        $this->currentRouteMatch,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked content moderation state transition access check class for tests.
 */
class ModerationStateTransitionAccessCheck_Test extends ModerationStateTransitionAccessCheck {

  /**
   * {@inheritdoc}
   */
  public function allowedIfCanTransition(ContentEntityInterface $entity, RouteMatchInterface $route_match): AccessResultInterface {
    return parent::allowedIfCanTransition($entity, $route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedIfStateDataIsValid(ContentEntityInterface $entity, RouteMatchInterface $route_match): AccessResultInterface {
    return parent::allowedIfStateDataIsValid($entity, $route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedIfTransitionAllowed(ContentEntityInterface $entity, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    return parent::allowedIfTransitionAllowed($entity, $route_match, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function getStateCurrent(ContentEntityInterface $entity): ?StateInterface {
    return parent::getStateCurrent($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getStateFrom(RouteMatchInterface $route_match): ?StateInterface {
    return parent::getStateFrom($route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function getStateTo(RouteMatchInterface $route_match): ?StateInterface {
    return parent::getStateTo($route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function isAllowedOnCanonicalRoute(ContentEntityInterface $entity): bool {
    return parent::isAllowedOnCanonicalRoute($entity);
  }

}
// @codingStandardsIgnoreEnd
