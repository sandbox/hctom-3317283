<?php

namespace Drupal\Tests\content_moderation_links\Unit\Access;

use Drupal\content_moderation_links\Access\DiscardLatestVersionAccessCheck;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\workflows\WorkflowInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Routing\Route;

/**
 * Tests the discard latest entity version access check.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Access\DiscardLatestVersionAccessCheck
 * @group content_moderation_links
 */
class DiscardLatestVersionAccessCheckTest extends AccessCheckTestBase {

  /**
   * Data provider for checking content moderation link access.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAccess(): array {
    return [
      'parent-not-allowed' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-workflow' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-latest-version-access' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-discard-permission' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-discardable-latest-version' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests checking content moderation link access.
   *
   * @param bool $parent_is_allowed
   *   Whether access is allowed by parent class.
   * @param bool $has_workflow
   *   Whether an associated workflow exists.
   * @param bool $last_version_access
   *   Whether access to latest entity version is granted.
   * @param bool $has_discard_permission
   *   Whether the 'Discard latest version' permission is granted.
   * @param bool $discardable_latest_version
   *   Whether it is a discardable latest version.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::access
   *
   * @dataProvider dataProviderAccess
   */
  public function testAccess(bool $parent_is_allowed, bool $has_workflow, bool $last_version_access, bool $has_discard_permission, bool $discardable_latest_version, bool $expected): void {
    // Prepare route mock.
    $route = $this->createMock(Route::class);

    // Prepare route match mock.
    $route_match = $this->createMock(RouteMatchInterface::class);

    // Prepare account mock.
    $account = $this->createMock(AccountInterface::class);

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->atMost(1))
      ->method('getWorkflowForEntity')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow : NULL);

    // Prepare access result mock.
    $access_result = $parent_is_allowed ? AccessResult::allowed() : AccessResult::neutral();

    // Prepare class mock.
    $class = $this->createClassMock([
      'allowedIfDiscardableLatestVersion',
      'allowedIfHasDiscardPermission',
      'allowedIfLatestVersionAccess',
      'checkAccess',
      'getEntity',
    ]);

    $class->expects($this->once())
      ->method('checkAccess')
      ->with($route, $route_match, $account)
      ->willReturn($access_result);

    $class->expects($this->atMost(1))
      ->method('getEntity')
      ->with($route, $route_match)
      ->willReturn($entity);

    $class->expects($parent_is_allowed && $has_workflow ? $this->once() : $this->never())
      ->method('allowedIfLatestVersionAccess')
      ->with($entity, $account)
      ->willReturn($last_version_access ? AccessResult::allowed() : AccessResult::neutral());

    $class->expects($parent_is_allowed && $has_workflow ? $this->once() : $this->never())
      ->method('allowedIfHasDiscardPermission')
      ->with($workflow, $account)
      ->willReturn($has_discard_permission ? AccessResult::allowed() : AccessResult::neutral());

    $class->expects($parent_is_allowed && $has_workflow ? $this->once() : $this->never())
      ->method('allowedIfDiscardableLatestVersion')
      ->with($entity)
      ->willReturn($discardable_latest_version ? AccessResult::allowed() : AccessResult::neutral());

    $actual = $class->access($route, $route_match, $account);

    $this->assertTrue($expected ? $actual->isAllowed() : $actual->isNeutral());

    if (!$has_workflow) {
      $this->assertInstanceOf(AccessResultReasonInterface::class, $actual);
      $this->assertEquals('No associated workflow.', $actual->getReason());
    }
  }

  /**
   * Data provider for checking, if entity is a discardable latest version.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfDiscardableLatestVersion(): array {
    return [
      'discardable-latest-version' => [
        TRUE,
        TRUE,
      ],
      'not-discardable-latest-version' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests access result, if entity is a discardable latest version.
   *
   * @param bool $is_discardable_latest_version
   *   Whether the entity is a discardable latest version.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfDiscardableLatestVersion
   *
   * @dataProvider dataProviderAllowedIfDiscardableLatestVersion
   */
  public function testAllowedIfDiscardableLatestVersion(bool $is_discardable_latest_version, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($this->once())
      ->method('isDiscardableLatestVersion')
      ->with($entity)
      ->willReturn($is_discardable_latest_version);

    // Prepare class mock.
    $class = $this->createClassMock();

    $access_result = $class->allowedIfDiscardableLatestVersion($entity);

    $this->assertTrue($expected ? $access_result->isAllowed() : $access_result->isNeutral());
  }

  /**
   * Data provider for checking, if user is allowed to discard latest version.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfHasDiscardPermission(): array {
    return [
      'has-permission' => [
        TRUE,
        TRUE,
      ],
      'not-has-permission' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests access result, if user is allowed to discard latest version.
   *
   * @param bool $has_permission
   *   Whether the user has required permission to discard latest version.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfHasDiscardPermission
   *
   * @dataProvider dataProviderAllowedIfHasDiscardPermission
   */
  public function testAllowedIfHasDiscardPermission(bool $has_permission, bool $expected): void {
    $workflow_id = 'test_workflow';

    // Prepare account mock.
    $account = $this->createMock(AccountInterface::class);

    $account->expects($this->once())
      ->method('hasPermission')
      ->with('discard latest version in ' . $workflow_id . ' workflow')
      ->willReturn($has_permission);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($this->once())
      ->method('id')
      ->willReturn($workflow_id);

    // Prepare class mock.
    $class = $this->createClassMock();

    $access_result = $class->allowedIfHasDiscardPermission($workflow, $account);

    $this->assertTrue($expected ? $access_result->isAllowed() : $access_result->isNeutral());
  }

  /**
   * Data provider for checking, if user is allowed to access latest version.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderAllowedIfLatestVersionAccess(): array {
    return [
      'allowed' => [
        [
          'view latest version',
          'view any unpublished content',
        ],
        TRUE,
        FALSE,
        TRUE,
      ],
      'allowed-owner' => [
        [
          'view latest version',
          'view own unpublished content',
        ],
        TRUE,
        TRUE,
        TRUE,
      ],
      'not-allowed' => [
        [],
        TRUE,
        FALSE,
        FALSE,
      ],
      'not-allowed-owner' => [
        [],
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-allowed-owner-entity-not-has-owner' => [
        [
          'view latest version',
          'view own unpublished content',
        ],
        FALSE,
        TRUE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests access result, if user is allowed to access latest version.
   *
   * @param array $permissions
   *   The permissions to check.
   * @param bool $entity_has_owner
   *   Whether the given entity has an owner.
   * @param bool $account_uid_is_entity_uid
   *   Whether the user ID of the given account is the entity owner's user ID.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::allowedIfLatestVersionAccess
   *
   * @dataProvider dataProviderAllowedIfLatestVersionAccess
   */
  public function testAllowedIfLatestVersionAccess(array $permissions, bool $entity_has_owner, bool $account_uid_is_entity_uid, bool $expected): void {
    $uid = 123;

    // Prepare entity mock.
    $entity = $this->createMock($entity_has_owner ? NodeInterface::class : ContentEntityInterface::class);

    if ($entity instanceof EntityOwnerInterface) {
      $entity->expects($this->atMost(1))
        ->method('getOwnerId')
        ->willReturn($account_uid_is_entity_uid ? $uid : ($uid + 1));
    }

    // Prepare account mock.
    $account = $this->createMock(AccountInterface::class);

    $account->expects($this->atMost(1))
      ->method('id')
      ->willReturn($uid);

    $account->expects($this->atMost(4))
      ->method('hasPermission')
      ->willReturnCallback(function (string $permission) use ($permissions): bool {
        return in_array($permission, $permissions, TRUE);
      });

    // Prepare class mock.
    $class = $this->createClassMock();

    $access_result = $class->allowedIfLatestVersionAccess($entity, $account);

    $this->assertTrue($expected ? $access_result->isAllowed() : $access_result->isNeutral());
  }

  /**
   * Data provider for checking, if is allowed on entity's canonical route.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderIsAllowedOnCanonicalRoute(): array {
    return [
      'restored-as-latest-version' => [
        TRUE,
        TRUE,
      ],
      'not-restored-as-latest-version' => [
        FALSE,
        FALSE,
      ],
    ];
  }

  /**
   * Tests access result, if is allowed on entity's canonical route.
   *
   * @param bool $is_restored_as_latest_version
   *   Whether the entity is restored as latest revision.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::isAllowedOnCanonicalRoute
   *
   * @dataProvider dataProviderIsAllowedOnCanonicalRoute
   */
  public function testIsAllowedOnCanonicalRoute(bool $is_restored_as_latest_version, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare moderated entity helper mock.
    $this->moderatedEntityHelper->expects($this->once())
      ->method('isRestoredAsLatestVersion')
      ->with($entity)
      ->willReturn($is_restored_as_latest_version);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected, $class->isAllowedOnCanonicalRoute($entity));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Access\DiscardLatestVersionAccessCheck_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): DiscardLatestVersionAccessCheck_Test|MockObject {
    return $this->getMockBuilder(DiscardLatestVersionAccessCheck_Test::class)
      ->setConstructorArgs([
        $this->moderatedEntityHelper,
        $this->moderationInformation,
        $this->currentRouteMatch,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked discard latest version access check class for tests.
 */
class DiscardLatestVersionAccessCheck_Test extends DiscardLatestVersionAccessCheck {

  /**
   * {@inheritdoc}
   */
  public function allowedIfDiscardableLatestVersion(ContentEntityInterface $entity): AccessResultInterface {
    return parent::allowedIfDiscardableLatestVersion($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedIfHasDiscardPermission(WorkflowInterface $workflow, AccountInterface $account): AccessResultInterface {
    return parent::allowedIfHasDiscardPermission($workflow, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedIfLatestVersionAccess(ContentEntityInterface $entity, AccountInterface $account): AccessResultInterface {
    return parent::allowedIfLatestVersionAccess($entity, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    return parent::checkAccess($route, $route_match, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function isAllowedOnCanonicalRoute(ContentEntityInterface $entity): bool {
    return parent::isAllowedOnCanonicalRoute($entity);
  }

}
// @codingStandardsIgnoreEnd
