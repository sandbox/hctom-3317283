<?php

namespace Drupal\Tests\content_moderation_links\Unit;

use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\ModerationLinkPermissions;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the dynamic content moderation link permissions.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\ModerationLinkPermissions
 * @group content_moderation_links
 */
class ModerationLinkPermissionTest extends ModerationLinkTestBase {

  /**
   * Tests getting the discard latest version moderation link permissions.
   *
   * @covers ::discardLatestVersionPermissions
   */
  public function testDiscardLatestVersionPermissions(): void {
    $workflow_id = 'test_workflow';
    $workflow_label = 'Test workflow';
    $config_dependency_key = 'test_config_dependency_key';
    $config_dependency_name = 'test_config_dependency_name';

    // Prepare workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createWorkflowMock($workflow_id, $workflow_type);

    $workflow->expects($this->atLeastOnce())
      ->method('label')
      ->willReturn($workflow_label);

    $workflow->expects($this->atLeastOnce())
      ->method('getConfigDependencyKey')
      ->willReturn($config_dependency_key);

    $workflow->expects($this->atLeastOnce())
      ->method('getConfigDependencyName')
      ->willReturn($config_dependency_name);

    // Prepare Workflow::loadMultipleByType() mock.
    $this->createWorkflowLoadMultipleByTypeMock($workflow_id, $workflow);

    // Create class mock.
    $class = $this->createClassMock();

    $permissions = $class->discardLatestVersionPermissions();

    $expected_count = 1;
    $expected_permission = 'discard latest version in ' . $workflow_id . ' workflow';
    $expected_title = '<em class="placeholder">' . $workflow_label . '</em> workflow: Discard latest version.';
    $expected_description = 'To back up and discard pending revision(s) in <em class="placeholder">' . $workflow_label . '</em> workflow, you also need permission to edit/update the corresponding content and to view its latest version.';
    $expected_dependencies = [
      $config_dependency_key => [
        $config_dependency_name,
      ],
    ];

    $this->assertCount($expected_count, $permissions);
    $this->assertArrayHasKey($expected_permission, $permissions);
    $this->assertArrayHasKey('title', $permissions[$expected_permission]);
    $this->assertEquals($expected_title, (string) $permissions[$expected_permission]['title']);
    $this->assertArrayHasKey('description', $permissions[$expected_permission]);
    $this->assertEquals($expected_description, (string) $permissions[$expected_permission]['description']);
    $this->assertArrayHasKey('dependencies', $permissions[$expected_permission]);
    $this->assertEquals($expected_dependencies, $permissions[$expected_permission]['dependencies']);
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\content_moderation_links\ModerationLinkPermissions|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModerationLinkPermissions|MockObject {
    return $this->getMockBuilder(ModerationLinkPermissions::class)
      ->onlyMethods($only_methods)
      ->getMock();
  }

}
