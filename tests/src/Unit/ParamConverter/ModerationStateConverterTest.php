<?php

namespace Drupal\Tests\content_moderation_links\Unit\ParamConverter;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_links\ParamConverter\ModerationStateConverter;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\workflows\StateInterface;
use Drupal\workflows\WorkflowInterface;
use Drupal\workflows\WorkflowTypeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Routing\Route;

/**
 * Tests the parameter converter for upcasting moderation state IDs to objects.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\ParamConverter\ModerationStateConverter
 * @group content_moderation_links
 */
class ModerationStateConverterTest extends UnitTestCase {

  /**
   * The moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moderationInformation;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Initialize moderation information mock.
    $this->moderationInformation = $this->createMock(ModerationInformationInterface::class);
  }

  /**
   * Data provider for converting path variables to their corresponding objects.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderConvert(): array {
    return [
      'not-has-route' => [
        FALSE,
        TRUE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-entity-type-id' => [
        TRUE,
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'no-entity' => [
        TRUE,
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'no-workflow' => [
        TRUE,
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
      'convert' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests converting path variables to their corresponding objects.
   *
   * @param bool $has_route
   *   Whether a route is available.
   * @param bool $has_entity_type_id
   *   Whether an entity type ID is available.
   * @param bool $has_entity
   *   Whether an entity is available.
   * @param bool $has_workflow
   *   Whether a related workflow is available.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::convert
   *
   * @dataProvider dataProviderConvert
   */
  public function testConvert(bool $has_route, bool $has_entity_type_id, bool $has_entity, bool $has_workflow, bool $expected): void {
    $entity_type_id = 'test_entity_type';
    $value = 'test_moderation_state';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare route mock.
    $route = $this->createMock(Route::class);

    // Prepare moderation state mock.
    $moderation_state = $this->createMock(StateInterface::class);

    // Prepare workflow type mock.
    $workflow_type = $this->createMock(WorkflowTypeInterface::class);

    $workflow_type->expects($has_route && $has_entity_type_id && $has_entity && $has_workflow ? $this->once() : $this->never())
      ->method('getState')
      ->with($value)
      ->willReturn($moderation_state);

    $defaults = [
      RouteObjectInterface::ROUTE_OBJECT => $has_route ? $route : NULL,
      $entity_type_id => $has_entity ? $entity : NULL,
    ];

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntity',
      'getEntityTypeId',
      'getRoute',
      'getWorkflowType',
    ]);

    $class->expects($this->once())
      ->method('getRoute')
      ->with($defaults)
      ->willReturn($has_route ? $route : NULL);

    $class->expects($has_route ? $this->once() : $this->never())
      ->method('getEntityTypeId')
      ->with($route, $defaults)
      ->willReturn($has_entity_type_id ? $entity_type_id : NULL);

    $class->expects($has_route && $has_entity_type_id ? $this->once() : $this->never())
      ->method('getEntity')
      ->with($entity_type_id, $defaults)
      ->willReturn($has_entity ? $entity : NULL);

    $class->expects($has_route && $has_entity_type_id && $has_entity ? $this->once() : $this->never())
      ->method('getWorkflowType')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow_type : NULL);

    $this->assertEquals($expected ? $moderation_state : NULL, $class->convert($value, [], 'name', $defaults));
  }

  /**
   * Data provider for if converter applies to specific route and variable.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderApplies(): array {
    return [
      'no-definition' => [
        NULL,
        FALSE,
      ],
      'empty-type' => [
        ['type' => ''],
        FALSE,
      ],
      'invalid-type' => [
        ['type' => 'invalid'],
        FALSE,
      ],
      'valid-type' => [
        ['type' => 'content_moderation_links:moderation_state'],
        TRUE,
      ],
    ];
  }

  /**
   * Tests determining if the converter applies to specific route and variable.
   *
   * @param array|null $definition
   *   The definition or NULL for none.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::applies
   *
   * @dataProvider dataProviderApplies
   */
  public function testApplies(?array $definition, bool $expected): void {
    // Prepare route mock.
    $route = $this->createMock(Route::class);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected, $class->applies($definition, 'name', $route));
  }

  /**
   * Data provider for returning content entity object from route defaults.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetEntity(): array {
    return [
      'no-entity' => [
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'invalid-entity' => [
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'no-moderated-entity' => [
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
      'moderated-entity' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests returning the content entity object from given route defaults.
   *
   * @param bool $has_entity
   *   Whether an entity is available.
   * @param bool $is_valid_entity
   *   Whether the entity is valid.
   * @param bool $is_moderated_entity
   *   Whether it is a moderated entity.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getEntity
   *
   * @dataProvider dataProviderGetEntity
   */
  public function testGetEntity(bool $has_entity, bool $is_valid_entity, bool $is_moderated_entity, bool $expected): void {
    $entity_type_id = 'test_entity_type';

    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare defaults mock.
    $defaults = [];
    if ($has_entity) {
      $defaults[$entity_type_id] = $is_valid_entity ? $entity : $this->createMock(EntityInterface::class);
    }

    // Prepare moderation information mock.
    $this->moderationInformation->expects($has_entity && $is_valid_entity ? $this->once() : $this->never())
      ->method('isModeratedEntity')
      ->with($entity)
      ->willReturn($is_moderated_entity);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $entity : NULL, $class->getEntity($entity_type_id, $defaults));
  }

  /**
   * Data provider for returning the entity type ID from given route defaults.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetEntityTypeId(): array {
    return [
      'no-entity-type-id' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'invalid-entity-type-id' => [
        TRUE,
        FALSE,
        FALSE,
      ],
      'entity-type-id' => [
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests returning the entity type ID from given route defaults.
   *
   * @param bool $has_entity_type_id
   *   Whether an entity type ID is available.
   * @param bool $is_valid_entity_type_id
   *   Whether the entity type ID is valid.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getEntityTypeId
   *
   * @dataProvider dataProviderGetEntityTypeId
   */
  public function testGetEntityTypeId(bool $has_entity_type_id, bool $is_valid_entity_type_id, bool $expected): void {
    $entity_type_id = NULL;
    if ($has_entity_type_id) {
      $entity_type_id = $is_valid_entity_type_id ? 'foo' : 'foobar';
    }

    // Prepare route mock.
    $route = $this->createMock(Route::class);

    $route->expects($this->once())
      ->method('getOption')
      ->with('_content_moderation_links_entity_type')
      ->willReturn($entity_type_id);

    // Prepare defaults mock.
    $defaults = [
      'foo' => 'foo',
      'bar' => 'bar',
    ];

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $entity_type_id : NULL, $class->getEntityTypeId($route, $defaults));
  }

  /**
   * Data provider for returning the route object from given route defaults.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetRoute(): array {
    return [
      'no-route' => [
        FALSE,
        TRUE,
        FALSE,
      ],
      'invalid-route' => [
        TRUE,
        FALSE,
        FALSE,
      ],
      'route' => [
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests returning the route object from given route defaults.
   *
   * @param bool $has_route
   *   Whether a route is available.
   * @param bool $is_valid_route
   *   Whether the route is valid.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getRoute
   *
   * @dataProvider dataProviderGetRoute
   */
  public function testGetRoute(bool $has_route, bool $is_valid_route, bool $expected): void {
    $route = $this->createMock(Route::class);

    // Prepare defaults mock.
    $defaults = [];
    if ($has_route) {
      $defaults[RouteObjectInterface::ROUTE_OBJECT] = $is_valid_route ? $route : [];
    }

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $route : NULL, $class->getRoute($defaults));
  }

  /**
   * Data provider for returning the workflow type for the given entity.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetWorkflowType(): array {
    return [
      'no-workflow' => [
        FALSE,
        FALSE,
      ],
      'workflow' => [
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests returning the workflow type for the given entity.
   *
   * @param bool $has_workflow
   *   Whether a related workflow is available.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getWorkflowType
   *
   * @dataProvider dataProviderGetWorkflowType
   */
  public function testGetWorkflowType(bool $has_workflow, bool $expected): void {
    // Prepare entity mock.
    $entity = $this->createMock(ContentEntityInterface::class);

    // Prepare workflow type mock.
    $workflow_type = $this->createMock(WorkflowTypeInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($has_workflow ? $this->once() : $this->never())
      ->method('getTypePlugin')
      ->willReturn($workflow_type);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->once())
      ->method('getWorkflowForEntity')
      ->with($entity)
      ->willReturn($has_workflow ? $workflow : NULL);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $workflow_type : NULL, $class->getWorkflowType($entity));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\ParamConverter\ModerationStateConverter_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModerationStateConverter_Test|MockObject {
    return $this->getMockBuilder(ModerationStateConverter_Test::class)
      ->setConstructorArgs([
        $this->moderationInformation,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked parameter converter for content moderation state IDs class for tests.
 */
class ModerationStateConverter_Test extends ModerationStateConverter {

  /**
   * {@inheritdoc}
   */
  public function getEntity(string $entity_type_id, array $defaults): ?ContentEntityInterface {
    return parent::getEntity($entity_type_id, $defaults);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(Route $route, array $defaults): ?string {
    return parent::getEntityTypeId($route, $defaults);
  }

  /**
   * {@inheritdoc}
   */
  public function getRoute(array $defaults): ?Route {
    return parent::getRoute($defaults);
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkflowType(ContentEntityInterface $entity): ?WorkflowTypeInterface {
    return parent::getWorkflowType($entity);
  }

}
// @codingStandardsIgnoreEnd
