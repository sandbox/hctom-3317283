<?php

namespace Drupal\Tests\content_moderation_links\Unit\Plugin\Derivative\Menu\LocalAction;

use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\ModerationStateTransitionDeriver;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Tests\content_moderation_links\Unit\ModerationLinkTestBase;

/**
 * Tests the deriver for moderation state transition local action definitions.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\ModerationStateTransitionDeriver
 * @group content_moderation_links
 */
class ModerationStateTransitionTest extends ModerationLinkTestBase {

  /**
   * Tests getting the definition of all derivatives of a base plugin.
   *
   * @covers ::getDerivativeDefinitions
   */
  public function testGetDerivativeDefinitions(): void {
    $entity_type_id = 'test_content_entity_type';
    $entity_type_id_invalid = 'test_entity_type';
    $state_from_id = 'test_from';
    $state_to_id = 'test_to';
    $transition_id = 'test_transition';
    $transition_label = 'Test transition';
    $transition_weight = 123;
    $workflow_id = 'test_workflow';
    $workflow_cache_contexts = [
      'test_context_1',
      'test_context_2',
    ];
    $workflow_cache_tags = [
      'test_tags_1',
      'test_tags_2',
    ];
    $workflow_cache_max_age = Cache::PERMANENT;

    // Prepare base plugin definition mock.
    $base_plugin_definition = [
      'test' => TRUE,
    ];

    // Prepare invalid entity type mock.
    $entity_type_invalid = $this->createMock(EntityTypeInterface::class);

    // Prepare content entity type mock.
    $entity_type = $this->createMock(ContentEntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('hasLinkTemplate')
      ->with(LinkTemplate::MODERATION_STATE_TRANSITION)
      ->willReturn(TRUE);

    // Prepare entity type manager mock.
    $this->entityTypeManager->expects($this->atLeast(2))
      ->method('getDefinition')
      // @todo Implement better solution for consecutive calls.
      ->willReturnCallback(fn (string $entity_type_id_arg): EntityTypeInterface => match ($entity_type_id_arg) {
        $entity_type_id => $entity_type,
        $entity_type_id_invalid => $entity_type_invalid,
        default => throw new \LogicException('Parameters do not match expected values.'),
      });

    // Prepare content moderation state transition mock.
    $transition = $this->createTransitionMock($state_from_id, $state_to_id, $transition_label, $transition_weight);

    // Prepare workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    $workflow_type->expects($this->once())
      ->method('getEntityTypes')
      ->willReturn([
        $entity_type_id => $entity_type_id,
        $entity_type_id_invalid => $entity_type_id_invalid,
      ]);

    $workflow_type->expects($this->once())
      ->method('getTransitions')
      ->willReturn([
        $transition_id => $transition,
      ]);

    // Prepare workflow mock.
    $workflow = $this->createWorkflowMock($workflow_id, $workflow_type, $workflow_cache_contexts, $workflow_cache_tags, $workflow_cache_max_age);

    // Prepare Workflow::loadMultipleByType() mock.
    $this->createWorkflowLoadMultipleByTypeMock($workflow_id, $workflow);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getWorkflowType',
    ]);

    $class->expects($this->atLeastOnce())
      ->method('getWorkflowType')
      ->willReturn($workflow_type);

    $this->assertEquals([
      $entity_type_id . '.' . $workflow_id . '.' . $state_from_id . '.' . $state_to_id => [
        'title' => $transition_label,
        'route_name' => 'entity.' . $entity_type_id . '.content_moderation_links_moderation_state_transition',
        'route_parameters' => [
          'from' => $state_from_id,
          'to' => $state_to_id,
        ],
        'appears_on' => [
          'entity.' . $entity_type_id . '.canonical',
          'entity.' . $entity_type_id . '.latest_version',
        ],
        'weight' => $transition_weight,
        'cache_contexts' => $workflow_cache_contexts,
        'cache_tags' => $workflow_cache_tags,
        'cache_max_age' => $workflow_cache_max_age,
        'test' => TRUE,
      ],
    ], $class->getDerivativeDefinitions($base_plugin_definition));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\ModerationStateTransitionDeriver|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []) {
    return $this->getMockBuilder(ModerationStateTransitionDeriver::class)
      ->setConstructorArgs([
        $this->entityTypeManager,
        $this->moderationInformation,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}
