<?php

namespace Drupal\Tests\content_moderation_links\Unit\Plugin\Derivative\Menu\LocalAction;

use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\ModerationLinkDeriverBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Tests\content_moderation_links\Unit\ModerationLinkTestBase;
use Drupal\workflows\WorkflowInterface;

/**
 * Tests the base class for menu link definitions of content moderation links.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\ModerationLinkDeriverBase
 * @group content_moderation_links
 */
class ModerationLinkDeriverBaseTest extends ModerationLinkTestBase {

  /**
   * Tests getting the entity types that support content moderation.
   *
   * @covers ::getModeratedEntityTypes
   */
  public function testGetModeratedEntityTypes(): void {
    // Prepare (content) entity type mocks.
    $entity_type = $this->createMock(EntityTypeInterface::class);
    $content_entity_type = $this->createMock(ContentEntityTypeInterface::class);

    // Prepare moderation information mock.
    $this->moderationInformation->expects($this->atLeastOnce())
      ->method('isModeratedEntityType')
      ->willReturn(TRUE);

    // Prepare entity type manager mock.
    $this->entityTypeManager->expects($this->once())
      ->method('getDefinitions')
      ->willReturn([
        'test_entity_type' => $entity_type,
        'test_content_entity_type' => $content_entity_type,
      ]);

    // Prepare class mock.
    $class = $this->createClassMock();

    // Get entity types twice to test static cache.
    $class->getModeratedEntityTypes();
    $entity_types = $class->getModeratedEntityTypes();

    $this->assertArrayNotHasKey('test_entity_type', $entity_types);
    $this->assertArrayHasKey('test_content_entity_type', $entity_types);
  }

  /**
   * Tests getting the content moderation workflow type plugin.
   *
   * @covers ::getWorkflowType
   */
  public function testGetWorkflowType(): void {
    // Prepare workflow type mock.
    $workflow_type = $this->createMock(ContentModerationInterface::class);

    // Prepare workflow mock.
    $workflow = $this->createMock(WorkflowInterface::class);

    $workflow->expects($this->once())
      ->method('getTypePlugin')
      ->willReturn($workflow_type);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($workflow_type, $class->getWorkflowType($workflow));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Plugin\Derivative\Menu\LocalAction\ModerationLinkDeriverBase_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []) {
    return $this->getMockBuilder(ModerationLinkDeriverBase_Test::class)
      ->setConstructorArgs([
        $this->entityTypeManager,
        $this->moderationInformation,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked content moderation link deriver base class for tests.
 */
class ModerationLinkDeriverBase_Test extends ModerationLinkDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getModeratedEntityTypes(): array {
    return parent::getModeratedEntityTypes();
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkflowType(WorkflowInterface $workflow): ?ContentModerationInterface {
    return parent::getWorkflowType($workflow);
  }

}
// @codingStandardsIgnoreEnd
