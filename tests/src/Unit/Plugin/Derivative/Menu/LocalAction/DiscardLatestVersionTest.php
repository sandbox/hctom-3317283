<?php

namespace Drupal\Tests\content_moderation_links\Unit\Plugin\Derivative\Menu\LocalAction;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\DiscardLatestVersionDeriver;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Tests\content_moderation_links\Unit\ModerationLinkTestBase;

/**
 * Tests the deriver for discard latest entity version local action definitions.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\DiscardLatestVersionDeriver
 * @group content_moderation_links
 */
class DiscardLatestVersionTest extends ModerationLinkTestBase {

  /**
   * Tests getting the definition of all derivatives of a base plugin.
   *
   * @covers ::getDerivativeDefinitions
   */
  public function testGetDerivativeDefinitions(): void {
    $entity_type_id = 'test_content_entity_type';

    // Prepare base plugin definition mock.
    $base_plugin_definition = [
      'test' => TRUE,
    ];

    // Prepare entity type mock.
    $entity_type = $this->createMock(ContentEntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('hasLinkTemplate')
      ->with(LinkTemplate::DISCARD_LATEST_VERSION)
      ->willReturn(TRUE);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getModeratedEntityTypes',
    ]);

    $class->expects($this->once())
      ->method('getModeratedEntityTypes')
      ->willReturn([
        $entity_type_id => $entity_type,
      ]);

    $this->assertEquals([
      $entity_type_id => [
        'title' => 'Discard latest version',
        'route_name' => 'entity.' . $entity_type_id . '.content_moderation_links_discard_latest_version',
        'appears_on' => [
          'entity.' . $entity_type_id . '.canonical',
          'entity.' . $entity_type_id . '.latest_version',
        ],
        'weight' => -50,
        'test' => TRUE,
      ],
    ], $class->getDerivativeDefinitions($base_plugin_definition));
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\content_moderation_links\Plugin\Derivative\Menu\LocalAction\DiscardLatestVersionDeriver|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []) {
    return $this->getMockBuilder(DiscardLatestVersionDeriver::class)
      ->setConstructorArgs([
        $this->entityTypeManager,
        $this->moderationInformation,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}
