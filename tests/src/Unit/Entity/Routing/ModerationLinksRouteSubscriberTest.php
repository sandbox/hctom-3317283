<?php

namespace Drupal\Tests\content_moderation_links\Unit\Entity\Routing;

use Drupal\content_moderation_links\Entity\LinkTemplate;
use Drupal\content_moderation_links\Entity\Routing\ModerationLinksRouteSubscriber;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Tests the route subscriber for moderation links of revisionable entities.
 *
 * @coversDefaultClass \Drupal\content_moderation_links\Entity\Routing\ModerationLinksRouteSubscriber
 * @group content_moderation_links
 */
class ModerationLinksRouteSubscriberTest extends UnitTestCase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Initialize entity field manager mock.
    $this->entityFieldManager = $this->createMock(EntityFieldManagerInterface::class);
  }

  /**
   * Tests creating and returning a new route collection.
   *
   * @covers ::createRouteCollection
   */
  public function testCreateRouteCollection(): void {
    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertInstanceOf(RouteCollection::class, $class->createRouteCollection());
  }

  /**
   * Data provider for getting the type of the ID key for a given entity type.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetEntityTypeIdKeyType(): array {
    return [
      'not-fieldable' => [
        FALSE,
        TRUE,
        TRUE,
        FALSE,
      ],
      'not-has-key' => [
        TRUE,
        FALSE,
        TRUE,
        FALSE,
      ],
      'not-has-field-storage' => [
        TRUE,
        TRUE,
        FALSE,
        FALSE,
      ],
      'has-id' => [
        TRUE,
        TRUE,
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests getting the type of the ID key for a given entity type.
   *
   * @param bool $is_fieldable_entity_type
   *   Whether it is a fieldable entity type.
   * @param bool $has_key
   *   Whether the key is available.
   * @param bool $field_storage_exists
   *   Whether the field storage exists.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getEntityTypeIdKeyType
   *
   * @dataProvider dataProviderGetEntityTypeIdKeyType
   */
  public function testGetEntityTypeIdKeyType(bool $is_fieldable_entity_type, bool $has_key, bool $field_storage_exists, bool $expected): void {
    $entity_type_id = 'test_entity_type';
    $key_id = 'test_key_id';
    $key_id_type = 'string';

    // Prepare entity type mock.
    $entity_type = $this->createMock(EntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('entityClassImplements')
      ->with(FieldableEntityInterface::class)
      ->willReturn($is_fieldable_entity_type);

    $entity_type->expects($is_fieldable_entity_type ? $this->once() : $this->never())
      ->method('getKey')
      ->with('id')
      ->willReturn($has_key ? $key_id : FALSE);

    $entity_type->expects($is_fieldable_entity_type && $has_key ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($entity_type_id);

    // Prepare field storage mock.
    $field_storage = $this->createMock(FieldStorageDefinitionInterface::class);

    $field_storage->expects($is_fieldable_entity_type && $has_key && $field_storage_exists ? $this->once() : $this->never())
      ->method('getType')
      ->willReturn($key_id_type);

    // Prepare field storage definitions list mock.
    $field_storage_definitions = [];
    if ($field_storage_exists) {
      $field_storage_definitions[$key_id] = $field_storage;
    }

    // Prepare entity field manager mock.
    $this->entityFieldManager->expects($is_fieldable_entity_type && $has_key ? $this->once() : $this->never())
      ->method('getFieldStorageDefinitions')
      ->with($entity_type_id)
      ->willReturn($field_storage_definitions);

    // Prepare class mock.
    $class = $this->createClassMock();

    $this->assertEquals($expected ? $key_id_type : NULL, $class->getEntityTypeIdKeyType($entity_type));
  }

  /**
   * Data provider for providing routes for entities.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetRoutes(): array {
    return [
      'not-has-route-moderation-state-transition' => [
        FALSE,
        TRUE,
      ],
      'not-has-route-discard-latest-version' => [
        TRUE,
        FALSE,
      ],
      'all-routes' => [
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Tests providing routes for entities.
   *
   * @param bool $has_route_moderation_state_transition
   *   Whether the 'Moderation state transition' route exists.
   * @param bool $has_route_discard_latest_version
   *   Whether the 'Discard latest version' route exists.
   *
   * @covers ::getRoutes
   *
   * @dataProvider dataProviderGetRoutes
   */
  public function testGetRoutes(bool $has_route_moderation_state_transition, bool $has_route_discard_latest_version): void {
    $entity_type_id = 'test_entity_type';
    $route_name_discard_latest_version = 'entity.' . $entity_type_id . '.content_moderation_links_discard_latest_version';
    $route_name_moderation_state_change = 'entity.' . $entity_type_id . '.content_moderation_links_moderation_state_transition';

    // Prepare entity type mock.
    $entity_type = $this->createMock(EntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('id')
      ->willReturn($entity_type_id);

    // Prepare route collection mock.
    $route_collection = $this->createMock(RouteCollection::class);

    // Prepare moderation state transition route mock.
    $route_moderation_state_transition = $this->createMock(Route::class);

    // Prepare discard latest version route mock.
    $route_discard_latest_version = $this->createMock(Route::class);

    $consecutive_add = [];
    if ($has_route_discard_latest_version) {
      $consecutive_add[] = [
        $route_name_discard_latest_version,
        $route_discard_latest_version,
      ];
    }
    if ($has_route_moderation_state_transition) {
      $consecutive_add[] = [
        $route_name_moderation_state_change,
        $route_moderation_state_transition,
      ];
    }

    $route_collection->expects($this->exactly(count($consecutive_add)))
      ->method('add')
      // @todo Implement better solution for consecutive calls.
      ->withConsecutive(...$consecutive_add);

    // Prepare class mock.
    $class = $this->createClassMock([
      'createRouteCollection',
      'getRouteDiscardLatestVersion',
      'getRouteModerationStateTransition',
    ]);

    $class->expects($this->once())
      ->method('createRouteCollection')
      ->willReturn($route_collection);

    $class->expects($this->once())
      ->method('getRouteModerationStateTransition')
      ->with($entity_type)
      ->willReturn($has_route_moderation_state_transition ? $route_moderation_state_transition : NULL);

    $class->expects($this->once())
      ->method('getRouteDiscardLatestVersion')
      ->with($entity_type)
      ->willReturn($has_route_discard_latest_version ? $route_discard_latest_version : NULL);

    $actual = $class->getRoutes($entity_type);

    $this->assertInstanceOf(RouteCollection::class, $actual);
  }

  /**
   * Data provider for getting the discard latest version route.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetRouteDiscardLatestVersion(): array {
    return [
      'not-has-link-template' => [
        FALSE,
        'string',
        FALSE,
      ],
      'key-type-string' => [
        TRUE,
        'string',
        TRUE,
      ],
      'key-type-integer' => [
        TRUE,
        'integer',
        TRUE,
      ],
    ];
  }

  /**
   * Tests getting the discard latest version route.
   *
   * @param bool $has_link_template
   *   Whether the entity link template exists.
   * @param string $key_type
   *   The ID key type.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getRouteDiscardLatestVersion
   *
   * @dataProvider dataProviderGetRouteDiscardLatestVersion
   */
  public function testGetRouteDiscardLatestVersion(bool $has_link_template, string $key_type, bool $expected): void {
    $entity_type_id = 'test_entity_type';
    $link_template = '/test/latest-version/discard';

    // Prepare entity type mock.
    $entity_type = $this->createMock(EntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('hasLinkTemplate')
      ->with(LinkTemplate::DISCARD_LATEST_VERSION)
      ->willReturn($has_link_template);

    $entity_type->expects($has_link_template ? $this->once() : $this->never())
      ->method('getLinkTemplate')
      ->with(LinkTemplate::DISCARD_LATEST_VERSION)
      ->willReturn($link_template);

    $entity_type->expects($has_link_template ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($entity_type_id);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntityTypeIdKeyType',
    ]);

    $class->expects($has_link_template ? $this->once() : $this->never())
      ->method('getEntityTypeIdKeyType')
      ->with($entity_type)
      ->willReturn($key_type);

    $actual = $class->getRouteDiscardLatestVersion($entity_type);

    if ($expected) {
      $this->assertInstanceOf(Route::class, $actual);
      $this->assertTrue($actual->hasDefault('_entity_form'));
      $this->assertEquals($entity_type_id . '.' . LinkTemplate::DISCARD_LATEST_VERSION, $actual->getDefault('_entity_form'));
      $this->assertTrue($actual->hasDefault('_title'));
      $this->assertEquals('Discard latest version', $actual->getDefault('_title'));
      $this->assertTrue($actual->hasRequirement('_content_moderation_links_discard_latest_version'));
      $this->assertEquals('TRUE', $actual->getRequirement('_content_moderation_links_discard_latest_version'));
      $this->assertTrue($actual->hasOption('_content_moderation_links_entity_type'));
      $this->assertEquals($entity_type_id, $actual->getOption('_content_moderation_links_entity_type'));
      $this->assertTrue($actual->hasOption('parameters'));
      $this->assertEquals([
        $entity_type_id => [
          'type' => 'entity:' . $entity_type_id,
          'load_latest_revision' => TRUE,
        ],
      ], $actual->getOption('parameters'));

      if ($key_type === 'integer') {
        $this->assertTrue($actual->hasRequirement($entity_type_id));
        $this->assertEquals('\d+', $actual->getRequirement($entity_type_id));
      }
      else {
        $this->assertFalse($actual->hasRequirement($entity_type_id));
      }
    }
    else {
      $this->assertNull($actual);
    }
  }

  /**
   * Data provider for getting the content moderation state transition route.
   *
   * @return array
   *   The test data.
   */
  public function dataProviderGetRouteModerationStateTransition(): array {
    return [
      'not-has-link-template' => [
        FALSE,
        'string',
        FALSE,
      ],
      'key-type-string' => [
        TRUE,
        'string',
        TRUE,
      ],
      'key-type-integer' => [
        TRUE,
        'integer',
        TRUE,
      ],
    ];
  }

  /**
   * Tests getting the content moderation state transition route.
   *
   * @param bool $has_link_template
   *   Whether the entity link template exists.
   * @param string $key_type
   *   The ID key type.
   * @param bool $expected
   *   The expected value.
   *
   * @covers ::getRouteModerationStateTransition
   *
   * @dataProvider dataProviderGetRouteModerationStateTransition
   */
  public function testGetRouteModerationStateTransition(bool $has_link_template, string $key_type, bool $expected): void {
    $entity_type_id = 'test_entity_type';
    $link_template = '/test/moderation-state-change';

    // Prepare entity type mock.
    $entity_type = $this->createMock(EntityTypeInterface::class);

    $entity_type->expects($this->once())
      ->method('hasLinkTemplate')
      ->with(LinkTemplate::MODERATION_STATE_TRANSITION)
      ->willReturn($has_link_template);

    $entity_type->expects($has_link_template ? $this->once() : $this->never())
      ->method('getLinkTemplate')
      ->with(LinkTemplate::MODERATION_STATE_TRANSITION)
      ->willReturn($link_template);

    $entity_type->expects($has_link_template ? $this->once() : $this->never())
      ->method('id')
      ->willReturn($entity_type_id);

    // Prepare class mock.
    $class = $this->createClassMock([
      'getEntityTypeIdKeyType',
    ]);

    $class->expects($has_link_template ? $this->once() : $this->never())
      ->method('getEntityTypeIdKeyType')
      ->with($entity_type)
      ->willReturn($key_type);

    $actual = $class->getRouteModerationStateTransition($entity_type);

    if ($expected) {
      $this->assertInstanceOf(Route::class, $actual);
      $this->assertTrue($actual->hasDefault('_entity_form'));
      $this->assertEquals($entity_type_id . '.' . LinkTemplate::MODERATION_STATE_TRANSITION, $actual->getDefault('_entity_form'));
      $this->assertTrue($actual->hasRequirement('_content_moderation_links_moderation_state_transition'));
      $this->assertEquals('TRUE', $actual->getRequirement('_content_moderation_links_moderation_state_transition'));
      $this->assertTrue($actual->hasOption('_content_moderation_links_entity_type'));
      $this->assertEquals($entity_type_id, $actual->getOption('_content_moderation_links_entity_type'));
      $this->assertTrue($actual->hasOption('parameters'));
      $this->assertEquals([
        $entity_type_id => [
          'type' => 'entity:' . $entity_type_id,
          'load_latest_revision' => TRUE,
        ],
        'from' => [
          'type' => 'content_moderation_links:moderation_state',
        ],
        'to' => [
          'type' => 'content_moderation_links:moderation_state',
        ],
      ], $actual->getOption('parameters'));

      if ($key_type === 'integer') {
        $this->assertTrue($actual->hasRequirement($entity_type_id));
        $this->assertEquals('\d+', $actual->getRequirement($entity_type_id));
      }
      else {
        $this->assertFalse($actual->hasRequirement($entity_type_id));
      }
    }
    else {
      $this->assertNull($actual);
    }
  }

  /**
   * Creates and returns a test class mock.
   *
   * @param array $only_methods
   *   An array of names for methods to be configurable.
   *
   * @return \Drupal\Tests\content_moderation_links\Unit\Entity\Routing\ModerationLinksRouteSubscriber_Test|\PHPUnit\Framework\MockObject\MockObject
   *   The mocked class.
   */
  protected function createClassMock(array $only_methods = []): ModerationLinksRouteSubscriber_Test|MockObject {
    return $this->getMockBuilder(ModerationLinksRouteSubscriber_Test::class)
      ->setConstructorArgs([
        $this->entityFieldManager,
      ])
      ->onlyMethods($only_methods)
      ->getMock();
  }

}

// @codingStandardsIgnoreStart
/**
 * Mocked route subscriber for content moderation links class for tests.
 */
class ModerationLinksRouteSubscriber_Test extends ModerationLinksRouteSubscriber {

  /**
   * {@inheritdoc}
   */
  public function createRouteCollection(): RouteCollection {
    return parent::createRouteCollection();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeIdKeyType(EntityTypeInterface $entity_type): ?string {
    return parent::getEntityTypeIdKeyType($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteDiscardLatestVersion(EntityTypeInterface $entity_type): ?Route {
    return parent::getRouteDiscardLatestVersion($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteModerationStateTransition(EntityTypeInterface $entity_type): ?Route {
    return parent::getRouteModerationStateTransition($entity_type);
  }

}
// @codingStandardsIgnoreEnd
